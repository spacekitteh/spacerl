module Control.Effects.RLUI where

import           Polysemy
--import           Games.RogueLike.Tile
import           Games.RogueLike.Types.Coordinates
import           Games.RogueLike.Types.Particle
import           Data.Text.Prettyprint.Widget
import           Games.RogueLike.Types.Glyph

import           Data.Text
--data ViewType = TopDown | MultiView | Isometric
--data InterfaceType = Terminal | Tileset | Graphical


{-
use SVG as intermediate format?
-}

data RenderRL m a where
    RenderGlyph ::UICoord -> Glyph -> RenderRL m ()
    DrawText ::UICoord -> Text -> RenderRL m ()
    DrawDocument ::UICoord -> TUIDoc -> RenderRL m ()
    DrawParticle ::Particle -> RenderRL m ()
    DrawPanel ::UICoord -> UICoord -> TUIDoc -> RenderRL m ()
