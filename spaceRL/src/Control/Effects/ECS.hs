module Control.Effects.ECS where
{-
import           Polysemy
import           GHC.Generics
import           Control.Lens
import           Data.Word
import           Data.Kind
import           Data.IntMap
import           Data.Functor.Identity
import           Control.Lens.Indexed
import           Data.Data
import           Data.Typeable
import           Control.Monad.State
import           Control.Lens.TH
--import           GDP

import           Data.Coerce
import           Data.Maybe
import           Linear
import qualified Data.Vector.Generic           as VG

import qualified Data.HashSet                  as HS
import           Data.Tagged


-}







{-
moveCritter'
    :: ( MonadState (World' Individual) m
       , HasComponent (World' Individual) (Position)
       )
    => m ()
moveCritter' = do
    position1 . x += 3.0

move
    :: (MonadState (World' Storing) m, Fact (critter `Has` Position))
    => Entity ~~ critter
    -> m ()
move ent = pure ()


moveCritter
    :: ( MonadState (World' Individual) m
       , HasComponent (World' Individual) (Position)
       )
    => Entity ~~ critter
    -> m ()
moveCritter ent = do
    position1 . x += 3.0
-}
{-onEntity :: MonadState (World Individual) m =>

moveCritter :: Entity -> StateT (World Storing) IO ()
moveCritter critter = do


checkForNearby :: StateT (World Storing) IO ()
checkForNearby = do

-}

{-with'
    :: (World w, Components c, Components c')
    => IndexedTraversal Entity (w s) (w t) (c a) (c b)
    -> IndexedTraversal Entity (w s) (w t) (c' a) (c' b)
with' t = undefined-}

{-}

Field storage:
1. Storing individual fields for concrete entities
2. Storing individual fields for AoS
3. Storing containers of fields for SoA


Component storage:
1. AoS = Store container of concrete Components
2. SoA = Store components parameterised by their field storage

Entity storage:
1. For world, delegate to component storage
2. a. For concrete entities, specialised structure.
   b. Alternatively, just use prisms and project them to tuples



Idea: Traversal from the ECS to lenses? i.e. with :: Traversal' ECS (Lens ECS someprop)

-- FIXME IMPORTANT!!!
Make sure to generate constraints when generating optics!
-}

--class Comp c acc p a where
--    type R c acc p a ::





{-

#1
do
    pos@(Position x y)<- with
    vel <- with @Velocity
    Not @Flying <- with
    pos.x += vel.x


#2
process l a = do
    ecs <- get
    iforMOf l ecs a

movement =
    process (with @Position . with @Velocity . with @(Not Flying)) $ \ent (position, velocity) -> do
        position.x += velocity.x
        if ent `in` bosses then
            putStrLn "gotta go fast"
            else pure ()


-}
