module Games.RogueLike.Systems.MapIndexingSystem where
import Numeric
import Control.Lens
import Data.Maybe    
import Control.Monad
import Data.Sequence
import Data.Monoid
import GHC.Real    
import Data.Sequence.Lens (seqOf)
import Debug.Trace (traceEvent)
import GHC.Generics (Generic)
import Games.ECS
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Name
  ( HasName (name),
    UsingName,
    renderEntityName,
  )
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory (UsingBlocksVision)
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Types.PhysicalTypes    
import Games.RogueLike.Systems.MapIndexingSystem.Octree as Octree hiding (entityIsTraversable)--hiding (positionInitiativePenalty)
--import qualified Games.RogueLike.Systems.MapIndexingSystem.Octree as Octree (positionInitiativePenalty)
import Linear.V3 (V3 (..))
import Polysemy (Member, Members, Sem)
import Polysemy.State (State, get, put)
import Games.RogueLike.Components.HealthStatus
import Control.DeepSeq    
data MapIndexingSystem = MapIndexingSystem {_octree :: !Octree.Octree} deriving (Eq, Show, Generic, NFData)

makeLenses ''MapIndexingSystem

class
  ( UsingBlocksVision worldType Individual,
    UsingBlocksMovement worldType Individual,
    UsingPosition worldType Individual,
--    UsingMovementPenalty worldType Individual,
    UsingHiddenFrom worldType Individual,
    UsingIsDead worldType Individual
  ) =>
  WritesMapIndexing' worldType where
      entityIsTraversable :: worldType Individual -> Bool
      entityIsTraversable critter = (has isDead critter)

instance
  ( UsingBlocksVision worldType Individual,
    UsingBlocksMovement worldType Individual,
    UsingPosition worldType Individual,
--    UsingMovementPenalty worldType Individual,
    UsingHiddenFrom worldType Individual,
    UsingIsDead worldType Individual                    
  ) =>
  WritesMapIndexing' worldType

class (Monad m) => MonadMapIndexing m where
  getMapIndex :: m MapIndexingSystem
  putMapIndex :: MapIndexingSystem -> m ()
  

instance (Member (State MapIndexingSystem) r) => MonadMapIndexing (Sem r) where
  {-# INLINE getMapIndex #-}
  getMapIndex = get
  {-# INLINE putMapIndex #-}
  putMapIndex = put

type WritesMapIndexing worldType m = (UsingGameLog m, MonadMapIndexing m, UsingName worldType Individual, WritesMapIndexing' worldType)

type WritesMapIndexing'' worldType r =
  ( UsingGameLog' r,
    Members '[State MapIndexingSystem, State GameLog] r,
    UsingName worldType Individual,
    WritesMapIndexing' worldType
  )

    
{-# INLINE initialiseIndexingSystem' #-}
initialiseIndexingSystem' :: (WritesMapIndexing' worldType) =>  worldType Storing -> MapIndexingSystem
initialiseIndexingSystem' world =
  let emptySys = newOctree
   in traceEvent "Initialising indexing system" $ MapIndexingSystem $ foldrOf (entitiesWith (withPosition)) (insertEntityIntoIndex entityIsTraversable) emptySys world

{-# INLINEABLE initialiseIndexingSystem #-}
initialiseIndexingSystem :: (WritesMapIndexing worldType m) =>  worldType Storing -> m (worldType Storing)
initialiseIndexingSystem world = do
  putMapIndex $ initialiseIndexingSystem'  world
  logDebug "Map index initialised"
  pure world

{-# INLINEABLE indexEntity #-}
indexEntity :: (WritesMapIndexing worldType m) => worldType Individual -> m ()
indexEntity critter = do
  (MapIndexingSystem sys) <- getMapIndex
  putMapIndex (MapIndexingSystem (insertEntityIntoIndex entityIsTraversable critter sys))

{-# INLINEABLE updateIndexedEntityInfo' #-}
updateIndexedEntityInfo' :: (WritesMapIndexing' worldType) => MapIndexingSystem -> worldType Individual -> worldType Individual -> MapIndexingSystem
updateIndexedEntityInfo' sys oldCritter newCritter = sys & octree %~ removeEntityFromIndex entityIsTraversable oldCritter & octree %~ insertEntityIntoIndex entityIsTraversable newCritter

{-# INLINE updateIndexedEntityInfo #-}

-- | Whenever information that is stored by the index is changed, the change must be added to the indexing system.
updateIndexedEntityInfo ::
  (WritesMapIndexing worldType m) =>
  worldType Individual ->
  worldType Individual ->
  m ()
updateIndexedEntityInfo oldCritter newCritter = do
  when (has name oldCritter) $ do
    logDebug ((renderEntityName oldCritter) <+> "oldCritter:" <+> (viaShow (newEntityTileEntry entityIsTraversable oldCritter)))
    logDebug ((renderEntityName newCritter) <+> "newCritter:" <+> (viaShow (newEntityTileEntry entityIsTraversable newCritter)))
  sys <- getMapIndex
  putMapIndex $ updateIndexedEntityInfo' sys oldCritter newCritter

{-# INLINE positionBlocksMovement' #-}
positionBlocksMovement' :: MapIndexingSystem -> Position -> Blocking
positionBlocksMovement' sys pos = Octree.positionBlocked (sys ^. octree) pos

{-# INLINE positionBlocksMovement #-}
positionBlocksMovement :: (MonadMapIndexing m) => Position -> m Blocking
positionBlocksMovement pos = do
  sys <- getMapIndex
  pure (positionBlocksMovement' sys pos)

{-# INLINE positionHasHiddenEntities' #-}
positionHasHiddenEntities' :: MapIndexingSystem -> Position -> Bool
positionHasHiddenEntities' sys pos = Octree.hiddenEntitiesAtPosition (sys ^. octree) pos

{-# INLINE positionHasHiddenEntities #-}
positionHasHiddenEntities :: (MonadMapIndexing m) => Position -> m Bool
positionHasHiddenEntities pos = do
  sys <- getMapIndex
  pure (positionHasHiddenEntities' sys pos)

{-{-# INLINE positionInitiativePenalty #-}
positionInitiativePenalty :: MapIndexingSystem -> Position -> MovementPenalty
positionInitiativePenalty idxSys pos = Octree.positionInitiativePenalty (idxSys ^. octree) pos
-}

{-# INLINE positionMovementPenalty #-}
positionMovementPenalty :: Position -> Fold MapIndexingSystem (Sum Double)
positionMovementPenalty pos = octree . (Octree.positionMovementPenalty  pos)

{- INLINABLE directPathAdjustedDistance
directPathAdjustedDistance :: MapIndexingSystem -> Position -> Position -> Maybe Double
directPathAdjustedDistance sys a b = -}

{-# INLINEABLE directPathCost #-}
directPathCost :: MapIndexingSystem -> Position -> Position -> Maybe (Seq Position, Double)
directPathCost idxSys pos goal = if completelyBlocked then Nothing else Just (thePath, result)
  where
    (_ :<| (thePath :|> _)) = (directPath pos goal)
    -- anyBlocked = thePath^.folded.(positionBlocksMovement' idxSys)
    completelyBlocked = isInfinite pathObstruction
    pathObstruction = case obstructionOnPath idxSys thePath of
                                     Nothing -> 0
                                     Just ObstructsCompletely -> fromRational infinity
                                     Just (MovementPenalty (Sum pen)) -> pen
    result = distance2D pos goal + pathObstruction
                                     

obstructionOnPath :: Foldable f => MapIndexingSystem -> f Position -> Blocking
obstructionOnPath (MapIndexingSystem idxSys) thePath = thePath ^. folded . to (positionBlocked idxSys)
             
{-# INLINE positionBlocksVision' #-}
positionBlocksVision' :: MapIndexingSystem -> Position -> Bool
positionBlocksVision' sys pos = Octree.positionBlocksVision (sys ^. octree) pos

{-# INLINE positionIsTraversable' #-}                                
positionIsTraversable' :: MapIndexingSystem -> Position -> Bool
positionIsTraversable' sys pos = Octree.positionIsTraversable (sys^.octree) pos
                                
{-# INLINE runIndexingSystem #-}
-- TODO: Create a "position changed" component, and use it to reindex entities
runIndexingSystem :: (Applicative m) => (worldType Storing) -> m (worldType Storing)
runIndexingSystem = pure

{-# INLINE entitiesAtPosition #-}

-- | This is only a valid traversal if you don't directly edit the position. Add a WantsToMove if you want to edit position.
entitiesAtPosition :: (World worldType) => MapIndexingSystem -> Position -> IndexedTraversal' Entity (worldType Storing) (worldType Individual)
entitiesAtPosition sys pos = lookupEntities (sys ^.. entitiesAtPosition' pos)

{-# INLINE entitiesAtPosition' #-}
entitiesAtPosition' :: Position -> Fold MapIndexingSystem Entity
entitiesAtPosition' pos = octree . (Octree.entitiesAtPosition pos)

{-# INLINE entitiesAtPositionByRenderOrder #-}
entitiesAtPositionByRenderOrder :: forall worldType. (UsingRenderable worldType Individual) => MapIndexingSystem -> Position -> worldType Storing -> Seq (worldType Individual)
entitiesAtPositionByRenderOrder idxing pos world = unstableSortOn (view ((singular renderable) . zHeight)) ents
  where
    ents = world & seqOf ((Games.RogueLike.Systems.MapIndexingSystem.entitiesAtPosition idxing pos) . filtered (has renderable))

instance
  ( UsingName worldType Individual,
    UsingHiddenFrom worldType Individual,
    UsingPosition worldType Storing,
    UsingBlocksMovement worldType Individual,
    UsingBlocksVision worldType Individual,
    UsingIsDead worldType Individual,
--    UsingMovementPenalty worldType Individual,
    MonadGameLog m,
    MonadMapIndexing m
  ) =>
  System "MapIndexingSystem" MapIndexingSystem worldType m
  where
  type RunsAfter MapIndexingSystem = '[]
  type RunsBefore MapIndexingSystem = '["MovementSystem"]
  {-# INLINE initialiseSystem #-}
  initialiseSystem _ = initialiseIndexingSystem 
  {-# INLINE runSystem #-}
  runSystem = runIndexingSystem
