module Games.RogueLike.Systems.FactionSystem where

import Control.Lens
import Data.Maybe
import Games.ECS
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Name
import Games.RogueLike.Types.Identifiers (FactionName (FactionName))

-- TODO FIXME: Take into account multiple factions!
{-# INLINEABLE evaluateAllegiances #-}
evaluateAllegiances :: (UsingName worldType Individual, UsingIsFaction worldType Individual) => worldType Storing -> HasAllegiances -> HasAllegiances -> FactionRelation
evaluateAllegiances world mine yours = myFactionOpinions
  where
    myFactionRef = mine ^? allegiances . folded . allegianceFaction
    myFaction = findOf (entitiesWith (withIsFaction <> withName)) (\faction -> myFactionRef == faction ^? name . coerced . to FactionName) world
    yourFaction = yours ^?! allegiances . folded
    myFactionOpinions = case myFaction ^? _Just . isFaction . factionRelations . at yourFaction . _Just of
      Nothing -> FactionRelation (Opinion 0) Ignore
      Just rel -> rel
