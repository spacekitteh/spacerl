{-# OPTIONS_GHC -Werror=unused-imports #-}
module Games.RogueLike.Systems.MovementSystem (MovementSystem (..), applyMovement) where
import Control.Parallel.Strategies.Lens
import Control.Parallel.Strategies    
import Control.Lens
import Control.Monad
import Data.Coerce
import Data.Maybe
import GHC.Generics (Generic)
import Games.ECS
import Games.RogueLike.Components.Combat
import Games.RogueLike.Components.DamagesOnContact
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Name
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Systems.MovementSystem.Pathing
import Games.RogueLike.Types.Damage
import Games.RogueLike.Types.Effects
import Games.RogueLike.Types.SpawnEffects
import Games.RogueLike.Types.PhysicalTypes
-- | Compute the position that an entity which WantsToMove will actually move to if possible.
-- TODO Break pathfinding out into its own system. No reason for it to be in here.
computeNewPosition ::
  ( UsingName worldType Individual,
    UsingWantsToMove worldType Individual,
    UsingPosition worldType Individual,
    DoesPathFinding worldType m,
    UsingGameLog m
  ) =>
  worldType Storing ->
  worldType Individual ->
  m (Maybe Position)
{-# INLINEABLE computeNewPosition #-}
computeNewPosition world critter = case critter ^? wantsToMove of
  Just (WantsToMoveBy (Velocity deltaPosition)) ->
    pure $ Just $ coerce $ (critter ^?! position . coerced) + deltaPosition -- TODO: Take into account multi-step movement rather than just teleporting.
    -- TODO: Hoist these next two cases out into their own AI system.
  Just (WantsToMoveTo pos) -> computeGoalPosition pos
  Just (WantsToMoveToEntity ent) -> computeGoalPosition (fromMaybe (critter ^?! position) (world ^? entity ent . position))
  Nothing -> error "computeNewPosition called with a critter that doesn't WantToMove!"
  where
    computeGoalPosition pos =
      if pos == (critter ^?! position)
        then do
          logDebug ((renderEntityName critter) <+> "is already at the goal!")
          pure Nothing -- We are on our goal!
        else do
          logDebug ((renderEntityName critter) <+> "started pathfinding")
          foundPath <- shortestRoute defaultPathingSearchOptions world (critter ^?! position) pos (Just critter) -- TODO: Take into account "time left"
          case foundPath of
            Nothing -> do
              logDebug ((renderEntityName critter) <+> "couldn't find a path!")
              pure (critter ^? position)
            (Just (_, _ :< next :< _)) -> do
              logDebug ((renderEntityName critter) <+> "finished pathfinding")
              case next of
                Adjacent nextPos -> pure (Just nextPos)
                DirectPath _ (Empty) -> pure Nothing
                DirectPath _ (nextPos :< _rest) -> do
                  logDebug ((renderEntityName critter) <+> "is following a direct path")
                  pure (Just nextPos)
                Waypoint nextPos -> computeGoalPosition nextPos
            Just (_, _cur :< _) -> do
              logDebug ((renderEntityName critter) <+> "is already at the goal!")
              pure Nothing -- We have reached our destination :)
            _ -> error "Empty path returned, instead of failure!"


entityIsTraversable :: UsingIsDead worldType Individual => worldType Individual -> Bool
entityIsTraversable critter =  has isDead critter
                               
{-# INLINEABLE applyMovement #-}
applyMovement ::
  ( WritesMapIndexing worldType m,
    UsingPositionChanged worldType Individual,
    UsingCombatStats worldType Individual,
    UsingDoesBumpDamage worldType Individual,
    UsingIsDead worldType Individual,
    UsingPosition worldType Individual,
    SpawnsEffects m,
    UsingGameLog m
  ) =>
  Position ->
  worldType Individual ->
  m (Maybe (worldType Individual))
applyMovement newPos critter =
  do
   blocks <- positionBlocksMovement newPos
   case blocks of
    Just a -> do
      idxSys <- getMapIndex
      -- TODO FIXME: FIgure out what to actually damage. E.g. don't automatically attack dead stuff.
      let targetedCritters = idxSys ^.. entitiesAtPosition' newPos

      when (has doesBumpDamage critter) $ do
        let damageDealt = fromMaybe (constantDamageOfType "Bump damage" 1 Nothing) (critter ^? combatStats . meleeDamage)
--        assert (critter^?position
        doEffect (critter ^. entityReference) (fmap TargetIndividual targetedCritters) (DoDamage (Damage Nothing damageDealt) (critter ^. entityReference))

      -- BUGBUG FIXME If two critters try to move to the same spot at the same time, they both will. Need to sort this out by binning critters by position, then resolving conflicts
      if (a /= ObstructsCompletely && positionIsTraversable' idxSys newPos ) then do
          Just <$> moveCritter newPos critter
      else do pure Nothing
    _ -> Just <$> moveCritter newPos critter


moveCritter :: (MonadGameLog m, WritesMapIndexing worldType m, UsingPosition worldType Individual, UsingPositionChanged worldType Individual) => Position -> worldType Individual -> m (worldType Individual)
moveCritter newPos critter = do
        let oldPos = critter ^?! position
            updatedCritter = critter & position .~ newPos & addPositionChanged .~ PositionChanged oldPos
        updateIndexedEntityInfo critter updatedCritter

        pure (updatedCritter)

-- | Note that this only gets called to computes damage on /movement/. Pits of acid or w/e won't work with this
-- yet.
computeMovementAndDamage ::
  forall worldType m.
  ( UsingGameLog m,
    UsingDamagesOnContact worldType Individual,
    UsingName worldType Individual,
    UsingWantsToMove worldType Individual,
    UsingPosition worldType Individual,
    DoesPathFinding worldType m,
    SpawnsEffects m
  ) =>
  worldType Storing ->
  worldType Individual ->
  m (Maybe (Position, worldType Individual))
{-# INLINEABLE computeMovementAndDamage #-}
computeMovementAndDamage world critter = do
  mapIndex <- getMapIndex
  newPos' <- computeNewPosition world critter
  case newPos' of
    Nothing -> pure Nothing
    Just newPos -> do
      let damageSources = world ^@.. entitiesAtPosition mapIndex newPos . damagesOnContact
          damage = fmap (\(!ent, DamagesOnContact !d) -> doEffect ent ([TargetIndividual (critter ^. entityReference)]) (DoDamage (Damage Nothing d) ent)) damageSources
      sequence_ damage
      pure (Just (newPos, critter))

{-# INLINEABLE processMovement #-}
processMovement ::
  forall worldType m.
  ( UsingPositionChanged worldType Individual,
    UsingWantsToMove worldType Storing,
    UsingCanMove worldType Storing,
    UsingDamagesOnContact worldType Individual,
    UsingCombatStats worldType Individual,
    UsingDoesBumpDamage worldType Individual,
    WritesMapIndexing worldType m,
    SpawnsEffects m
  ) =>
  (worldType Storing) ->
  m (worldType Storing)
processMovement w = do
  traverseOf (throughout rpar (entitiesWith (withWantsToMove <> withCanMove <> withPosition))) processCritter w
  where
    processCritter critter = do
      maybeMove <- computeMovementAndDamage @worldType w critter
      case maybeMove of
        Nothing -> pure critter
        Just (!newPos, !damaged) -> do -- BUGBUG FIXME If two critters try to move to the same spot at the same time, they both will. Need to sort this out by binning critters by position, then resolving conflicts
          actuallyActed <- applyMovement newPos damaged
          case actuallyActed of
            Nothing -> pure (clean damaged)
            Just moved -> pure (clean moved)

    clean moved =
      case moved ^?! wantsToMove of
        WantsToMoveBy _ -> removeWantsToMove moved
        WantsToMoveTo goal | goal == (moved ^?! position) -> removeWantsToMove moved
        WantsToMoveTo _ -> moved
        WantsToMoveToEntity targetCritter -> case w ^? entity targetCritter . position of
          -- Just targetPos | targetPos /= (moved^?! position) ->  moved -- If reached the target, stop moving.
          Nothing -> removeWantsToMove moved
          _ -> moved -- Always chase down the target, even if we are on it.
          -- _ -> removeWantsToMove moved
          --          WantsToMoveToEntity _ -> moved

data MovementSystem = MovementSystem deriving (Eq, Show, Generic)

instance
  ( UsingPositionChanged worldType Individual,
    UsingWantsToMove worldType Storing,
    UsingName worldType Individual,
    UsingCanMove worldType Storing,
    UsingDamagesOnContact worldType Individual,
    UsingPosition worldType Individual,
    UsingHasVision worldType Individual,
    UsingCombatStats worldType Individual,
    UsingDoesBumpDamage worldType Individual,
    WritesMapIndexing' worldType,
    DoesPathFinding' worldType,
    MonadGameLog m,
    MonadMapIndexing m,
    SpawnsEffects m
  ) =>
  System "MovementSystem" MovementSystem worldType m
  where
  type RunsAfter MovementSystem = '[]
  type RunsBefore MovementSystem = '["VisionSystem"]
  {-# INLINE runSystem #-}
  runSystem world = do
    logSystemPhase "Movement system started"
    processedMovement <- processMovement world
    movementRan <- runOnMovement processedMovement
    logSystemPhase "Movement system finished"
    pure movementRan

{-# INLINE runOnMovement #-}

-- | Set flags, mark things as dirty, etc. No real processing, just flag setting.
runOnMovement :: forall worldType m. (UsingPositionChanged worldType Individual, UsingHasVision worldType Individual, Applicative m) => worldType Storing -> m (worldType Storing)
runOnMovement world = do
  traverseOf (throughout rpar (entitiesWith (withPositionChanged))) processCritter world
  where
    processCritter oldCritter = do
      let oldPos = oldCritter ^?! positionChanged . oldPosition
          newCritter =
            ((removePositionChanged oldCritter) :: worldType Individual)
              & hasVision . viewshed . dirty ?~ OriginMoved -- TODO FIXME: If this critter blocks vision, invalidate the viewsheds it's in
      pure newCritter
