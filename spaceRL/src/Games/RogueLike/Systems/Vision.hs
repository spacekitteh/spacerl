module Games.RogueLike.Systems.Vision (VisionSystem(..), processVision, FOVMethod(..)) where
import Control.Lens
import Control.Exception.Assert.Sugar
import qualified Data.HashSet as HS
import Games.ECS
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Name
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Systems.Vision.DigitalFOV as DFOV
import Games.RogueLike.Types.Coordinates
import Games.RogueLike.Types.SpawnEffects
import qualified Linear.V3 as V3

data FOVMethod = DigitalFOV
                 
                 deriving (Eq, Show)

{-# INLINEABLE computeViewshed2D #-}
computeViewshed2D :: FOVMethod -> WorldCoordinateScalar -> Position -> (Position -> VisuallyClear) -> HS.HashSet Position
computeViewshed2D DigitalFOV r o@(Position origin) predicate = case r of
  0 -> Empty
  1 -> HS.singleton o
  _ | r <= 0 -> Empty
  _ ->
    let mapTransform m = scan (r - 1) predicate (transformView m)
        transformView (x1, y1, x2, y2) bump =
          Position (origin + V3.V3 ((x1 * bump ^. bx) + (y1 * bump ^. by)) ((x2 * bump ^. bx) + (y2 * bump ^. by)) 0)
     in -- Digital FOV scans one quadrant at a time.
        HS.unions
          [ HS.singleton o,
            mapTransform (1, 0, 0, -1), -- One for each quadrant
            mapTransform (0, 1, 1, 0),
            mapTransform (-1, 0, 0, 1),
            mapTransform (0, -1, -1, 0)
          ]

data VisionSystem = VisionSystem deriving (Eq, Show)

{-# INLINE updateViewshed #-}
updateViewshed ::
  (UsingGameLog m, MonadMapIndexing m, UsingHasVision worldType Individual, UsingPosition worldType Individual, UsingName worldType Individual) =>
  worldType Individual ->
  m (worldType Individual)
updateViewshed critter = assert (has hasVision critter `blame` "Tried to updated viewshed for a critter that doesn't have vision!" `swith` (critter ^. entityReference)) do
  logDebug ("Repairing dirty viewshed for" <+> (renderEntityName critter))
  mapIndexingSys <- getMapIndex
  let r = critter ^?! hasVision . visionRange
      origin = critter ^?! position
      predicate = not . (positionBlocksVision' mapIndexingSys) -- TODO: Account for things like different sensor bands
      updatedPositionSet =
        HS.filter
          ( \pos ->
              ((pos ^. x - origin ^. x) * (pos ^. x - origin ^. x))
                + ((pos ^. y - origin ^. y) * (pos ^. y - origin ^. y)) <= (r * r)
          )
          $ computeViewshed2D DigitalFOV r origin predicate
  -- FIXME: Horrid hack to get around the L_infty metric in DFOV
  pure
    ( critter
        & hasVision . viewshed . visiblePositions .~ updatedPositionSet
        & hasVision . viewshed . dirty .~ Nothing
    )

{-# INLINE seeThings #-}

-- | Vision is a sensor; so, sense!
seeThings ::
  ( SpawnsEffects m,
    UsingGameLog m,
    MonadMapIndexing m,
    UsingHasVision worldType Individual
  ) =>
  worldType Storing ->
  worldType Individual ->
  m ()
seeThings world critter =
  -- TODO Get vision sensor
  doEffect (critter ^. entityReference) (HS.map TargetPosition (critter ^?! hasVision . viewshed . visiblePositions)) (RevealHidden (critter ^. entityReference) Seen)

{-# INLINE processVision #-}
processVision :: forall worldType m. (SpawnsEffects m, UsingGameLog m, MonadMapIndexing m, UsingName worldType Individual, UsingHasVision worldType Individual, UsingPosition worldType Individual) => worldType Storing -> m (worldType Storing)
processVision world = do
  logSystemPhase "Vision system started"
  viewshedsMoved <- traverseOf (entitiesWith (withHasVision <> withPosition) . filtered (has (hasVision . viewshed . dirty . _Just))) updateViewshed world
  traverseOf_ (entitiesWith (withHasVision)) (seeThings viewshedsMoved) viewshedsMoved
  logSystemPhase "Vision system finished"
  pure viewshedsMoved

instance (MonadGameLog m, World worldType) => System "VisionSystem" VisionSystem worldType m where
  type ComponentFilters "VisionSystem" VisionSystem worldType m = (SpawnsEffects m, UsingGameLog m, MonadMapIndexing m, UsingName worldType Individual, UsingHasVision worldType Individual, UsingPosition worldType Individual)

  {-# INLINE processPredicate #-}
  processPredicate = has (hasVision . viewshed . dirty . _Just)
  componentFilter = withHasVision <> withPosition
  {-# INLINEABLE processEntity #-}                    
  processEntity = updateViewshed
  postTickCleanup world = do
    traverseOf_ (entitiesWith (withHasVision)) (seeThings world) world
    pure world
