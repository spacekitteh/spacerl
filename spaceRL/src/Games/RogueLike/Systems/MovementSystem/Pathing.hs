{-# OPTIONS_GHC -Werror=unused-imports #-}
module Games.RogueLike.Systems.MovementSystem.Pathing (DoesPathFinding, DoesPathFinding', shortestRoute, PathingSearchOptions (PathingSearchOptions), searchLimits, ignoreGoalBlockingMovement, honeInIfDirectPath, defaultPathingSearchOptions, PathingSearchLimits (PathingSearchLimits), waypointPosition, maxDistance, maxAdjustedDistance, maxDamage, newPathingSearchLimit) where
import Control.Lens

import Control.Monad    
import Control.Exception.Assert.Sugar
import Control.Monad.Search
import Control.Parallel.Strategies
import GHC.Exts (SPEC (..))
import Games.ECS
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Systems.MovementSystem.Pathing.Costs
import Games.RogueLike.Systems.MovementSystem.Pathing.Neighbours
import Games.RogueLike.Systems.MovementSystem.Pathing.Options
    
{- TODO/Ideas
Jump point search is SUPER easy to implement. Do that if needed.

RRT: https://doi.org/10.1145%2F2822013.2822036

Amit Patel's site: http://theory.stanford.edu/~amitp/GameProgramming

return distant waypoints in openNeighbours and using directPathCost in MapIndexingSystem
-}

class
  ( UsingBlocksMovement worldType Individual,
    UsingHiddenFrom worldType Individual
  ) =>
  DoesPathFinding' worldType

instance
  ( UsingBlocksMovement worldType Individual,
    UsingHiddenFrom worldType Individual
  ) =>
  DoesPathFinding' worldType

type DoesPathFinding worldType m = (UsingGameLog m, MonadMapIndexing m, DoesPathFinding' worldType)



{-# INLINEABLE shortestRoute #-}

-- | Compute the shortest route from the first position to the second, possibly taking into account a given entity's movement restrictions and/or a maximum cost.
shortestRoute ::
  forall worldType m.
  (DoesPathFinding worldType m) =>
  -- | The search options.
  PathingSearchOptions ->
  -- | The world.
  worldType Storing ->
  -- | The origin.
  Position ->
  -- | The goal.
  Position -> -- TODO: Make this an arbitrary predicate on positions, perhaps? Or at least, a Representable container of positions.
  -- | Path according to a critter's limited knowledge and abilities.
  Maybe (worldType Individual) ->
  m (Maybe (Double, Path))
shortestRoute options world origin goal critter = do
  idxSys <- getMapIndex
  let res = go idxSys (singletonPath (Adjacent origin)) SPEC
  let best = runSearchBest res
  pure (fmap (\(c, p) -> (c ^. adjustedDistance, p)) best)
  where
    go ::
      MapIndexingSystem ->
      -- The existing path we have found.
      Path ->
      SPEC ->
      Search CostMonoid Path
    go idxSys ls@(restOfPath :> end) !sPEC
      | end ^. waypointPosition == goal = pure ls -- TODO: Allow multiple possible ALTERNATIVE goals (so, no travelling-salesman-problem solving
      -- for us)
      | otherwise =
          do
            let previous = case restOfPath of
                  prevPath :> prev' -> assert (notElemOf (each . (waypointPosition <> (directPathRoute . folded))) goal prevPath `blame` "goal already exists in the path!" `swith` (goal, prevPath)) (Just prev')
                  _ -> Nothing
            -- Get list of neighbours of current node.
            let cells' =
                  ( openNeighbours
                      idxSys
                      world
                      goal
                      previous
                      (end ^. waypointPosition)
                      critter
                      (if options ^. ignoreGoalBlockingMovement then Just goal else Nothing)
                  )
            -- Cost and search each neighbour in parallel.
            msum $ withStrategy (parTraversable rseq) $ fmap mapper cells'
      where
        mapper :: (PathStep, CostMonoid) -> Search CostMonoid Path
        mapper (l, d) = do
          let distToGo = distance2D (l ^. waypointPosition) goal -- FIXME The problem with this is that some tiles may have a faster-than-normal
          -- movement speed. This means that, if
          -- there are any such tiles available
          -- nearby, this simple calculation may be
          -- an overestimate, and therefore breaking
          -- the rule that the heuristic is always an
          -- under-or-perfect estimate; this means
          -- that suboptimal paths may be returned.
          -- Does this path take us over our cost budget?
          if compareCostsToLimits (d & rawDistance +~ distToGo & adjustedDistance +~ distToGo) (options ^. searchLimits) /= LT
            then abandon
            else do
              -- If we have a direct path to the goal, and we are honing in, then don't consider any other options
              when (options ^. honeInIfDirectPath && has directPathRoute l) $ do
                collapse
              -- d is the definitive cost, whereas the latter value is a raw estimate for the A* heuristic
              cost d (Costs distToGo distToGo 0)
              go idxSys (ls :> l) sPEC
    go _ _ !_sPEC = abandon -- Empty path
