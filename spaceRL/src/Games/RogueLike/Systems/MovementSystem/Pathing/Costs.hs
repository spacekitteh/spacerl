
module Games.RogueLike.Systems.MovementSystem.Pathing.Costs where
import Control.Lens
import GHC.Generics    (Generic)
import Control.DeepSeq
import Data.Hashable
import Games.RogueLike.Systems.MovementSystem.Pathing.Options
    
data CostMonoid = Costs
  { _rawDistance :: !Double,
    _adjustedDistance :: !Double,
    _damageSustained :: !Double
  }
  deriving (Eq, Show, Generic)

deriving instance Hashable CostMonoid

deriving instance NFData CostMonoid

makeLenses ''CostMonoid

{-# INLINEABLE compareCostsToLimits #-}
compareCostsToLimits :: CostMonoid -> PathingSearchLimits -> Ordering
compareCostsToLimits costs psl = case compare (costs ^. damageSustained) (psl ^. maxDamage) of
  GT -> GT
  EQ -> EQ
  LT -> case compare (costs ^. adjustedDistance) (psl ^. maxAdjustedDistance) of
    GT -> GT
    EQ -> EQ
    LT -> compare (costs ^. rawDistance) (psl ^. maxDistance)

instance Semigroup CostMonoid where
  {-# INLINE (<>) #-}
  (Costs rda ada dsa) <> (Costs rdb adb dsb) = Costs (rda + rdb) (ada + adb) (dsa + dsb)

instance Monoid CostMonoid where
  {-# INLINEABLE mempty #-}
  mempty = Costs 0 0 0

instance Ord CostMonoid where
  {-# INLINEABLE compare #-}
  compare (Costs rda ada dsa) (Costs rdb adb dsb) = case compare dsa dsb of
    LT -> LT
    GT -> GT
    EQ -> case compare ada adb of
      LT -> LT
      GT -> GT
      EQ -> compare rda rdb
   
