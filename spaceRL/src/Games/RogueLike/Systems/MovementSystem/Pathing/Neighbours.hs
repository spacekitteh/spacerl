
module Games.RogueLike.Systems.MovementSystem.Pathing.Neighbours where
import Control.Lens
import Control.Exception.Assert.Sugar
import Data.Monoid
import Data.Sequence (Seq, unstableSortOn)        
import Data.Sequence.Lens
import Games.ECS
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Systems.MovementSystem.Pathing.Costs    
import Games.RogueLike.Types.PhysicalTypes
    


{-# INLINEABLE openNeighbours #-}
-- TODO: Figure out how to deal with 3d movement. Do floors take up an entire z-layer? Or do we assume that
-- for a floor at (x,y,z), the floor is actually at (x,y,(z-epsilon))?
openNeighbours ::
  forall w.
  (UsingBlocksMovement w Individual,
    UsingHiddenFrom w Individual) =>
  MapIndexingSystem ->
  w Storing ->
  -- | The goal node
  Position ->
  -- | Our previous position in the path, if it exists
  Maybe PathStep ->
  -- | The position whose neighbours we are trying to find
  Position ->
  -- | The critter who is trying pathing
  Maybe (w Individual) ->
  -- | If we trying to reach another critter, for example, and the goal critter blocks movement, then A* will
  -- never find a path to the critter itself. This parameter says "always consider this position unblocked".
  Maybe Position ->
  (Seq (PathStep, CostMonoid))
openNeighbours idxSys world goal previousPos pos mBM mbGoalAlwaysUnblocked =
  let positions =
        -- TODO: If check if the critter can move on the terrain type
        seqOf
          (neighbourPositions . confusing (filtered (isSameDirection previousPos) . filtered (\pos' -> (pos ^. z == pos' ^. z)) . filtered (not . (positionIsBlocked mBM))) . to adjacentCost)
          pos
      -- TODO: Take care of different blocking amounts (e.g. by size)
      adjacent = unstableSortOn snd positions
      -- If we can directly path to the goal in a straight line, we add that cost here
      result = case directPathCost idxSys pos goal of
        Nothing -> adjacent
        Just (directPath, directCost) -> ((DirectPath goal directPath), Costs (distance2D pos goal) directCost 0) :< adjacent
   in result
  where
    isSameDirection :: Maybe PathStep -> Position -> Bool
    isSameDirection _ p | p == goal = True
    isSameDirection (Just (Adjacent oldPos)) p = (pastMovementDir ^. x) * horizontalDirection >= 0 || (newMovementDir ^. y) * verticalDirection >= 0 -- If it's in the 270 degree cone of movement. If it was &&, it would be 90, and would rule out skipping around obstacles, right?
      where
        horizontalDirection = newMovementDir ^. x
        verticalDirection = newMovementDir ^. y
        newMovementDir = p - pos
        pastMovementDir = pos - oldPos
    isSameDirection _ _ = True
    adjacentCost :: Position -> (PathStep, CostMonoid)
    -- TODO FIXME: Include HP cost instead of just 0
    adjacentCost pos' = (Adjacent pos', Costs (distance2D pos pos') (movementPenaltyToInitiativeCost (getSum $ idxSys ^. (positionMovementPenalty pos')) * distance2D pos pos') 0)

    -- Figure out if the position is blocked. If we are passed a critter, then we figure out if the critter
    -- /knows/ whether the position is blocked, because we don't assume omniscience.
    -- TODO: Break this out into its own general function
    positionIsBlocked :: Maybe (w Individual) -> Position -> Bool
    positionIsBlocked _ pos' | Just pos' == mbGoalAlwaysUnblocked = False
    positionIsBlocked Nothing pos' = positionBlocksMovement' idxSys pos' == Just ObstructsCompletely
    positionIsBlocked (Just critter) pos' =
      let blocks = positionBlocksMovement' idxSys pos'
          hidden = positionHasHiddenEntities' idxSys pos'
       in case (blocks, hidden) of
            (Nothing, _) -> False
            (Just ObstructsCompletely, False) -> True
            (Just ObstructsCompletely, True) ->
              let allBlockers = (entitiesAtPosition idxSys pos') . filtered (has blocksMovement)
                  knownBlockers = allBlockers . filtered (notElemOf (hiddenFrom . victims . knownEntities) (critter ^. entityReference))
               in has (confusing knownBlockers) world
            (Just _, _) -> False
