{-# OPTIONS_GHC -Werror=unused-imports #-}
module Games.RogueLike.Systems.MovementSystem.Pathing.Options where
import Control.Lens
import GHC.Generics    (Generic)
import Data.Hashable
    
data PathingSearchLimits = PathingSearchLimits
  { _maxDistance :: !Double,
    _maxAdjustedDistance :: !Double,
    _maxDamage :: !Double
  }
  deriving (Eq, Show, Generic)

deriving instance Hashable PathingSearchLimits

makeLenses ''PathingSearchLimits

{-# INLINEABLE newPathingSearchLimit #-}

-- | A new pathing search limit, with maximum costs set to infinity; this means that you can set just the things you care about.
newPathingSearchLimit :: PathingSearchLimits
newPathingSearchLimit = PathingSearchLimits (recip 0) (recip 0) (recip 0)




data PathingSearchOptions = PathingSearchOptions
  { _searchLimits :: !PathingSearchLimits,
    _ignoreGoalBlockingMovement :: !Bool,
    _honeInIfDirectPath :: !Bool
  }
  deriving (Eq, Show, Generic)

deriving instance Hashable PathingSearchOptions

makeLenses ''PathingSearchOptions

{-# INLINEABLE defaultPathingSearchOptions #-}
defaultPathingSearchOptions :: PathingSearchOptions
defaultPathingSearchOptions = PathingSearchOptions {_searchLimits = newPathingSearchLimit, _ignoreGoalBlockingMovement = True, _honeInIfDirectPath = True}    
