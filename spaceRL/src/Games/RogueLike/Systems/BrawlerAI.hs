module Games.RogueLike.Systems.BrawlerAI (BrawlerAISystem(..), runBrawlerAI) where
import Control.Lens
import Control.Monad    
import Data.Maybe
import Data.Sequence
import Games.ECS
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Awareness    
import Games.RogueLike.Components.BrawlerAI
import Games.RogueLike.Components.Combat
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Name    
import Games.RogueLike.Systems.FactionSystem
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.Damage    
import Games.RogueLike.Types.Effects
import Games.RogueLike.Types.SpawnEffects

data BrawlerAISystem = BrawlerAISystem

{-# INLINEABLE brawl #-}
brawl :: (UsingGameLog m, SpawnsEffects m,UsingName worldType Individual,  UsingCombatStats worldType Individual, UsingIsFaction worldType Individual, UsingWantsToMove worldType Individual, UsingPosition worldType Individual, UsingHealthStatus worldType Individual, UsingHasAllegiances worldType Individual) => worldType Storing -> worldType Individual -> m ()
brawl world critter = do
  {-
  Hacky as fuck. We look for a WantsToMoveToEntity component. If we are standing next to the target, and the
  target has a health status, and the faction relations say "Attack", then we attack. This is really "do bump damage".

  TODO FIXME: Instead of directly pulling "hasAllegiances", use the /observed/ allegiances.
  -}
  let  myFactions = critter ^? hasAllegiances
  let target' = critter ^?  (wantsToMove . goalEntity)
                
  when (isJust target') $ do
    checkEntityExists world (fromJust target')
    let target = world ^?! entity (fromJust target')
        targetFactions = target ^? hasAllegiances
    when (has healthStatus target && target ^?! healthStatus . health > 0 && isJust myFactions && isJust targetFactions) $ do
      let feelings = evaluateAllegiances world (fromJust myFactions) (fromJust targetFactions)
      when
        ( feelings ^. reaction == Attack
            && has position target
            && (Data.Sequence.length (directPath (critter ^?! position) (target ^?! position))) <= 2
        )
        do
          meleeAttack critter (fromJust target')

          
chase' :: (UsingWantsToMove worldType Individual, UsingHasAllegiances worldType Individual, UsingHasObjectPermanence worldType Individual, UsingIsFaction worldType Individual, UsingName worldType Individual) =>  worldType Storing -> worldType Individual ->  Maybe (Entity, Entity)
chase' world critter = do
  case critter^?wantsToMove of
    Just _ -> Nothing
    Nothing -> do
      let myFactions = critter ^? hasAllegiances
          target = critter^?(hasObjectPermanence
                . observationsByEntity
                . folded
                . filtered (\(obs :: Observation) ->
                   has (observedAllegiance
                       . _Just
                       . to (\allegs -> evaluateAllegiances world (fromJust myFactions) (HasAllegiances allegs mempty))
                       . reaction
                       . filtered (== Attack))
                   obs)
                . observedEntity) 
      case target of
            Nothing -> Nothing
            Just target' ->  pure (critter^.entityReference, target')
chase :: (UsingWantsToMove worldType Individual, UsingHasAllegiances worldType Individual, UsingHasObjectPermanence worldType Individual, UsingIsFaction worldType Individual, UsingName worldType Individual, UsingBrawlerAI worldType Individual ) => worldType Storing -> worldType Storing
chase world =
    let crittersToUpdate = world^..entitiesWith (withBrawlerAI <> withHasAllegiances) . to (chase' world) . _Just
    in  foldr (\ (critter, target) world' -> world' & entity critter . addWantsToMove .~ WantsToMoveToEntity target) world crittersToUpdate

-- | Attack a targeted entity with melee.
{-# INLINEABLE meleeAttack #-}          
meleeAttack :: (HasCombatStats worldType, UsingGameLog m, SpawnsEffects m) => worldType Individual -> Entity -> m ()
meleeAttack attackingCritter targetedCritter = do
  -- TODO: Need to get a better way to "get melee attack stats" 
  let damageDealt = fromMaybe (constantDamageOfType "Melee damage" 1 Nothing) (attackingCritter ^? combatStats . meleeDamage)
  logDebug "Doing damage!"
  doEffect (attackingCritter ^. entityReference) [TargetIndividual (targetedCritter)] (DoDamage (Damage Nothing damageDealt) (attackingCritter ^. entityReference))
  
                   
{-# INLINEABLE runBrawlerAI #-}
runBrawlerAI :: (SpawnsEffects m, MonadGameLog m, UsingName worldType Individual, UsingCombatStats worldType Individual, UsingIsFaction worldType Individual, UsingWantsToMove worldType Individual, UsingPosition worldType Individual, UsingBrawlerAI worldType Individual, UsingHealthStatus worldType Individual,UsingHasObjectPermanence worldType Individual,  UsingHasAllegiances worldType Individual) => worldType Storing -> m (worldType Storing)
runBrawlerAI world = do
  let updated' = chase world
  forMOf_ (entitiesWith (withBrawlerAI <> withHasAllegiances <> withCombatStats  <>   withPosition)) updated' (brawl updated')
  pure updated'
