module Games.RogueLike.Systems.AwarenessSystem (AwarenessSystem (..), RevealsByObservers(..), MonadReveals (..)) where
import Control.Exception.Assert.Sugar
import Control.Monad    
import Control.Lens    

import GHC.Exts (toList, fromList)
--import qualified Data.HashMap.Strict as HMS
import Data.Set as DSet
import qualified Data.HashSet as HS
import qualified Data.Sequence as DS
import Games.ECS
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.Name
import Games.RogueLike.Components.Renderable
import Games.ECS.Entity.EntityMap as EM
import Games.ECS.Entity.EntitySet as ES    
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Types.RNGStore
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.Tick
import Games.RogueLike.Types.Effects
import Polysemy
import Polysemy.State
--import Control.Monad.State
data AwarenessSystem = AwarenessSystem deriving (Eq)

newtype RevealsBySneaks = RevealsBySneaks (EntityMap (Set (Entity, ObservationReason)))

newtype RevealsByObservers = RevealsByObservers (EntityMap (Set (Entity, ObservationReason))) deriving Eq

class Monad m => MonadReveals m where
  getRevealsByObservers :: m RevealsByObservers
  putRevealsByObservers :: RevealsByObservers -> m ()

instance (Member (State RevealsByObservers) r) => MonadReveals (Sem r) where
  {-# INLINE getRevealsByObservers #-}
  getRevealsByObservers = get
  {-# INLINE putRevealsByObservers #-}
  putRevealsByObservers = put

-- | Collects all of the reveals from the nowAwareOf components, and returns two maps:
-- a) RevealsBySneaks is a map from newly-detected objects to the set of entities that are newly aware of them, and
-- b) RevealsByObservers is a map from observers to the sets of entities they are now aware of.
--
-- This is also where detection given a detection probability is resolved.
collectNewlyAwareOf ::
  ( UsingNowAwareOf worldType Individual,
    UsingHiddenFrom worldType Individual,
    RollsDice m
  ) =>
  worldType Storing ->
  m (RevealsBySneaks, RevealsByObservers)
{-# INLINEABLE collectNewlyAwareOf #-}
collectNewlyAwareOf world = do
  let critters = world ^@.. entitiesWith withNowAwareOf <. nowAwareOf . potentialRevelations . folded -- The list of /potential/ revelations, indexed by the critter that made them
  culled <- filterM willReveal critters
  let sneakKvs = fmap (\(smarty, (sneakyBoi, reason)) -> (sneakyBoi, DSet.singleton (smarty, reason))) culled
      observerKvs = fmap (\(observer, (sneakyBoi, reason)) -> (observer, DSet.singleton (sneakyBoi, reason))) culled
  pure (RevealsBySneaks $ fromListWith (<>) sneakKvs, RevealsByObservers $ fromListWith (<>) observerKvs)
  where
    -- Here we deal with actual reveal logic. Here, either the sneaky boi isn't actually hidden from anyone,
    -- or isn't hidden from the victim; therefore reveal
    willReveal (victim, (sneakyBoi, reason)) | hasn't (entity sneakyBoi . hiddenFrom . victims . ix victim ) world = pure True
                                                                                                                     
    -- TODO: How to implement faction revelations? Will it be a random faction of the victim that is removed
    -- from the hidden , or all factions? Need to figure that out before I add hiding from factions.
                                                                                                                     
    -- Forces a reveal
    willReveal (_victim, (_sneakyBoi, BecauseISaySo)) = pure True
    -- The sneaky boi is hidden from the victim, and the observation made is one of the reveal reasons for
    -- the sneaky boi; so we get the challenge to beat. If we beat the challenge, then the victim is
    -- revealed.
    willReveal (_victim, (sneakyBoi, reason)) | elemOf (entity sneakyBoi . revealsOnObservationTypes) reason world = do
      -- TODO optimise this so we don't traverse twice
      let challengeToBeat = world ^?! entity sneakyBoi . hiddenFrom . revealsOnInteraction . revealConditionMap . at reason . _Just . revealDifficulty
      score <- roll "Reveal hidden" (challengeToBeat ^. diceRoll)
      pure (score >= (challengeToBeat ^. scoreToBeat))

    
    -- The victim didn't observe the sneaky boi in a correct observation method, so the sneaky boi isn't revealed.
    willReveal _ = pure False

-- | Process entity sensor data into a cohesive whole.
processAwareness ::
  forall worldType m.
  (
    UsingGameLog m,
    WritesMapIndexing worldType m,
    RollsDice m,
    MonadReveals m,
    MonadGameTime m, 
    UsingHasVision worldType Individual,
    UsingNowAwareOf worldType Individual,
    UsingRenderable worldType Individual,
    UsingHasAllegiances worldType Individual,
    UsingHasObjectPermanence worldType Individual
  ) =>
  worldType Storing ->
  m (worldType Storing)
processAwareness world = do
  logSystemPhase "Awareness system started"
  
  (RevealsBySneaks revealsBySneaks, RevealsByObservers revealsByObservers) <- collectNewlyAwareOf world
  RevealsByObservers existingReveals <- getRevealsByObservers
  putRevealsByObservers  (RevealsByObservers (unionWith DSet.union existingReveals revealsByObservers))
  logDebug "Processing sneaks"

  
  worldWithProcessedSneaks <- -- Remove "Hidden from" status
    forMOf
      (lookupEntities ((keysSet revealsBySneaks) ^..getEntityReferences ))
      world
      ( \oldCritter -> do
          -- Remove "hidden" status from oldCritter
          let theEntityRef = oldCritter ^. entityReference
              reveals = DSet.map fst (revealsBySneaks ! theEntityRef)
          if DSet.null reveals
            then pure oldCritter
            else do
              let critter = oldCritter & hiddenFrom . victims %~ (`ES.difference` (ES.fromDistinctAscList . DSet.toAscList $ reveals))
              updateIndexedEntityInfo oldCritter critter
              when (has name critter) $ logDebug ((renderEntityName critter) <+> "was discovered by" <+> (pretty (show (revealsBySneaks ! theEntityRef))))
              if has hiddenFrom critter && hasn't (hiddenFrom . victims . knownEntities) critter
                then pure (critter & removeHiddenFrom)
                else pure critter
      )

  pure worldWithProcessedSneaks


{-# INLINEABLE processAwareness #-}

instance (MonadMapIndexing m, MonadGameLog m, MonadGameTime m, MonadReveals m, WritesMapIndexing' worldType, UsingHasObjectPermanence worldType Individual, UsingHiddenFrom worldType Individual, UsingNowAwareOf worldType Individual, UsingNowAwareOf worldType Storing, UsingRenderable worldType Individual, UsingHasAllegiances worldType Individual, UsingName worldType Individual, UsingHasVision worldType Individual, RollsDice m) => System "AwarenessSystem" AwarenessSystem worldType m where
  type RunsAfter AwarenessSystem = '[]
  type RunsBefore AwarenessSystem = '[]

  {-# INLINE runSystem #-}
  runSystem = processAwareness
  {-# INLINABLE runAfterEffects #-}
  runAfterEffects world = do
    logDebug "Processing clued-in-on"
    RevealsByObservers revealsByObservers <- getRevealsByObservers
    checkEntitiesExist world (setOf getEntityReferences (keysSet revealsByObservers))
    gameTime <- getGameTime
    -- | Add object permanence and remove "nowAwareOf"    
    updatedWorld <- forMOf
      (lookupEntities ((keysSet revealsByObservers)^..getEntityReferences))
      world
      ( \oldObserver -> do
          let observationPairs  = GHC.Exts.toList $ revealsByObservers ! (oldObserver ^. entityReference)
              sequencedPairs = fmap (\(a, b) -> (a, DS.singleton b)) observationPairs
              mergedPairs :: EntityMap (DS.Seq ObservationReason) = fromListWith (<>) sequencedPairs
              
          checkEntitiesExist world (GHC.Exts.fromList $ fmap fst observationPairs) 
          
          let observations = fmap (\(object, reason) -> observeEntity gameTime oldObserver (world ^?! entity object) reason) ((GHC.Exts.toList mergedPairs) :: [(Entity, DS.Seq ObservationReason)])
              clearedObserver = oldObserver & hasObjectPermanence %~ clearObservedPositions (oldObserver ^.. entityFieldOfView)
              wiserCritter = clearedObserver & hasObjectPermanence %~ insertObservations observations
          pure (wiserCritter & nowAwareOf .~ blankNowAwareOf)
      )
    putRevealsByObservers (RevealsByObservers emptyEntityMap)
    logSystemPhase "Awareness system finished"
    pure updatedWorld

