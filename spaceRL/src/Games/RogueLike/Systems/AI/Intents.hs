-- |
-- Module      :  Games.RogueLike.Systems.AI.Intents
-- Description : AI intent system.
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- When AI subsystems want to present multiple possible options, it must propose an action via an t'Intent'.
--
--


-- This system will naturally depend on many components, and therefore be rebuilt regularly. Thus, try to
-- minimise compilation time.

-- The flow for how actions are chosen by the AI is roughly as follows:
--  
--        AI capability evaluation
--                system
--                  
--                   |
--                   |
--           sets intent flag
--                   |
--                   |
--                   v
--                     
--          AI intent evaluation
--                system
--            
--                   |
--                   |
--             pick intent to
--                act on
--                   |                  
--                   |
--                   v
--
--       Relevant capability system 
--


module Games.RogueLike.Systems.AI.Intents where


import Games.ECS


data IntentEvaluationSystem = IntentEvaluationSystem    
