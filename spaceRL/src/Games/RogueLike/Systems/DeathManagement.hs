module Games.RogueLike.Systems.DeathManagement (DeathProcessingSystem (..)) where
import Control.Lens
import Games.ECS
import Games.RogueLike.Components.BrawlerAI    
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Name
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Components.Awareness
data DeathProcessingSystem = DeathProcessingSystem deriving (Eq)

{-# INLINEABLE removeWantsToIntelligentMove #-}
-- TODO: Put this into the AI system, to allow for turning undead while keeping consciousness; or perhaps just directly add IsDead?
removeWantsToIntelligentMove ::
  forall worldType.
  (UsingWantsToMove worldType Individual) =>
  worldType Individual ->
  worldType Individual
removeWantsToIntelligentMove critter =
  case critter ^? wantsToMove @worldType of
    Just (WantsToMoveBy _) -> critter
    Just _ -> critter & removeWantsToMove
    Nothing -> critter

instance
  ( MonadGameLog m,
    UsingName worldType Individual,
    UsingJustDied worldType Storing,
    UsingIsDead worldType Individual,
    UsingWantsToMove worldType Individual,
    UsingHasObjectPermanence worldType Individual,
    UsingNowAwareOf worldType Individual,
    UsingBrawlerAI worldType Individual
    
  ) =>
  System "DeathProcessingSystem" DeathProcessingSystem worldType m
  where
  type RunsAfter DeathProcessingSystem = '[]
  type RunsBefore DeathProcessingSystem = '[]
  type
    ComponentFilters "DeathProcessingSystem" DeathProcessingSystem worldType m =
      ( MonadGameLog m,
        UsingName worldType Individual,
        UsingJustDied worldType Storing,
        UsingIsDead worldType Individual,
        UsingWantsToMove worldType Individual,
        UsingBrawlerAI worldType Individual,
        UsingHasObjectPermanence worldType Individual
      )
  componentFilter = withJustDied
  processEntity critter = do
    logMessage ((renderEntityName critter) <+> (pretty ("was" :: String)) <+> (renderJustDied $ critter ^?! justDied))
    pure (critter
         & removeWantsToIntelligentMove
         & removeJustDied
         & removeHasObjectPermanence
         & removeNowAwareOf
         & removeBrawlerAI
         & addIsDead .~ IsDead)
