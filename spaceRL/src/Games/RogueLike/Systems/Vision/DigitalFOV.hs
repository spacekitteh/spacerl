{-# OPTIONS_GHC "-funbox-small-strict-fields" #-}
module Games.RogueLike.Systems.Vision.DigitalFOV (scan, type VisuallyClear, bx, by) where
import Control.Lens
import qualified Data.List.NonEmpty as DLNE
import qualified Data.HashSet as DS
import Games.ECS
import Games.RogueLike.Types.Coordinates
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Systems.MapIndexingSystem
import Control.Exception
--import Control.Exception.Assert.Sugar

-- Shamelessly adapted from https://github.com/LambdaHack/LambdaHack/blob/master/engine-src/Game/LambdaHack/Server/FovDigital.hs


-- | Distance from the (0,0) point where FOV originates
type Distance = WorldCoordinateScalar

-- | Progress along an arc with a constant distance from (0,0)
type Progress = WorldCoordinateScalar

-- | Rotated and translated coordinates of 2D points, so that the points fit
-- in a single quadrant area (e, g., quadrant I for Permissive FOV, hence both
-- coordinates positive; adjacent diagonal halves of quadrant I and II
-- for Digital FOV, hence y positive).
-- The special coordinates are written using the standard mathematical
-- coordinate setup, where quadrant I, with x and y positive,
-- is on the upper right.
data Bump = Bump
  { _bx :: !WorldCoordinateScalar
  , _by :: !WorldCoordinateScalar
  }
  deriving Show
makeLenses ''Bump

-- | Two strict orderings of lines with a common point.
data LineOrdering = Steeper | Shallower

-- | Straight line between points.
data Line = Line {_lineOrigin :: !Bump, _lineEnd :: !Bump}
  deriving Show
makeLenses ''Line
-- | Convex hull represented as a non-empty list of points.

newtype ConvexHull = ConvexHull {_theList :: DLNE.NonEmpty Bump} deriving (Show)
makeLenses ''ConvexHull
{-# INLINE newConvexHull #-}
newConvexHull :: Bump -> ConvexHull
newConvexHull b = ConvexHull (b DLNE.:| [])

{-# INLINE bumps #-}
bumps :: Fold ConvexHull Bump
bumps = theList . folded



-- | Strictly compare steepness of lines @(b1, bf)@ and @(b2, bf)@,
-- according to the @LineOrdering@ given. This is related to comparing
-- the slope (gradient, angle) of two lines, but simplified wrt signs
-- to work fast in this particular setup.
steepness :: LineOrdering -> Bump -> Bump -> Bump -> Bool
{-#INLINE steepness #-}
steepness lineOrdering bf b1 b2 = result where
  y2x1 = (bf^.by - b2^.by) * (bf ^.bx - b1^.bx)
  y1x2 = (bf^.by - b1^.by) * (bf^.bx - b2^.bx)
  result =  case lineOrdering of
    Steeper -> y2x1 > y1x2
    Shallower -> y2x1 < y1x2

steepestInHull :: LineOrdering -> Bump -> ConvexHull -> Bump
{-# INLINE steepestInHull #-}
steepestInHull lineOrdering newBump hull = foldl1Of' bumps maximumBump hull where
  maximumBump x' y' = if steepness lineOrdering newBump x' y' then x' else y'

-- | Extends a convex hull of bumps with a new bump. The new bump makes
-- some old bumps unnecessary, e.g. those that are joined with the new steep
-- bump with lines that are not shallower than any newer lines in the hull.
-- Removing such unnecessary bumps slightly speeds up computation
-- of 'steepestInHull'.
--
-- Recursion in @addToHullGo@ seems spurious, but it's called each time with
-- potentially different comparison predicate, so it's necessary.
addToHull :: LineOrdering  -- ^ the line ordering to use
          -> Bump          -- ^ a new bump to consider
          -> ConvexHull    -- ^ a convex hull of bumps represented as a list
          -> ConvexHull
{-# INLINE addToHull #-}
addToHull lineOrdering new oldCH =
  ConvexHull (new DLNE.:| culled) where
  culled :: [Bump]
  culled = cull (oldCH^..bumps)
  cull :: [Bump] -> [Bump]
  cull (a : remaining@(b : _)) | not (steepness lineOrdering new b a) = cull remaining
  cull remaining = remaining


{-# INLINE intersect #-}
intersect :: Line -> Distance -> (WorldCoordinateScalar, WorldCoordinateScalar)
intersect l d = ((d - l^.lineOrigin.by) * (l^.lineEnd.bx - l^.lineOrigin.bx) + (l^.lineOrigin.bx * (l^.lineEnd.by - l^.lineOrigin.by)), l^.lineEnd.by - l^.lineOrigin.by)

-- | An edge (comprising of a line and a convex hull) of the area to be scanned.
data Edge = Edge {_theLine :: Line, _theConvexHull :: ConvexHull } deriving (Show)

-- | The contiguous area left to be scanned, delimited by edges.
data EdgeInterval = EdgeInterval {_shallowEdge :: Edge, _steepEdge ::  Edge} deriving Show


type VisuallyClear = Bool

scan :: Distance -> (Position -> VisuallyClear) -> (Bump -> Position) -> DS.HashSet Position
{-# INLINABLE scan #-}
scan range isClear coordTransform = dscan 1
  -- | TODO: Un-L_infty. This is a circle in the L_infty metric, but a square in L_2.
  (EdgeInterval
   (Edge (Line (Bump 1 0) (Bump (-range) range)) (newConvexHull (Bump 0 0)))
   (Edge (Line (Bump 0 0) (Bump (range+1) range)) (newConvexHull (Bump 1 0))))
  where

    dscan :: Distance -> EdgeInterval -> DS.HashSet Position
    dscan d (EdgeInterval (Edge shallowLine shallowHull) steep@(Edge steepLine steepHull)) = outsideCells where
      minimalProgress = let (n,k) = intersect shallowLine d in n `div` k
      maximalProgress = let (n,k) = intersect steepLine d in  (-1 + n `divUp` k) 
      divUp n k = (n + k - 1) `div` k
      infixl 7 `divUp`
      outsideCells =
        if d < range
          then
            let transformedBump = bump minimalProgress in
              if isClear transformedBump
              then DS.insert transformedBump (mscanVisible shallowLine shallowHull (minimalProgress + 1))
              else DS.insert transformedBump (mscanShadowed (minimalProgress + 1))
          else
            DS.fromList $ map bump  [minimalProgress .. maximalProgress]

      bump :: Progress -> Position
      bump progress = coordTransform (Bump progress d)

      mscanVisible :: Line -> ConvexHull -> Progress -> DS.HashSet Position
      mscanVisible line hull = goVisible where
        goVisible :: Progress -> DS.HashSet Position
        goVisible ps =
          if ps <= maximalProgress
          then
            let transformedBump = bump ps in
              if isClear transformedBump
              then DS.insert transformedBump (goVisible (ps+1))
              else
                let steepBump = Bump ps d
                    nep = steepestInHull Shallower steepBump hull
                    neLine = Line nep steepBump
                    neHull = addToHull Shallower steepBump steepHull
                in
                  DS.union
                    (DS.insert transformedBump (dscan (d+1) (EdgeInterval (Edge line hull) (Edge neLine neHull))))
                    (mscanShadowed (ps+1))

          else dscan (d+1) (EdgeInterval (Edge line hull) steep)
     
      mscanShadowed :: Progress -> DS.HashSet Position
      mscanShadowed ps =
        if ps <= maximalProgress
        then
          let transformedBump = bump ps in
          if not (isClear transformedBump)
          then DS.insert transformedBump (mscanShadowed (ps+1))
          else
            let shallowBump = Bump ps d
                nsp = steepestInHull Steeper shallowBump steepHull
                nsLine = Line nsp shallowBump
                nsHull = addToHull Steeper shallowBump shallowHull
            in DS.insert transformedBump (mscanVisible nsLine nsHull (ps+1))              
        else DS.empty


