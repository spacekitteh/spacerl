{-# LANGUAGE CPP #-}
{-# LANGUAGE StrictData #-}
module Games.RogueLike.Systems.GameLog (module Games.RogueLike.Systems.GameLog, module Games.RogueLike.Types.Message) where
import GHC.Generics
import Control.Lens    
import Data.Foldable
import Data.Sequence (Seq, empty)
import Debug.Trace
import GHC.Stack
import Games.ECS
import Games.RogueLike.Types.Message
import Polysemy
import Polysemy.State
import Prettyprinter
import Control.DeepSeq
#ifndef DEBUG
import Control.Monad (void)
#endif
-- TODO: Implement a verbosity filter. Allow multiple debug centres.
data GameLog = GameLog {_messages :: Seq Message, _debugMessages :: Seq Message, _allMessages :: Seq Message} deriving stock (Show, Generic)
             deriving anyclass NFData

makeLenses ''GameLog

-- TODO: Implement a debug levels system
#ifndef ASSERTS
{-# RULES "yeet logAssert" forall a b. logAssert a b = pure ()  #-}
#endif

{-# NOINLINE logAssert #-}
-- TODO: Make this save the log before crashing
logAssert :: UsingGameLog m => Message -> Bool -> m ()
logAssert _ True = pure ()
logAssert msg False = withFrozenCallStack $ do
  logError msg
  error (show msg)

{-# INLINE checkEntitiesExist #-}
checkEntitiesExist :: (World worldType, UsingGameLog m) => worldType Storing -> EntitySet -> m ()
checkEntitiesExist world ents = withFrozenCallStack $ mapMOf_ knownEntities (checkEntityExists world) ents

{-# INLINE checkEntityExists #-}
checkEntityExists :: (World worldType, UsingGameLog m) => worldType Storing -> Entity -> m ()
checkEntityExists world ent = withFrozenCallStack $ logAssert ("Entity which should exist, doesn't!" <+> renderBareEntity ent) (has (entity ent) world)

newGameLog :: GameLog
newGameLog = GameLog empty empty empty

{-# INLINEABLE logError #-}
logError :: UsingGameLog m => Message -> m ()
logError message = do
  let stackTrace = withFrozenCallStack (prettyCallStack callStack)
  let newMsg = annotate (LogVerbosity (Error stackTrace)) message
  (GameLog gameLog dbg allMsgs) <- getGameLog
  putGameLog $! traceMarker "ERROR" $ traceEvent ((show message) ++ stackTrace) $ GameLog gameLog (dbg :> newMsg) (allMsgs :> newMsg)

#ifndef DEBUG
{-# RULES "yeet logDebug" logDebug = const (pure ())#-}
#endif 
             
{-# INLINEABLE[2] logDebug #-}
logDebug :: UsingGameLog m => Message -> m ()
logDebug !message = do
  let area = force $ foldMap (\(site, loc) -> site ++ (prettySrcLoc loc)) $ getCallStack $ popCallStack callStack
  gl@(GameLog gameLog dbg allMsgs) <- getGameLog
  let newMsg = annotate (force $! LogVerbosity (Debug $ force $! area)) message
  putGameLog $! traceEvent (show $! pretty area <> ":" <+> message) $ gl{_debugMessages = (dbg :> newMsg), _allMessages =  (allMsgs :> newMsg)}

{-# INLINEABLE logMessage #-}
logMessage :: (UsingGameLog m) => Message -> m () --Sem r ()
logMessage message = do
  gl@(GameLog gameLog dbg allMsgs) <- getGameLog
  putGameLog $ gl{_messages =  (gameLog :> message), _allMessages =  (allMsgs :> message)}

{-# INLINEABLE logObject #-}
logObject :: (Pretty a, UsingGameLog m) => a -> m ()
logObject = do
  logMessage . pretty

{-# INLINEABLE logSystemPhase #-}
logSystemPhase :: UsingGameLog m => Message -> m ()
logSystemPhase msg = do
  let newMsg = annotate (LogVerbosity (Debug "System phase")) msg
  gl@(GameLog gameLog dbg allMsgs) <- getGameLog
  putGameLog $ traceMarker (show msg) $ gl{_debugMessages = (dbg :> newMsg), _allMessages =  (allMsgs :> newMsg)}

type UsingGameLog m = (HasCallStack, MonadGameLog m)

type UsingGameLog' r = MonadGameLog (Sem r)

class Monad m => MonadGameLog m where
  getGameLog :: m GameLog
  putGameLog :: GameLog -> m ()

instance (Member (State GameLog) r) => MonadGameLog (Sem r) where
  {-# INLINE getGameLog #-}
  getGameLog = get @GameLog
  {-# INLINE putGameLog #-}
  putGameLog = put @GameLog

instance (World worldType, MonadGameLog m) => System "GameLog" GameLog worldType m where
  type RunsAfter GameLog = '[]
  type RunsBefore GameLog = '[]
  {-# INLINE initialiseSystem #-}
  initialiseSystem gl input = do
    putGameLog gl
    pure input
