module Games.RogueLike.Systems.EffectSystem (runEffectsSystem, EffectsToProcess (..)) where
import Control.Lens
import Control.DeepSeq    
import Control.Monad.IO.Class
import Games.RogueLike.Components.BrawlerAI    
import Control.Monad    
import qualified Data.HashSet as HS
import Data.HashSet.Lens (setOf)
import qualified Games.ECS.Entity.EntitySet as ES (setOf)
import Data.Sequence (Seq)
import qualified Data.Set.Ordered as DSO (delete, singleton, (<|))
import Games.ECS
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Awareness
  ( HasHasObjectPermanence (hasObjectPermanence),
    HasNowAwareOf (nowAwareOf),
    UsingHasObjectPermanence,
    UsingNowAwareOf,
    personalEventLog,
    potentialRevelations,
  )
import Games.RogueLike.Components.Combat
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Material
import Games.RogueLike.Components.MovementComponents
  ( UsingWantsToMove,
  )
import Games.RogueLike.Components.Name
  ( UsingName,
    renderEntityName,
  )
import Games.RogueLike.Components.Renderable (UsingRenderable)
import Games.RogueLike.Components.Sensory (UsingHasVision)
import Games.RogueLike.Systems.AwarenessSystem (MonadReveals)
import Games.RogueLike.Systems.DeathManagement ()
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Systems.MovementSystem
import Games.RogueLike.Types.Damage
import Games.RogueLike.Types.Effects
import Games.RogueLike.Types.Lore
import Games.RogueLike.Types.RNGStore (RollsDice)
import Games.RogueLike.Types.SpawnEffects
import Games.RogueLike.Types.Tick (MonadGameTime)

{-# INLINEABLE effectAffectsEntitiesAtPosition #-}
effectAffectsEntitiesAtPosition :: EffectSpecifics -> Bool
effectAffectsEntitiesAtPosition reason
  | DoDamage {} <- reason = True -- E.g. a bomb blast
  | HealDamage {} <- reason = True -- A ray of healing?
  | RevealHidden {} <- reason = True -- Reveal everything at the position
  --  | CustomEffect _ b <- reason = b -- Let the custom effect decide!
  | Bloodstain {} <- reason = False -- We're just spawning an entity at the position.
  | SpawnPrototype {} <- reason = False -- We're just spawning an entity at the position.
  | RemoveEntity {} <- reason = False -- We probably don't want to delete everything at a position.
  | Resurrect {} <- reason = True -- We can resurrect everything in a given area.
  | PlayAudio <- reason = False -- It makes no sense to play audio to the user once per entity at a position
  -- automatically.
  | PlaySound <- reason = True -- Each entity at a position should be able to receive the sound.
  | Blind {} <- reason = True -- E.g. a flashbang
  | Stun {} <- reason = True -- E.g. a flashbang
  | Deafen {} <- reason = True -- E.g. a flashbang
  | Confuse {} <- reason = True -- E.g. a flashbang
  | StartFire {} <- reason = True -- Engulf the position in flame
  | ExtinguishFire {} <- reason = True -- Intermingled flames don't really care about their individual sources
  | SetFlag {} <- reason = True -- Apply a flag to everything in the position
  | RemoveFlag {} <- reason = True -- Remove the flag from everything in the position
  | Soak {} <- reason = True -- Soak everything at the position
  | LogEvent {} <- reason = True -- If an event happens at a position, then every entity at the position will
  -- experience the event?
  | EntityDeath {} <- reason = True -- A death ray?
  | GiveOrders {} <- reason = False -- Orders should be specifically given to entities, not implicitly
  | JoinSquad {} <- reason = False -- Shouldn't be implicit
  | LeaveSquad {} <- reason = False -- Shouldn't be implicit
  | ActivateTrigger {} <- reason = True -- Any entity with a trigger with the given name in the target area
  | StopTrigger {} <- reason = True -- Any entity with a trigger with the given name in the target area
  | Teleport {} <- reason = True -- Teleport everything at the position
  | ChangeFactionRelation {} <- reason = False -- Floor tiles shouldn't be affected, for example
  | AddAllegiance {} <- reason = False -- Floor tiles shouldn't be affected
  | RemoveAllegiance {} <- reason = False -- Floor tiles
  | CauseBleeding {} <- reason = False

{-# INLINEABLE processEffects #-}

-- | Processes the effects queue.
processEffects ::
  forall worldType m.
  ( --  Members '[State MapIndexingSystem, Embed IO] r,
    RollsDice m,
    NFData (worldType Storing),
    WritesMapIndexing worldType m,
    UsingGameLog m,
    SpawnsEffects m,
    MonadIO m,
    UsingPosition worldType Individual,
    UsingBasicMaterialData worldType Individual,
    UsingHealthStatus worldType Individual,
    UsingNowAwareOf worldType Individual,
    UsingJustDied worldType Individual,
    UsingHasObjectPermanence worldType Individual,
    UsingPositionChanged worldType Individual,
    UsingCombatStats worldType Individual,
    UsingDoesBumpDamage worldType Individual,
    UsingIsPrototype worldType Individual,
    UsingSpawnedFromPrototype worldType Individual,
    UsingIsDead worldType Individual,
    UsingHasAllegiances worldType Individual,
    UsingMadeOf worldType Individual
  ) =>
  EffectsToProcess ->
  worldType Storing ->
  m (MessagesProcessed (DoEffect EffectSpecifics), worldType Storing)
processEffects passiveOnly oldWorld = processMessageQueue (shouldSkipEffectFilter passiveOnly) processEffect oldWorld

data EffectsToProcess = ProcessAllEffects | ProcessCosmeticEffects

shouldSkipEffectFilter :: EffectsToProcess -> worldType Storing -> (DoEffect EffectSpecifics) -> Bool
shouldSkipEffectFilter ProcessAllEffects _ _ = False
shouldSkipEffectFilter ProcessCosmeticEffects _ (DoEffect {_effectDetails}) = not $ effectIsCosmetic _effectDetails

collectTargets :: (World worldType, MonadMapIndexing m) => worldType Storing -> DoEffect EffectSpecifics -> m (HS.HashSet Entity, HS.HashSet Position)
collectTargets world eff =  do
  -- TODO: Filter targets by effect type.
  idxSys <- getMapIndex
  let positions = setOf (targets . folded . targetsPosition) eff
      entsAtPosition pos = setOf (entitiesAtPosition idxSys pos . entityReference) world
      targetedEntsByPosition =
        if effectAffectsEntitiesAtPosition (eff ^. effectDetails)
          then HS.unions $ HS.toList (HS.map entsAtPosition positions)
          else Empty
  pure (targetedEntsByPosition `HS.union` setOf (targets . folded . targetsEntity) eff, positions)

-- | Process an individual effect, by applying it to each target.
processEffect ::
  forall worldType m.
  ( --  Members '[State MapIndexingSystem, Embed IO] r,

    RollsDice m,
    WritesMapIndexing worldType m,
    UsingGameLog m,
    SpawnsEffects m,
    MonadIO m,
    UsingPosition worldType Individual,
    UsingBasicMaterialData worldType Individual,
    UsingHealthStatus worldType Individual,
    UsingNowAwareOf worldType Individual,
    UsingJustDied worldType Individual,
    UsingHasObjectPermanence worldType Individual,
    UsingPositionChanged worldType Individual,
    UsingCombatStats worldType Individual,
    UsingDoesBumpDamage worldType Individual,
    UsingIsPrototype worldType Individual,
    UsingSpawnedFromPrototype worldType Individual,
    UsingIsDead worldType Individual,
    UsingHasAllegiances worldType Individual,
    UsingMadeOf worldType Individual
  ) =>
  worldType Storing ->
  DoEffect EffectSpecifics ->
  m (worldType Storing)
processEffect world eff = do
  (targetedCritters, targetedPositions) <- collectTargets world eff
  checkEntityExists world (eff ^. causalEntity)
  checkEntitiesExist world (ES.setOf folded targetedCritters)
  -- Handle special cases first
  case eff ^. effectDetails of
    RevealHidden wiseGuy reason -> pure (world & entity wiseGuy . nowAwareOf . potentialRevelations <>~ HS.map (,reason) targetedCritters)
    -- For effects which apply orthogonally to each critter
    _ -> do
      positionsDone <- foldM applyEffectToPosition world targetedPositions

      crittersDone <-
        forMOf
          (lookupEntities targetedCritters)
          positionsDone
          ( \targetedCritter -> do
              case eff ^. effectDetails of
                DoDamage _ _ -> doDamage targetedCritter world eff
                Teleport newPos -> do
                  moved <- applyMovement newPos targetedCritter
                  case moved of
                    Nothing -> pure targetedCritter
                    Just moved' -> pure moved'
                EntityDeath causalEffect -> do
                  logEvent
                    (eff ^. causalEntity)
                    [
                     TargetIndividual (targetedCritter ^. entityReference),
                     TargetIndividual (eff ^. causalEntity)
                    ]
                    (InGameEvent eff)

                  pure $ targetedCritter & addJustDied .~ JustDied causalEffect

                -- CustomEffect (ACustomEffect desc (customEff :: eff) ) _b -> runEffectOnCritter @eff @EffectSpecifics world targetedCritter customEff eff

                HealDamage dmg instr ->
                  -- TODO parity with doDamage
                  if hasn't healthStatus targetedCritter
                    then pure targetedCritter
                    else do
                      dmg' <- resolveDamage dmg
                      let damageToHeal = dmg' ^?! resolvedDamageAmount . _Just

                      -- Make sure health doesn't go above maximum health
                      let maxHealth = targetedCritter ^?! healthStatus . maximumHealth
                          newHealth = min maxHealth (targetedCritter ^?! healthStatus . health + damageToHeal)
                      pure (targetedCritter & healthStatus . health .~ newHealth)
                Resurrect _ | hasn't isDead targetedCritter -> pure targetedCritter
                Resurrect dmg -> do
                  -- Let the entity know it's being resurrected
                  logEvent
                    (eff ^. causalEntity)
                    [
                     TargetIndividual (targetedCritter ^. entityReference),
                     TargetIndividual (eff ^. causalEntity)
                    ]
                    (InGameEvent eff)

                  -- Heal the entity
                  doImmediateEffect
                    (eff ^. causalEntity)
                    [TargetIndividual (targetedCritter ^. entityReference)]
                    (HealDamage dmg (eff ^. causalEntity))

                  pure (targetedCritter & removeIsDead)
                LogEvent effData -> pure $ targetedCritter & hasObjectPermanence . personalEventLog %~ (`snoc` effData)
                AddAllegiance newAllegiance -> do
                  logEvent
                    (eff ^. causalEntity)
                    [
                     TargetIndividual (targetedCritter ^. entityReference),
                     TargetIndividual (eff ^. causalEntity)
                    ]
                    (InGameEvent eff)
                  if has hasAllegiances targetedCritter
                    then pure $ targetedCritter & hasAllegiances . allegiances %~ (newAllegiance DSO.<|)
                    else pure $ targetedCritter & addHasAllegiances .~ HasAllegiances (DSO.singleton newAllegiance) Empty
                RemoveAllegiance formerAllegiance -> do
                  logEvent
                    (eff ^. causalEntity)
                    [
                     TargetIndividual (targetedCritter ^. entityReference),
                     TargetIndividual (eff ^. causalEntity)
                    ]
                    (InGameEvent eff)
                  pure $ targetedCritter & hasAllegiances . allegiances %~ DSO.delete formerAllegiance
                Bloodstain _ -> error "Bloodstain effect on an entity has not been implemented"
          )
      pure crittersDone
  where
    applyEffectToPosition :: worldType Storing -> Position -> m (worldType Storing)
    applyEffectToPosition oldWorld pos = do
      case eff ^. effectDetails of
        Bloodstain bleeder -> do
          case oldWorld ^? entity bleeder . madeOf of
            Nothing -> pure oldWorld
            Just (MadeOf m) -> case oldWorld ^? prototype m . basicMaterialData . bloodType of
              Nothing -> pure oldWorld
              Just bloodPrototype -> do
                doImmediateEffect bleeder [TargetPosition pos] (SpawnPrototype bloodPrototype)
                pure oldWorld
        SpawnPrototype proto -> do
          res <- spawnNamedPrototype proto oldWorld
          case res of
            Nothing -> pure oldWorld
            Just (protoCritter, newWorld) -> do
              let protoCritter' = protoCritter & addPosition .~ pos
              indexEntity protoCritter'
              -- Store again because we add position
              pure (newWorld & storeEntity protoCritter')

-- | Apply damage to a critter
doDamage ::
  ( UsingPosition worldType Individual,
    UsingName worldType Individual,
    UsingHealthStatus worldType Individual,
    SpawnsEffects m,
    MonadGameLog m,
    RollsDice m
  ) =>
  worldType Individual ->
  worldType Storing ->
  DoEffect EffectSpecifics ->
  m (worldType Individual)
doDamage targetedCritter _ _ | hasn't healthStatus targetedCritter = pure targetedCritter
doDamage targetedCritter world eff | (DoDamage dmg instr) <- eff ^. effectDetails = do
  -- Get concrete value of damage
  dmg' <- resolveDamage dmg
  let damageToDo = dmg' ^?! resolvedDamageAmount . _Just
      updatedEffect = eff & effectDetails .~ DoDamage dmg' instr
      hurtCritter = targetedCritter & healthStatus . health -~ damageToDo

  -- Let the critter know about the cause
  doEffect
    instr
    [TargetIndividual $ updatedEffect ^. causalEntity]
    (RevealHidden (hurtCritter ^. entityReference) CausedDamage)

  -- Log the damage
  logEvent
    (eff ^. causalEntity)
    [ TargetIndividual (hurtCritter ^. entityReference),
      TargetIndividual (updatedEffect ^. causalEntity)
    ]
    (InGameEvent updatedEffect)

  -- Kill the critter if its health goes past 0 (and was above 0 before the damage)
  when (hurtCritter ^?! healthStatus . health <= 0 && targetedCritter ^?! healthStatus . health > 0) $ do
    doImmediateEffect
      (updatedEffect ^. causalEntity)
      [TargetIndividual (hurtCritter ^. entityReference)]
      (EntityDeath $ Just (InGameEvent updatedEffect))

  -- TODO: for HUGE hits, spawn multiple bloodstains
  let bloodStainTargets = fmap TargetPosition (targetedCritter ^? position)

  -- Bloodstain
  doImmediateEffect
    (eff ^. causalEntity)
    bloodStainTargets
    (Bloodstain (targetedCritter ^. entityReference))

  logMessage
    ( renderEntityName targetedCritter
        <+> "suffered"
        <+> renderDamage dmg'
        <+> "damage from"
        <+> renderEntityName (world ^?! entity (updatedEffect ^. causalEntity))
    )
  pure hurtCritter

{-# INLINEABLE runEffectsSystem #-}

-- | Runs the effects system -- and all dependant systems -- in a loop until no more effects are queued.
runEffectsSystem ::
  forall worldType m.
  ( UsingGameLog m,
    SpawnsEffects m,

    NFData (worldType Storing),                  
    RollsDice m,              
    MonadGameTime m,
    MonadReveals m,
    MonadIO m,
    UsingHasObjectPermanence worldType Individual,
    UsingHasAllegiances worldType Individual,
    UsingRenderable worldType Individual,
    WritesMapIndexing worldType m,
    World worldType,
    UsingHealthStatus worldType Individual,
    UsingJustDied worldType Individual,
    UsingIsDead worldType Individual,
    UsingWantsToMove worldType Individual,
    UsingName worldType Individual,
    UsingJustDied worldType Storing,
    UsingBasicMaterialData worldType Individual,
    UsingNowAwareOf worldType Storing,
    UsingPositionChanged worldType Individual,
    UsingCombatStats worldType Individual,
    UsingDoesBumpDamage worldType Individual,
    UsingHasVision worldType Individual,
    UsingHealthStatus worldType Storing,
    UsingIsPrototype worldType Individual,
    UsingSpawnedFromPrototype worldType Individual,
    UsingIsDead worldType Individual,
    UsingMadeOf worldType Individual,
    UsingBrawlerAI worldType Individual
  ) =>
  EffectsToProcess ->
  worldType Storing ->
  m (worldType Storing)
runEffectsSystem passiveOnly oldWorld = do
  (effsDone, world) <- processEffects passiveOnly oldWorld
  case effsDone of
    AllMessagesProcessed -> do
      --      g1 <- runAfterEffects @"DamageSystem" world
      g2 <- runAfterEffects @"DeathProcessingSystem" world
      g3 <- runAfterEffects @"AwarenessSystem" g2
      pure g3
    NewMessagesProcessed _effs -> do
      --     g1 <- runSystem @"DamageSystem" world
      g2 <- runSystem @"DeathProcessingSystem" world
      g3 <- runSystem @"AwarenessSystem" g2

      runEffectsSystem passiveOnly g3
