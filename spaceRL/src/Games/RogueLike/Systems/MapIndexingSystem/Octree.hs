{- LANGUAGE UnliftedDatatypes #-}
module Games.RogueLike.Systems.MapIndexingSystem.Octree
    where
import Control.Lens
import Numeric.Natural    
import GHC.Generics    
import Data.Bits
import Data.Coerce
import qualified Data.HashMap.Strict as HMS
import qualified Data.HashSet as HS
import Data.Hashable
import Data.Interned
import Games.ECS
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Types.BasicInterfaces
import Games.RogueLike.Types.Effects
import Games.RogueLike.Types.SpaceFillingCodes
import GHC.Exts
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Types.PhysicalTypes
import Data.Word
import Data.Monoid
import Control.DeepSeq
type Blocking = Maybe (Obstruction (Sum Double))
    
data EntityTileEntryData = EntityTileEntryData
  { __entityBlocksMovement :: !Blocking,
    __entityBlocksVision :: !Bool,
--    __entityInitiativePenalty :: !MovementPenalty,
    __hiddenFromSomeone :: !Bool,
    __entityIsTraversable :: !Bool                           
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass NFData

deriving anyclass instance Hashable EntityTileEntryData

data InternedEntityTileEntryData = InternedEntityTileEntryData {_internedEntryID :: !Id, _theEntryData :: !EntityTileEntryData}
                                   deriving stock (Show, Generic)
                                   deriving anyclass NFData

instance Eq InternedEntityTileEntryData where
  {-# INLINE (==) #-}
  (InternedEntityTileEntryData a _) == (InternedEntityTileEntryData b _) = a == b

instance Ord InternedEntityTileEntryData where
  {-# INLINE compare #-}
  compare (InternedEntityTileEntryData a _) (InternedEntityTileEntryData b _) = compare a b

instance Hashable InternedEntityTileEntryData where
  {-# INLINE hashWithSalt #-}
  hashWithSalt s (InternedEntityTileEntryData i _) = hashWithSalt s i

instance Interned InternedEntityTileEntryData where
  type Uninterned InternedEntityTileEntryData = EntityTileEntryData
  newtype Description InternedEntityTileEntryData = DETED EntityTileEntryData deriving newtype (Eq, Hashable, NFData)
  describe = DETED
  {-# INLINE CONLIKE describe #-}
  identify = InternedEntityTileEntryData
  {-# INLINE CONLIKE identify #-}
  cache = itCache

instance Uninternable InternedEntityTileEntryData where
  unintern (InternedEntityTileEntryData _ b) = b
  {-# INLINE unintern #-}

itCache :: Cache InternedEntityTileEntryData
itCache = mkCache
{-# NOINLINE itCache #-}

makeLenses ''EntityTileEntryData

data EntityTileEntry = EntityTileEntry {_entityRef :: !Entity, _thedata :: !InternedEntityTileEntryData}
                       deriving stock (Eq, Ord, Show, Generic)
                       deriving anyclass NFData

internedEntry :: Iso' InternedEntityTileEntryData EntityTileEntryData
internedEntry = iso unintern intern
{-# INLINE internedEntry #-}

makeLenses ''EntityTileEntry

entityBlocksMovement :: Lens' EntityTileEntry Blocking
entityBlocksMovement = thedata . internedEntry . _entityBlocksMovement
{-# INLINE entityBlocksMovement #-}

entityBlocksVision :: Lens' EntityTileEntry Bool
entityBlocksVision = thedata . internedEntry . _entityBlocksVision
{-# INLINE entityBlocksVision #-}

--entityInitiativePenalty :: Lens' EntityTileEntry MovementPenalty
--entityInitiativePenalty = thedata . internedEntry . _entityInitiativePenalty
-- {-# INLINE entityInitiativePenalty #-}

hiddenFromSomeone :: Lens' EntityTileEntry Bool
hiddenFromSomeone = thedata . internedEntry . _hiddenFromSomeone
{-# INLINE hiddenFromSomeone #-}

entityIsTraversable :: Lens' EntityTileEntry Bool
entityIsTraversable = thedata . internedEntry . _entityIsTraversable

{-# INLINEABLE newEntityTileEntry #-}
newEntityTileEntry :: (UsingHiddenFrom w Individual, UsingBlocksMovement w Individual, UsingBlocksVision w Individual{-, UsingMovementPenalty w Individual-}) => (w Individual -> Bool) -> w Individual -> EntityTileEntry
newEntityTileEntry _traversableFilter ent = EntityTileEntry (ent ^. entityReference) $ intern $ EntityTileEntryData (fmap (fmap Sum) $ ent^?blocksMovement . obstructionAmount  ) (has blocksVision ent) {-(ent ^. movementPenalty)-} (has (hiddenFrom . victims . knownEntities) ent) False --(traversableFilter ent)

instance Hashable EntityTileEntry where
  {-# INLINE hash #-}
  hash (EntityTileEntry {_entityRef}) = hash _entityRef
  {-# INLINE hashWithSalt #-}
  hashWithSalt s (EntityTileEntry {_entityRef}) = hashWithSalt s _entityRef


                                                  
newtype TileEntry a = TileEntry {
      -- | This should /probably/ be just a normal Set, as most tiles will be empty, only have a single occupant, or have a very small number of occupants. 
      _inhabitants :: (HS.HashSet a)} --EntityTileEntry)}
  deriving newtype (Eq, Ord, Show)
  deriving stock (Generic)
  deriving anyclass NFData           

makeLenses ''TileEntry

--inhabitants :: IndexedTraversal' InternedEntityTileEntry TileEntry InternedEntityTileEntry
--inhabitants = (iso unintern intern) . _inhabitants
--{-# INLINE inhabitants #-

instance Hashable a => Hashable (TileEntry a) where
  {-# INLINE hash #-}
  hash (TileEntry {_inhabitants}) = hash _inhabitants
  {-# INLINE hashWithSalt #-}
  hashWithSalt s (TileEntry {_inhabitants}) = hashWithSalt s _inhabitants

instance (Eq a, Hashable a) => Semigroup (TileEntry a) where
  {-# INLINE (<>) #-}
  (TileEntry ia) <> (TileEntry ib) = TileEntry (HS.union ia ib)

instance (Eq a, Hashable a) => Monoid (TileEntry a) where
  mempty = newEntry
  {-# INLINE mconcat #-}
  mconcat a = TileEntry (HS.unions (fmap coerce a))

newEntry :: TileEntry a
newEntry = TileEntry HS.empty

-- | Currently: A HAMT, mapping from Morton/Hilbert-coded Positions to sets of Entities. It's a fairly safe bet that
-- it would have the same locality-of-reference properties. Eventually: A linear octree, implemented as an
-- ordered sequence with the ordering derived from a space-filling curve.
-- 
-- Should it be an ordered map instead, due to the Morton coding? Yes, absolutely!
newtype Octree' a = Octree {_entries :: HilbertMap -- HMS.HashMap HilbertCoded
                                        (TileEntry a)}
  deriving newtype (Eq, Show, Hashable)
  deriving stock (Generic, Generic1)
  deriving anyclass NFData

makeLenses ''Octree'

type Octree = Octree' EntityTileEntry

newOctree :: Octree' a
newOctree = Octree (()^.re _Empty)

{-# INLINE insertEntityIntoIndex #-}
insertEntityIntoIndex :: (UsingHiddenFrom w Individual, UsingBlocksMovement w Individual, UsingPosition w Individual, UsingBlocksVision w Individual{-, UsingMovementPenalty w Individual-}) => (w Individual -> Bool) -> w Individual -> Octree -> Octree
insertEntityIntoIndex traversableFilter critter octree = octree & entries %~ insertWith (<>) coded entry
  where
    coded :: HilbertCoded
    coded = critter ^. singular position . hilbertCoded
    entry = TileEntry (HS.singleton (newEntityTileEntry traversableFilter critter))

{-# INLINE removeEntityFromIndex #-}
removeEntityFromIndex :: (UsingBlocksMovement w Individual, UsingHiddenFrom w Individual, UsingPosition w Individual, UsingBlocksVision w Individual{-, UsingMovementPenalty w Individual-}) => (w Individual -> Bool) -> w Individual -> Octree -> Octree
removeEntityFromIndex traversableFilter critter octree = octree & entries . ix (critter ^. singular position . hilbertCoded) . inhabitants %~
                                                           sans (newEntityTileEntry traversableFilter critter) -------- TODO FIXME what if the traversable filter is different? :/ or the critter had a different property? e.g. if simple jack dies, and then walks away, and then walks back, it is still there?

{-removeEntityFromIndex :: (UsingBlocksMovement w Individual,UsingHiddenFrom w Individual, UsingPosition w Individual, UsingBlocksVision w Individual, UsingMovementPenalty w Individual) => Entity->Position -> Octree -> Octree
removeEntityFromIndex entRef oldPos octree = octree & entries . ix (oldPos ^. hilbertCoded) . inhabitants . sans ( EntityTileEntry {_entityRef = entRef})-}

{-# INLINE indexedEntities #-}
indexedEntities :: IndexedFold Position Octree Entity
indexedEntities = conjoined (entries . traversed . inhabitants . folded . entityRef) (reindexed (review hilbertCoded) (entries . itraversed <. inhabitants . folded . entityRef))

{-# INLINE entitiesAtPosition #-}
entitiesAtPosition :: Position -> Fold Octree Entity
entitiesAtPosition pos = positionEntries pos . entityRef

-- {-# INLINE positionInitiativePenalty #-}
-- positionInitiativePenalty :: Octree -> Position -> 
-- positionInitiativePenalty oct pos = oct ^. (positionEntries pos . entityInitiativePenalty)



{-# INLINE positionEntries #-}
positionEntries :: Position -> Fold Octree EntityTileEntry
positionEntries pos = entries . ix (pos ^. hilbertCoded) . inhabitants . folded

{-# INLINE positionInitiativePenalty #-}
positionInitiativePenalty :: Position -> Fold Octree (Sum Double)
positionInitiativePenalty pos = (positionEntries pos) .  entityBlocksMovement . _Just . _MovementPenalty




-- positionMovementPenalty :: Octree -> Position -> Maybe (Sum Double)
-- positionMovementPenalty oct pos = oct^.(positionEntries pos . _entityInitiativePenalty . _MovementPenalty)
{-# INLINE positionMovementPenalty #-}
positionMovementPenalty :: Position -> Fold Octree (Sum Double)
positionMovementPenalty pos = (positionEntries pos) .  entityBlocksMovement . _Just . _MovementPenalty
                           
{-# INLINE positionBlocked #-}
positionBlocked :: Octree -> Position -> Blocking
positionBlocked oct pos = oct^. (positionEntries pos . entityBlocksMovement)

{-# INLINE positionBlocksVision #-}
positionBlocksVision :: Octree -> Position -> Bool
positionBlocksVision oct pos = orOf (positionEntries pos . entityBlocksVision) oct

{-# INLINE hiddenEntitiesAtPosition #-}
hiddenEntitiesAtPosition :: Octree -> Position -> Bool
hiddenEntitiesAtPosition oct pos = orOf (positionEntries pos . hiddenFromSomeone) oct

{-# INLINE positionIsTraversable #-}                                   
positionIsTraversable :: Octree -> Position -> Bool
positionIsTraversable oct pos = orOf (positionEntries pos . entityIsTraversable) oct
{-
{-# INLINE entitiesInCube #-}
entitiesInCube :: Position -> Position -> IndexedFold Position Octree Entity
entitiesInCube minPos maxPos = indexedEntities . ifiltered filterPosition
  where
    filterPosition pos _ = undefined
    minVal = minPos ^. hilbertCoded
    maxVal = maxPos ^. hilbertCoded
-}
