module Games.RogueLike.Systems.AnimationSystem where
import Games.ECS
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Kinematics


data AnimationSystem = AnimationSystem

instance
  ( Monad m, 
    UsingRenderable worldType Individual,
    UsingCurrentAnimation worldType Individual
  ) =>
  System "AnimationSystem" AnimationSystem worldType m
  where
  type RunsAfter AnimationSystem = '[]
  type RunsBefore AnimationSystem = '[]
  type
    ComponentFilters "AnimationSystem" AnimationSystem worldType m =
      (
       Monad m, 
       UsingRenderable worldType Individual,
       UsingCurrentAnimation worldType Individual
      )                     

  componentFilter = withCurrentAnimation <> withRenderable
  processEntity critter = do
      pure critter
--    logMessage ((renderEntityName critter) <+> (pretty ("was" :: String)) <+> (renderJustDied $ critter ^?! justDied))
--    pure (critter & removeWantsToIntelligentMove & removeJustDied & addIsDead .~ IsDead)
