module Games.RogueLike.Components.Crafting.Resource where
import GHC.Generics
import Control.Lens    
import Games.ECS
import Games.RogueLike.Components.Material
import Games.RogueLike.Types.Identifiers
import qualified Data.HashSet as HS

data ResourceClass = ResourceClass {_resourceClassName :: ResourceClassName,
                                    _subtypes :: HS.HashSet ResourceClassName}
                   deriving stock (Eq, Generic, Show)  
makeLenses ''ResourceClass
