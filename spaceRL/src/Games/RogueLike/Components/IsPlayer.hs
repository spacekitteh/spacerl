module Games.RogueLike.Components.IsPlayer where

import           Games.ECS
import           GHC.Generics
import           Games.ECS.Entity


data IsPlayer = IsPlayer deriving (Eq, Show, Generic)

instance Component IsPlayer where
  type CanonicalName IsPlayer = "isPlayer"
  type Prop IsPlayer = Unique
  type Storage IsPlayer = UniqueStore
  {-# INLINABLE emptyStorage #-}
  emptyStorage = UniqueStore Nothing

instance FlagComponent IsPlayer

makeHasComponentClass ''IsPlayer
