{-# OPTIONS_GHC -fno-warn-orphans #-}
module Games.RogueLike.Components.Allegiance (module Games.RogueLike.Components.Allegiance, module Games.RogueLike.Types.Effects) where
import GHC.Generics
import Control.Lens    
import Data.Interned.Text
import Games.ECS
import Data.Sequence
import Data.Set.Ordered
import Data.Hashable
import Data.Maybe
import Data.Word
import Control.DeepSeq
import Data.Foldable    
import qualified Data.HashMap.Strict as HMS
import Games.RogueLike.Types.Effects (Allegiance, allegianceFaction, FactionRank(FactionRank), combatRank, socialRank, FactionRelation(FactionRelation), opinion, reaction, Opinion(Opinion), FactionReaction(..))

instance NFData (OSet Allegiance) where
    rnf = rnf . toAscList
data HasAllegiances = HasAllegiances {_allegiances :: OSet Allegiance, _ranks :: HMS.HashMap Allegiance FactionRank} deriving (Eq, Show, Generic)
makeLenses ''HasAllegiances
instance Component HasAllegiances where
  type CanonicalName HasAllegiances = "hasAllegiances"
makeHasComponentClass ''HasAllegiances



data IsFaction = IsFaction {_factionRelations :: HMS.HashMap Allegiance FactionRelation} deriving (Eq, Show, Generic)
deriving instance Hashable IsFaction


makeLenses ''IsFaction
instance Component IsFaction where
  type CanonicalName IsFaction = "isFaction"
makeHasComponentClass ''IsFaction

newFaction :: IsFaction
newFaction = IsFaction HMS.empty

