module Games.RogueLike.Components.Wearable where
import           Games.ECS.Component
import Games.ECS.World.TH
import GHC.Generics

data Wearable = WearableOn 
  deriving stock (Eq, Show, Generic)

instance Component (Wearable ) where
  type CanonicalName (Wearable ) = "wearable"
--makeHasComponentClass ''Wearable
