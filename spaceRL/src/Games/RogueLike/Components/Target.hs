module Games.RogueLike.Components.Target where
import GHC.Generics
import           Games.ECS
import           Control.Lens
import Control.DeepSeq    
import           Games.ECS.Entity
import Data.Word
import Data.Set
import Games.RogueLike.Types.Identifiers    
data Target = Target {_targetedEntity :: Entity, _targetedWith :: Entity} deriving stock (Eq, Show)
                               deriving stock Generic
makeLenses ''Target

instance Component Target where
  type CanonicalName Target = "target"
makeHasComponentClass ''Target
instance ReferenceComponent Target


data TargetingRange = AdjacentOnly
                    | RangeLimitedTo Word8
                      deriving stock (Eq, Ord, Show, Generic)
                      deriving anyclass NFData


makePrisms ''TargetingRange    
data CanTarget = CanTarget {_maximumRange :: TargetingRange, _targetableClasses :: Set TargetClassName} deriving stock (Eq, Show, Ord, Generic)
makeLenses ''CanTarget
           
instance Component CanTarget where
    type CanonicalName CanTarget = "canTarget"
makeHasComponentClass ''CanTarget        
