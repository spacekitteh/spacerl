module Games.RogueLike.Components.Material where
import Control.Lens
import GHC.Generics    
import Games.ECS 
import Games.RogueLike.Types.PhysicalTypes
import Games.ECS.World.TH
import Data.Hashable
import Games.ECS.Prototype    
data BasicMaterialData = BasicMaterialData {_density :: Density, _bloodType :: PrototypeID, _flammability :: Double}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass Hashable

makeLenses ''BasicMaterialData
instance Component BasicMaterialData where
  type CanonicalName BasicMaterialData = "basicMaterialData"
makeHasComponentClass ''BasicMaterialData

data MadeOf = MadeOf {_madeOfMaterial :: PrototypeID}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass Hashable
makeLenses ''MadeOf
instance Component MadeOf where
  type CanonicalName MadeOf = "madeOf"
makeHasComponentClass ''MadeOf
