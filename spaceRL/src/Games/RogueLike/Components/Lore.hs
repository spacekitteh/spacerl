module Games.RogueLike.Components.Lore where
import GHC.Generics
import Control.Lens    
import Data.HashSet as HS
import Data.Hashable
import Data.Interned.Text
import Data.String
import Games.ECS
import Games.RogueLike.Types.Identifiers
import Games.RogueLike.Types.Lore
import Games.RogueLike.Types.SpawnEffects
import Numeric.Natural

data HistoricalEvent = HistoricalEvent
  { _historicalTime :: HistoricalTime,
    _historicalEventDescription :: EventDescription,
    _inGameEvent :: Maybe InGameEvent
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable)

makeLenses ''HistoricalEvent

instance Component HistoricalEvent where
  type CanonicalName HistoricalEvent = "historicalEvent"

makeHasComponentClass ''HistoricalEvent
