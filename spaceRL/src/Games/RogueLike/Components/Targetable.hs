module Games.RogueLike.Components.Targetable where
import GHC.Generics
import           Games.ECS
import           Control.Lens
import           Games.ECS.Entity
import Games.ECS.World.TH
data TargetableByDefault = TargetableByDefault deriving stock (Eq, Show, Generic)
makeLenses ''TargetableByDefault

instance Component TargetableByDefault where
  type CanonicalName TargetableByDefault = "targetableByDefault"
makeHasComponentClass ''TargetableByDefault
instance FlagComponent TargetableByDefault
