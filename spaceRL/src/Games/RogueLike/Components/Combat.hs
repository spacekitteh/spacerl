module Games.RogueLike.Components.Combat where
import GHC.Generics
import Control.Lens    
import Data.Hashable
import Data.Sequence
import Games.ECS
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Types.Effects (Orders)
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.Damage
    
data CombatStats = CombatStats {_meleeDamage :: DamageSpecification}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable)

makeLenses ''CombatStats

instance Component (CombatStats) where
  type CanonicalName (CombatStats) = "combatStats"

makeHasComponentClass ''CombatStats

data DoesBumpDamage = DoesBumpDamage
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable)

instance Component DoesBumpDamage where
  type CanonicalName DoesBumpDamage = "doesBumpDamage"

instance FlagComponent DoesBumpDamage

makeHasComponentClass ''DoesBumpDamage



data IsSquad = IsSquad
  { _squadLeader :: Entity,
    -- | Sequence because chain-of-command.
    _squadMembers :: Seq Entity
  }
  deriving stock (Eq, Ord, Generic, Show)

makeLenses ''IsSquad

instance Hashable IsSquad where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (IsSquad ldr mbrs) = hashWithSalt salt (ldr, HashableSeq mbrs)

instance Component IsSquad where
  type CanonicalName IsSquad = "isSquad"

makeHasComponentClass ''IsSquad

data SquadMember = SquadMember {_squad :: Entity}
  deriving stock (Eq, Ord, Generic, Show)
  deriving anyclass (Hashable)

makeLenses ''SquadMember

instance Component SquadMember where
  type CanonicalName SquadMember = "squadMember"

makeHasComponentClass ''SquadMember

data HasOrders = HasOrders {_orders :: Orders}
  deriving stock (Eq, Ord, Generic, Show)
  deriving anyclass (Hashable)

makeLenses ''HasOrders

instance Component HasOrders where
  type CanonicalName HasOrders = "hasOrders"

makeHasComponentClass ''HasOrders
