module Games.RogueLike.Components.BrawlerAI where
import Games.ECS
import GHC.Generics    
import Games.RogueLike.Components.MovementComponents
import Data.Sequence

data BrawlerAI = BrawlerAI  deriving (Eq, Show, Generic)

instance Component BrawlerAI where
  type CanonicalName BrawlerAI = "brawlerAI"
instance FlagComponent BrawlerAI
makeHasComponentClass ''BrawlerAI
