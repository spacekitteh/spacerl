{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StrictData #-}
module Games.RogueLike.Components.Awareness where
import GHC.Generics (Generic)
import Control.Lens     
import Control.Applicative (Alternative ((<|>)))
import Control.Exception.Assert.Sugar
import Data.Foldable as DF

import qualified Data.HashMap.Strict as HMS
import Data.HashSet as HS
import qualified Data.HashSet as Set
import Data.Hashable
import Data.Sequence hiding (Empty)
import qualified Data.Sequence as DS
import Data.Set.Ordered
import GHC.Exts
import Games.ECS
import Games.ECS.Entity.EntityMap as EM
import qualified Games.ECS.Entity.EntitySet as ES    
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.DiceRoll
import Games.RogueLike.Types.Effects
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.SpaceFillingCodes
import Games.RogueLike.Types.SpawnEffects
import Games.RogueLike.Types.Tick
import qualified Prettyprinter as PP
import Control.DeepSeq
--import Games.RogueLike.Types.Identifiers (FactionName)    
data RevealCondition = RevealCondition
  { _observationType :: ObservationReason,
    _revealDifficulty :: Challenge
  }
  deriving stock (Eq, Show, Generic)
  deriving anyclass NFData

deriving instance Hashable RevealCondition

makeLenses ''RevealCondition

newtype RevealConditions = RevealConditions {_revealConditionMap :: HMS.HashMap ObservationReason RevealCondition}
  deriving stock (Eq, Show, Generic)
  deriving anyclass (Hashable, NFData)

makeLenses ''RevealConditions

neverReveal :: RevealConditions
neverReveal = RevealConditions HMS.empty

addRevealCondition' :: RevealCondition -> RevealConditions -> RevealConditions
addRevealCondition' cond conditions = conditions & revealConditionMap . at (cond ^. observationType) .~ Just cond

newtype NowAwareOf = NowAwareOf {_potentialRevelations :: Set.HashSet (Entity, ObservationReason)} deriving (Eq, Generic)

makeLenses ''NowAwareOf

{-# INLINEABLE blankNowAwareOf #-}
blankNowAwareOf :: NowAwareOf
blankNowAwareOf = NowAwareOf Set.empty

{-# INLINEABLE renderNowAwareOf #-}
renderNowAwareOf :: NowAwareOf -> Message
renderNowAwareOf (NowAwareOf s) | Set.null s = ""
renderNowAwareOf (NowAwareOf xs) = "now aware of: " <+> renderXs
  where
    renderX (ent, reason) = (renderBareEntity ent) <+> PP.parens (renderReason reason)
    renderXs = PP.list $ fmap renderX $ Set.toList xs

instance Show NowAwareOf where
  {-# INLINEABLE show #-}
  show = show . renderNowAwareOf

instance Component NowAwareOf where
  type CanonicalName NowAwareOf = "nowAwareOf"

makeHasComponentClass ''NowAwareOf

-- TODO add hidden from factions
data HiddenFrom = HiddenFrom
  { _victims :: EntitySet,
--    _victimFactions :: Set FactionName,
    _revealsOnInteraction :: RevealConditions
  }
  deriving stock (Eq, Show, Generic)


makeLenses ''HiddenFrom

instance Component HiddenFrom where
  type CanonicalName HiddenFrom = "hiddenFrom"

makeHasComponentClass ''HiddenFrom

{-# INLINE revealsOnObservationTypes #-}
revealsOnObservationTypes :: UsingHiddenFrom worldType Individual => Fold (worldType Individual) ObservationReason
revealsOnObservationTypes = hiddenFrom . revealsOnInteraction . revealConditionMap . to HMS.keys . folded

data Observation = Observation
  { _observedEntity :: Entity,
    _observedPosition :: Maybe Position,
    _observationReason :: DS.Seq ObservationReason,
    _observedRenderable :: Maybe Renderable,
    _observedAllegiance :: Maybe (OSet Allegiance),
    _observationTime :: GameTime
  }
  deriving stock (Eq, Show, Generic)
  deriving anyclass NFData

makeLenses ''Observation

-- | Make an Observation of an entity, from the POV of a given entity.
observeEntity ::
  ( UsingPosition worldType Individual,
    UsingRenderable worldType Individual,
    UsingHasAllegiances worldType Individual,
    UsingHiddenFrom worldType Individual
  ) =>
  GameTime ->
  worldType Individual ->
  worldType Individual ->
  DS.Seq ObservationReason ->
  Observation
-- TODO FIXME: Actually take the observer into account.
observeEntity gameTime observer object observationReasons =
  assert
    ( notElemOf (hiddenFrom . victims . knownEntities) (observer ^. entityReference) object
        `blame` ("Entity is making observations of something hidden from it" :: String)
    )
    $ Observation (object ^. entityReference) (object ^? position) observationReasons (object ^? renderable) (object ^? hasAllegiances . allegiances) gameTime
{-# INLINE observeEntity #-}

{-# INLINE mergeObservations #-}

-- | Do sensor fusion
mergeObservations :: Observation -> Observation -> Maybe Observation
mergeObservations oldObs newObs | oldObs ^. observedEntity == newObs ^. observedEntity = Just result
  where
    result =
      oldObs & observedPosition .~ ((newObs ^. observedPosition) <|> (oldObs ^. observedPosition))
        & observationReason <>~ (newObs ^. observationReason)
        & observedRenderable .~ ((newObs ^. observedRenderable) <|> (oldObs ^. observedRenderable))
        & observedAllegiance %~ (`mergeMaybeOSets` (newObs ^. observedAllegiance))
        & observationTime .~ newObs ^. observationTime
    -- Prefer newer observations
    mergeMaybeOSets a b = fmap unbiased $ (fmap (Bias @R) a) <> (fmap Bias b)
mergeObservations _ _ = Nothing

newtype OrderByGameTime = OrderByGameTime Observation deriving (Eq, Generic)

instance Ord OrderByGameTime where
  {-# INLINE (<=) #-}
  (OrderByGameTime ob1) <= (OrderByGameTime ob2) = ob1 ^. observationTime <= ob2 ^. observationTime

-- | This is a collection of the /most recent/ observations of entities, not their observational history.
data HasObjectPermanence = HasObjectPermanence
  { _observationsByEntity :: EntityMap Observation,
    _observationsByPosition :: HMS.HashMap MortonCoded (EntityMap Observation),
    _personalEventLog :: DS.Seq InGameEvent
  }
  deriving (Eq, Show, Generic)

makeLenses ''HasObjectPermanence

instance Component HasObjectPermanence where
  type CanonicalName HasObjectPermanence = "hasObjectPermanence"

makeHasComponentClass ''HasObjectPermanence

{-# INLINE observationsAtPosition #-}
-- TODO FIXME: Make this a traversal using insertObservations etc
observationsAtPosition :: UsingHasObjectPermanence worldType Individual => Position -> Fold (worldType Individual) Observation
observationsAtPosition absPos = confusing (hasObjectPermanence . observationsByPosition . at (absPos ^. mortonCoded) . _Just . traversed)

{-# INLINEABLE blankObjectPermanence #-}
blankObjectPermanence :: HasObjectPermanence
blankObjectPermanence = HasObjectPermanence Empty Empty Empty

{-# INLINE insertObservation #-}

-- | Add an observation.
insertObservation :: Observation -> HasObjectPermanence -> HasObjectPermanence
insertObservation o mem = result
  where
    {-
    Because there might be an existing observation of the entity we have just observed, we need to remove it from
    the observation memory. This includes removing it from the Position-indexed memory, too, if a prior
    observation included position.
      -}
    oldPosition :: Maybe Position
    -- Get the prior observed position for the observed entity, if it exists.
    oldPosition = mem ^? observationsByEntity . at (o ^. observedEntity) . _Just . observedPosition . _Just
    -- Add the new observation.
    basic = mem & observationsByEntity . at (o ^. observedEntity) .~ Just o
    -- Remove the old position-indexed observation.
    removedPosition = case oldPosition of
      (Just pos) -> basic & observationsByPosition . at (pos ^. mortonCoded) . _Just . at (o ^. observedEntity) .~ Nothing
      _ -> basic
    -- Insert the new position-indexed observation.
    result = case o ^? observedPosition . _Just of
      Just pos ->
        removedPosition & observationsByPosition . at (pos ^. mortonCoded) %~ \case
          Just contents -> Just $ EM.insert (o ^. observedEntity) o contents
          Nothing -> Just  $ singletonEntityMap (o ^. observedEntity) o
      Nothing -> removedPosition

{-# INLINE clearObservationsAtPosition #-}

-- | Remove all observations corresponding to a given position. Remove corresponding Entity-indexed
-- observations.
clearObservationsAtPosition :: Position -> HasObjectPermanence -> HasObjectPermanence
clearObservationsAtPosition pos mem = result
  where
    result = HasObjectPermanence obe obp (mem ^. personalEventLog)
    obp = (mem ^. observationsByPosition) & at (pos ^. mortonCoded) .~ Nothing
    entsToRemove = fmap keysSet (mem ^? observationsByPosition . at (pos ^. mortonCoded) . _Just)
    obe = case entsToRemove of
      Nothing -> mem ^. observationsByEntity
      Just e -> ES.foldl' (\obs ent -> obs & at ent . _Just . observedPosition .~ Nothing) (mem ^. observationsByEntity) e

{-# INLINE clearObservedPositions #-}
clearObservedPositions :: Foldable f => f Position -> HasObjectPermanence -> HasObjectPermanence
clearObservedPositions poses hop = DF.foldl' (flip clearObservationsAtPosition) hop poses

{-# INLINE insertObservations #-}
insertObservations :: Foldable f => f Observation -> HasObjectPermanence -> HasObjectPermanence
insertObservations obs hop = DF.foldl' (flip insertObservation) hop obs
