module Games.RogueLike.Components.HasParts where
import GHC.Generics
import Control.Lens hiding (parts)
import Control.Applicative
import Data.Foldable
import qualified Data.HashMap.Strict as HMS
import Data.Hashable
import Data.Interned
import Data.Interned.Text
import Data.Maybe
import Data.String
import Games.ECS
import Games.RogueLike.Types.Identifiers


data BodyPartPrototype = BodyPartPrototype {_partTypeName :: PartTypeName}
  deriving stock (Eq, Show, Generic)
  deriving anyclass Hashable
makeLenses ''BodyPartPrototype
instance Component BodyPartPrototype where
  type CanonicalName BodyPartPrototype = "isBodyPartPrototype"
makeHasComponentClass ''BodyPartPrototype



-- | A body part. The idea is that all abilities and so-on are determined by the abstract prototype entity,
-- but can be overriden for a specific instance by modifying the realised body part entity.
data BodyPart = BodyPart {_partName :: PartName, _prototypeRef :: Entity}
  deriving stock (Eq, Show, Generic)
  deriving anyclass Hashable

makeLenses ''BodyPart

instance Component BodyPart where
  type CanonicalName BodyPart = "bodyPart"

instance CompositeComponent BodyPart where
  type SubComponentIndex BodyPart = PartName
  {-# INLINE subComponentReferences #-}
  subComponentReferences = icompose (\bp _ -> bp^.partName) selfIndex (indexing prototypeRef)



makeHasComponentClass ''BodyPart
{-# INLINE bodyPartPrototype #-}
bodyPartPrototype ::
  UsingBodyPart worldType Individual =>
  worldType Individual ->
  IndexedTraversal' PartName (worldType Storing) (worldType Individual)
bodyPartPrototype critter = components (critter ^?! bodyPart)

data HasBodyParts = HasBodyParts {_parts :: HMS.HashMap PartName Entity}
  deriving stock (Eq, Show, Generic)

makeLenses ''HasBodyParts

instance Component HasBodyParts where
  type CanonicalName HasBodyParts = "hasBodyParts"

instance CompositeComponent HasBodyParts where
  type SubComponentIndex HasBodyParts = PartName
  {-# INLINE subComponentReferences #-}
  subComponentReferences = parts . ifolded

makeHasComponentClass ''HasBodyParts

{-# INLINE traverseBodyPartsOf #-}
-- | Given a critter, traverse all of its bodyparts, indexed by part name.
traverseBodyPartsOf ::
  (UsingHasBodyParts worldType Individual) =>
  -- | The gameworld
  worldType Individual ->
  IndexedTraversal' PartName (worldType Storing) (worldType Individual)
traverseBodyPartsOf critter = components (critter ^?! hasBodyParts)
