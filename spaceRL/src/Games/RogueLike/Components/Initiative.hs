module Games.RogueLike.Components.Initiative where
import GHC.Generics
import Control.Lens    
import Games.ECS
import Games.RogueLike.Types.DiceRoll
import Data.Int
import GHC.Generics
import Data.Hashable


data MyTurn = MyTurn deriving stock (Eq, Ord, Show, Generic)

instance Component MyTurn where
  type CanonicalName MyTurn = "myTurn"
instance FlagComponent MyTurn
makeHasComponentClass ''MyTurn

-- TODO Turn this into an energy system instead?

data Initiative = Initiative {_turnsLeft :: Int,
                              _initiativeRoll :: DiceAlgebra}
  deriving stock (Eq, Ord, Generic, Show)
  deriving anyclass Hashable
makeLenses ''Initiative
instance Component Initiative where
  type CanonicalName Initiative = "initiative"
makeHasComponentClass ''Initiative

