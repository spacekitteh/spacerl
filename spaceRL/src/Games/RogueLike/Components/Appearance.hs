module Games.RogueLike.Components.Appearance where

import           Games.ECS
import Games.ECS.World.TH

import Games.RogueLike.Components.Material

{-data Appearance = Appearance (ComponentReference "madeOf" Material) deriving stock Generic

instance Component Appearance where
  type CanonicalName Appearance = "appearance"

makeHasComponentClass ''Appearance
-}
