module Games.RogueLike.Components.Archetypes.Physical where

import Games.ECS
--import           Games.ECS.Archetype

import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.Kinematics

{-

Have a bunch of instances like "HasPosition a, Archetype a, HasArchetype w a => HasPosition w"?
data PhysicallyPlaced s =
  PhysicallyPlaced ::
    AComponent "position" s Position ->
    AComponent "renderable" s Renderable ->
    PhysicallyPlaced s

instance Archetype PhysicallyPlaced s where
  type Archetype
-}
--instance Archetype Physical where
