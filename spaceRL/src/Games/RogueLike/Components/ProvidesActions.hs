module Games.RogueLike.Components.ProvidesActions where

import Games.ECS

import Games.RogueLike.Types.Identifiers (ActionName)
import Data.Set (Set)
import qualified Data.Set as Set
import GHC.Generics
import Control.Lens    
data ProvidesActions = ProvidesActions {_providedActions :: Set ActionName} deriving stock (Eq, Show, Generic)
makeLenses ''ProvidesActions
instance Component ProvidesActions where
    type CanonicalName ProvidesActions = "providesActions"

makeHasComponentClass ''ProvidesActions        
           
