module Games.RogueLike.Components.EncyclopaediaEntry where

import Games.ECS
import Games.RogueLike.Types.Message
import GHC.Generics
import Control.DeepSeq    
data EncyclopaediaEntry = EncyclopaediaEntry {_entryText :: Message} deriving stock (Show, Generic)
                        deriving anyclass NFData
