module Games.RogueLike.Components.DamagesOnContact where
import           Games.ECS
import           GHC.Generics
import Games.ECS.World.TH
import Control.Lens
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.Damage    
data DamagesOnContact where
  DamagesOnContact :: {_damagePerTick :: DamageSpecification} -> DamagesOnContact
  deriving stock (Eq, Generic)

{-# INLINABLE renderDamagesOnContact #-}
renderDamagesOnContact :: DamagesOnContact -> Message
renderDamagesOnContact (DamagesOnContact dpt) = pretty ("does" :: String) <+> renderVerboseDamageSpecification dpt <+> " per tick"

instance Show DamagesOnContact where
  show = show . renderDamagesOnContact
  {-# INLINE show #-}
makeLenses ''DamagesOnContact
instance Component DamagesOnContact where
  type CanonicalName DamagesOnContact = "damagesOnContact"

makeHasComponentClass ''DamagesOnContact
