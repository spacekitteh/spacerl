module Games.RogueLike.Components.Kinematics where

import Control.DeepSeq
import Control.Lens
import Control.Lens.Iso
import Control.Lens.TH
import Data.Bits
import Data.Coerce

import Data.Foldable
import Data.Functor.Identity
import qualified Data.HashMap.Strict as HMS
import qualified Data.HashSet as HS
import Data.Hashable
import Data.Kind
import Data.Proxy
import Data.Sequence
import Data.Sequence.Lens
import Data.Word
import GHC.Exts
import GHC.Generics
import GHC.Types
import Games.ECS
import Games.ECS.Component
import Games.RogueLike.Types.Coordinates
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.SpaceFillingCodes
import Linear.Metric
import Linear.V3
import qualified Linear.V3 as V

-- | A 3-d position.
newtype Position = Position {_worldCoord :: WorldCoord}
  deriving stock (Eq, Show, Generic, Read)
  deriving newtype (Num)
makeLenses ''Position
instance Enum Position where
  {-# INLINE toEnum #-}
  toEnum = Position . hilbertDecodeV3 . HilbertCoded . fromIntegral
  {-# INLINE fromEnum #-}
  fromEnum (Position a) = fromIntegral encoded
    where
      (HilbertCoded encoded) = hilbertEncodeV3 a

{-# INLINE modulo #-}                               
modulo :: WorldCoordinateScalar -> Getter Position Position
modulo n = Control.Lens.to (\(Position v) -> Position (v <&> (`rem` n)))
                               
instance Bounded Position where
  minBound = toEnum minBound
  maxBound = toEnum maxBound



instance XMLPickler [Node] Position where
  xpickle = xpWrap (\(x', y', z') -> Position (V3 x' y' z')) (\(Position v) -> (v ^. _x, v ^. _y, v ^. _z)) (xpElemAttrs "position" (xp3Tuple (xpAttr "x" xpPrim) (xpAttr "y" xpPrim) (xpAttr "z" xpPrim)))

{-# INLINE renderPosition #-}
renderPosition :: Position -> Message
renderPosition = viaShow

{-# INLINEABLE neighbourPositions #-}
neighbourPositions :: Fold Position Position
neighbourPositions = folding (\(Position pos) -> [Position (V3 ((pos ^. V._x) + dx) ((pos ^. V._y) + dy) ((pos ^. V._z) + dz)) | dx <- [-1 .. 1], dy <- [-1 .. 1], dz <- [-1 .. 1], not (dx == 0 && dy == 0 && dz == 0)])

{-# INLINE distance #-}
distance :: Position -> Position -> Double
distance (Position !a) (Position !b) = Linear.Metric.distance (fmap fromIntegral a) (fmap fromIntegral b)

{-# INLINE distance2D #-}
distance2D :: Position -> Position -> Double
distance2D a b = d * (dx' + dy') + (d2 - 2 * d) * (min dx' dy')
  where
    dx' = fromIntegral $ abs (a ^. x - b ^. x)
    dy' = fromIntegral $ abs (a ^. y - b ^. y)
    d = 1
    d2 = sqrt 2

{-# INLINEABLE directPath #-}

-- | A digital line via Bresenham's algorithm from the start to the end, inclusive.
directPath :: Position -> Position -> Seq Position
directPath start end = start :<| mostly
  where
    goal = start
    a = end
    (mostly) = worker SPEC (dx' + dy') (a ^. x) (a ^. y) empty -- (singleton a)
    z' = goal ^. z
    dx' = abs $ a ^. x - goal ^. x
    dy' = -(abs $ a ^. y - goal ^. y)
    sx = if a ^. x < goal ^. x then 1 else -1
    sy = if a ^. y < goal ^. y then 1 else -1
    worker :: SPEC -> WorldCoordinateScalar -> WorldCoordinateScalar -> WorldCoordinateScalar -> Seq Position -> Seq Position
    worker !sPEC _ x0 y0 acc | x0 == goal ^. x && y0 == goal ^. y = acc
    worker !sPEC err x0 y0 acc = worker sPEC err' x0' y0' ((Position (V3 x0 y0 z')) :<| acc)
      where
        e2 = 2 * err
        (x0', err1) = if e2 >= dy' then (x0 + sx, err + dy') else (x0, err)
        (y0', err') = if e2 <= dx' then (y0 + sy, err1 + dx') else (y0, err1)

{-# INLINE crowFliesDistance #-}
crowFliesDistance :: Position -> Position -> Double
crowFliesDistance (Position !a) (Position !b) = Linear.Metric.distance (fmap fromIntegral a) (fmap fromIntegral b)


-- TODO: Define Ord and Hashable instances based based on Morton coding

{-# INLINE x #-}
x :: Lens' Position WorldCoordinateScalar
x = (coerced @Position @Position @WorldCoord) . _x

{-# INLINE y #-}
y :: Lens' Position WorldCoordinateScalar
y = (coerced @Position @Position @WorldCoord) . _y

{-# INLINE z #-}
z :: Lens' Position WorldCoordinateScalar
z = (coerced @Position @Position @WorldCoord) . _z

instance Component Position where
  type CanonicalName Position = "position"

makeHasComponentClass ''Position




{-# INLINE mortonCoded #-}
mortonCoded :: Iso' Position MortonCoded
mortonCoded = worldCoord . coded

{-# INLINE hilbertCoded #-}
hilbertCoded :: Iso' Position HilbertCoded
hilbertCoded = worldCoord . coded

instance Ord Position where
  compare a b = compare (a ^. hilbertCoded) (b ^. hilbertCoded)
  {-# INLINE compare #-}

instance Hashable Position where
  {-# INLINE hash #-}
  hash p = fromIntegral $ p ^. hilbertCoded --mortonCoded -- hilbertCoded
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt pos = salt `xor` (hash pos)




                      
data PositionChanged = PositionChanged {_oldPosition :: !Position} deriving (Eq, Show, Generic)

makeLenses ''PositionChanged

instance Component PositionChanged where
  type CanonicalName PositionChanged = "positionChanged"

instance FlagComponent PositionChanged

makeHasComponentClass ''PositionChanged

-- | A 3-d Velocity.
newtype Velocity = Velocity WorldCoord
  deriving newtype (Eq, Show, Num)
  deriving stock (Generic)
  deriving anyclass (Hashable)

instance XMLPickler [Node] Velocity where
  xpickle = xpWrap (\(x', y', z') -> Velocity (V3 x' y' z')) (\(Velocity v) -> (v ^. _x, v ^. _y, v ^. _z)) (xpElemAttrs "velocity" (xp3Tuple (xpAttr "dx" xpPrim) (xpAttr "dy" xpPrim) (xpAttr "dz" xpPrim)))

{-# INLINE renderVelocity #-}
renderVelocity :: Velocity -> Message
renderVelocity = viaShow

{-# INLINE dx #-}
dx :: Lens' Velocity WorldCoordinateScalar
dx = (coerced @Velocity @Velocity @WorldCoord) . _x

{-# INLINE dy #-}
dy :: Lens' Velocity WorldCoordinateScalar
dy = (coerced @Velocity @Velocity @WorldCoord) . _y

{-# INLINE dz #-}
dz :: Lens' Velocity WorldCoordinateScalar
dz = (coerced @Velocity @Velocity @WorldCoord) . _z

instance Component Velocity where
  type CanonicalName Velocity = "velocity"

makeHasComponentClass ''Velocity

-- | A step in a path.
data PathStep
  = -- | A position directly adjacent to the current position.
    Adjacent {_waypointPosition :: Position}
  | -- | A waypoint which may or may not be accessible via a straight line.
    Waypoint {_waypointPosition :: Position}
  | -- | A distant point that /is/ directly accessible via a straight line.
    DirectPath {_waypointPosition :: Position, _directPathRoute :: Seq Position}
  deriving stock (Eq, Show, Ord, Generic)
  deriving anyclass NFData

instance Hashable PathStep where
  {-# INLINEABLE hashWithSalt #-}
  hashWithSalt salt (Adjacent a) = hashWithSalt 1 (hashWithSalt salt a)
  hashWithSalt salt (Waypoint a) = hashWithSalt 2 (hashWithSalt salt a)
  hashWithSalt salt (DirectPath wp route) = 3 `hashWithSalt` wp `hashWithSalt` (hashWithSalt salt route)

makeLenses ''PathStep

newtype Path = Path (Seq PathStep)
  deriving stock (Eq, Show, Ord, Generic)
  deriving newtype (IsList, Semigroup, Monoid, NFData)

instance AsEmpty Path

instance Hashable Path where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (Path p) = hashWithSalt salt p

{-# INLINE CONLIKE singletonPath #-}
singletonPath :: PathStep -> Path
singletonPath = Path . Data.Sequence.singleton

{-# INLINE pathOf #-}
pathOf :: Getting Path s PathStep -> s -> Path
pathOf l = views l singletonPath

instance Cons Path Path PathStep PathStep where
  {-# INLINE _Cons #-}
  _Cons = coerced . (_Cons :: Prism' (Seq PathStep) (PathStep, Seq PathStep)) . coerced

instance Snoc Path Path PathStep PathStep where
  {-# INLINE _Snoc #-}
  _Snoc = coerced . (_Snoc :: Prism' (Seq PathStep) (Seq PathStep, PathStep)) . coerced

instance Each Path Path PathStep PathStep where
  each = coerced . (each @(Seq PathStep))
