module Games.RogueLike.Components.HealthStatus where
import Games.ECS
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.Effects
import Games.RogueLike.Types.SpawnEffects
import Games.RogueLike.Types.Lore
import Data.Maybe
import Data.Sequence
import GHC.Exts
import Data.Hashable
import GHC.Generics
import Control.Lens    



-- | TODO FIXME: Replace this with a Pool
data HealthStatus =
  HealthStatus 
    { _health :: !Double,
      _maximumHealth :: !Double--,
      -- | Damage that has occurred but has yet to be removed from the entity's health.
--      _unprocessedDamage :: Seq (Entity,Damage)
    } 
  deriving (Eq, Ord, Generic)
deriving instance Hashable HealthStatus
instance XMLPickler [Node] HealthStatus where
    xpickle = xpAlt (\(HealthStatus amt maxAmt) -> if amt >= maxAmt then 0 else 1)
              [ xpWrap (\mx -> HealthStatus mx mx) (\(HealthStatus _ mx) -> mx) (xpElemNodes "maximumHealth" (xpContent xpPrim)), --Implicit pickler for when max health
                xpWrap (\(a,b) -> HealthStatus a b) (\(HealthStatus a b) -> (a,b)) (xp2Tuple (xpElemNodes "health" (xpContent xpPrim)) (xpElemNodes "maximumHealth" (xpContent xpPrim)))] 

makeLenses ''HealthStatus

{-# INLINABLE renderHealthStatus #-}
renderHealthStatus :: HealthStatus -> Message
renderHealthStatus (HealthStatus h m {-u-}) =
  pretty ("Health:" :: String) <+>
  viaShow h <>
  pretty ("/"::String) <>
  viaShow m <+>
  pretty ("HP" :: String) {-<>
  case u == empty of
    True -> emptyDoc
    False -> pretty ("with unprocessed damage of" :: String) <+> list (toList $ fmap (renderDamage . snd) u)-}

instance Show HealthStatus where
  {-# INLINE show #-}
  show = show . renderHealthStatus
  
instance Component HealthStatus where
  type CanonicalName HealthStatus = "healthStatus"

instance AttributeComponent HealthStatus

makeHasComponentClass ''HealthStatus

data IsDead = IsDead deriving (Eq, Show, Generic)

instance Component IsDead where
  type CanonicalName IsDead = "isDead"

instance FlagComponent IsDead

makeHasComponentClass ''IsDead

data JustDied = JustDied {_causeOfDeath :: Maybe InGameEvent} deriving (Eq, Generic)

{-# INLINABLE renderJustDied #-}
renderJustDied :: JustDied -> Message
renderJustDied (JustDied cod) =
  pretty ("killed by" :: String) <+>
  fromMaybe (pretty ("nothing at all. Spooky!" :: String)) (fmap renderBareEntity $ (cod)^?_Just .effectData. causalEntity)

instance Show JustDied where
  {-# INLINE show #-}
  show = show . renderJustDied

instance Component JustDied where
  type CanonicalName JustDied = "justDied"

makeHasComponentClass ''JustDied


