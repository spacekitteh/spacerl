module Games.RogueLike.Components.ToolTip where

import           Data.Interned.Text
import           Games.ECS
import           Control.Lens
import Data.String
import GHC.Generics
import Control.DeepSeq
newtype ToolTipData = ToolTipData {_toolTipText :: InternedText} deriving newtype (Eq, Show, IsString)
                                                  deriving stock Generic
instance NFData ToolTipData where rnf = rwhnf    
newtype ToolTip = ToolTip {_text :: ToolTipData} deriving newtype (Eq, Show, IsString)
                                                  deriving stock Generic

makeLenses ''ToolTip

instance Component ToolTip where
  type CanonicalName ToolTip = "tooltip"
makeHasComponentClass ''ToolTip


