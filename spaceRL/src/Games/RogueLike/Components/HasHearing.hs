module Games.RogueLike.Components.HasHearing where

import           Games.ECS
import           Control.Lens
import           GHC.Generics
data HasHearing = HasHearing {_sensitivity :: Double} deriving (Eq, Show, Generic)
makeLenses ''HasHearing

instance Component HasHearing where
  type CanonicalName HasHearing = "hasHearing"
instance SensoryComponent HasHearing
instance AttributeComponent HasHearing

makeHasComponentClass ''HasHearing
