module Games.RogueLike.Components.Inventory where

import           Games.ECS
import           Games.RogueLike.Types.PhysicalTypes
import           Data.HashSet
import           Control.Lens
import           GHC.Generics
import Games.ECS.World.TH
data Inventory = Inventory {_capacity :: Mass, _contents :: EntitySet} deriving (Eq, Show, Generic)

makeLenses ''Inventory

instance Component Inventory where
  type CanonicalName Inventory = "inventory"
instance CompositeComponent Inventory where
  type SubComponentIndex Inventory = Entity
  {-# INLINE subComponentReferences #-}
  subComponentReferences =  contents . knownEntities.selfIndex
makeHasComponentClass ''Inventory
