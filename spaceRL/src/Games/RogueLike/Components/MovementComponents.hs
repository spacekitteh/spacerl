module Games.RogueLike.Components.MovementComponents where
import GHC.Generics
import Control.Lens
import Games.ECS
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Types.PhysicalTypes
import Games.RogueLike.Types.Message
import Data.Hashable
import Numeric.Natural

    
data WantsToMove
  = WantsToMoveTo {_newPosition :: !Position}
  | WantsToMoveBy {_delta :: !Velocity}
  | WantsToMoveToEntity { _goalEntity :: !Entity}
  deriving stock (Eq, Generic)
  deriving anyclass Hashable

makeLenses ''WantsToMove



{-# INLINABLE renderWantsToMove #-}
renderWantsToMove :: WantsToMove -> Message
renderWantsToMove (WantsToMoveTo pos) = "wants to move to" <+> renderPosition pos
renderWantsToMove (WantsToMoveBy d) = "wants to move by" <+> renderVelocity d
renderWantsToMove (WantsToMoveToEntity critter) = "wants to follow" <+> renderBareEntity critter

instance Show WantsToMove where
  {-# INLINABLE show #-}
  show = show . renderWantsToMove



instance Component WantsToMove where
  type CanonicalName WantsToMove = "wantsToMove"
makeHasComponentClass ''WantsToMove
instance IntentComponent WantsToMove

{-# INLINABLE renderEntityWantsToMove #-}
renderEntityWantsToMove :: (UsingWantsToMove worldType Individual) => worldType Individual -> Message
renderEntityWantsToMove critter = case critter ^? wantsToMove of
  Nothing -> emptyDoc
  Just wtm -> renderWantsToMove wtm




data FollowingPath = FollowingPath {_path :: Path, _recalculateIn :: Maybe Int} deriving stock (Eq, Show, Generic)
                   deriving anyclass Hashable
makeLenses ''FollowingPath

instance Component FollowingPath where
    type CanonicalName FollowingPath = "followingPath"

makeHasComponentClass ''FollowingPath

data CanMove = CanMove deriving (Eq, Show, Generic)

instance Component CanMove where
  type CanonicalName CanMove = "canMove"
makeHasComponentClass ''CanMove
instance FlagComponent CanMove

instance CapabilityComponent CanMove where
  type Intent CanMove = WantsToMove

data BlocksMovement = BlocksMovement {_obstructionAmount :: (Obstruction Double)}
                      deriving stock (Eq, Ord, Generic)
                      deriving anyclass Hashable

makeLenses ''BlocksMovement


                                                             
{-# INLINE renderBlocksMovement #-}
renderBlocksMovement :: BlocksMovement -> Message
renderBlocksMovement (BlocksMovement (MovementPenalty x)) | isInfinite x = "blocks movement"
renderBlocksMovement (BlocksMovement (MovementPenalty x)) = "doesn't completely block movement, but has movement penalty of" <+> pretty x
instance Show BlocksMovement where
  {-# INLINE show #-}
  show = show . renderBlocksMovement

instance Component BlocksMovement where
  type CanonicalName BlocksMovement = "blocksMovement"
makeHasComponentClass ''BlocksMovement
instance FlagComponent BlocksMovement

data CanWalkOn = CanWalkOn
  deriving (Eq, Generic)
{-# INLINE renderCanWalkOn #-}
renderCanWalkOn :: CanWalkOn -> Message
renderCanWalkOn CanWalkOn = "can walk on"

instance Show CanWalkOn where
  {-# INLINE show #-}
  show = show . renderCanWalkOn

instance Component CanWalkOn where
  type CanonicalName CanWalkOn = "canWalkOn"

makeHasComponentClass ''CanWalkOn

instance FlagComponent CanWalkOn

data SupportsWeight
  = SupportsInfiniteWeight
  | Supports {_maxSupportedWeight :: !Mass}
  deriving stock (Eq, Generic)
  deriving anyclass Hashable
makeLenses ''SupportsWeight

{-# INLINABLE renderSupportsWeight #-}
renderSupportsWeight :: SupportsWeight -> Message
renderSupportsWeight SupportsInfiniteWeight = "supports infinite weight"
renderSupportsWeight (Supports wt) = "supports load up to" <+> renderMass wt

instance Show SupportsWeight where
  {-# INLINE show #-}
  show = show . renderSupportsWeight

instance Component SupportsWeight where
  type CanonicalName SupportsWeight = "supportsWeight"

makeHasComponentClass ''SupportsWeight
data EntityMoved = EntityMoved deriving (Eq, Show)


-- data MovementPenalty = MovementPenalty {_initiativeLoss :: !Double}
--   deriving stock (Eq, Show, Ord, Generic)
--   deriving anyclass Hashable

-- instance Semigroup MovementPenalty where
--   {-# INLINE (<>) #-}
--   (MovementPenalty il1) <> (MovementPenalty il2) = MovementPenalty (il1+il2)
-- instance Monoid MovementPenalty where
--   {-# INLINABLE mempty #-}
--   mempty = MovementPenalty 0
-- makeLenses ''MovementPenalty
-- instance Component MovementPenalty where
--   type CanonicalName MovementPenalty = "movementPenalty"
-- makeHasComponentClass ''MovementPenalty

-- {-# INLINE initiativeLossToMovementCost #-}
-- initiativeLossToMovementCost :: MovementPenalty -> Double
-- -- SEE ALSO WHEN UPDATING: MapIndexingSystem.directPathCost
-- initiativeLossToMovementCost mp = 1 + mp^.initiativeLoss
movementPenaltyToInitiativeCost :: Double -> Double
movementPenaltyToInitiativeCost = succ
