{-# LANGUAGE DerivingVia #-}
module Games.RogueLike.Components.Sensory
    (
     Viewshed(Viewshed), dirty, visiblePositions, newViewshed, ViewshedDirtyReason(..),

     HasVision(HasVision), visionRange, viewshed, UsingHasVision(..), HasHasVision(..),

     positionVisibleToEntity, entityFieldOfView,

     BlocksVision(BlocksVision), UsingBlocksVision(..), HasBlocksVision(..),
    )
     where

import Control.Lens
import Data.HashSet as HS
import Data.Hashable
import GHC.Generics
import Games.ECS
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Types.Coordinates
import Control.DeepSeq
import Data.Default.Class
import GHC.Generics    
data ViewshedDirtyReason = FreshlyInitialised | OriginMoved | BlockerChanged | SensorAttributeChanged deriving (Eq, Show, Generic, NFData)

deriving instance Hashable ViewshedDirtyReason

data Viewshed = Viewshed
  { _dirty :: Maybe ViewshedDirtyReason,
    _visiblePositions :: HS.HashSet Position
  }
  deriving (Eq, Show, Generic, NFData)

makeLenses ''Viewshed

{-# NOINLINE newViewshed #-}
newViewshed :: Viewshed
newViewshed = Viewshed (Just FreshlyInitialised) HS.empty

instance Default Viewshed where
    def = newViewshed
deriving via (ElideAsDefault Viewshed) instance XMLPickler [Node] Viewshed
instance {-# OVERLAPPING #-} (Selector c) => GXmlPickler [Node] (S1 c (K1 i Viewshed)) where
    gxpickleContentsf _ = xpWrap (M1 . K1 . unElideAsDefault) (ElideAsDefault . unK1 . unM1) xpickle
         
data HasVision = HasVision
  {
    _visionRange :: WorldCoordinateScalar,
    _viewshed :: Viewshed
  }
  deriving (Eq, Show, Generic)

makeLenses ''HasVision

instance Component HasVision where
  type CanonicalName HasVision = "hasVision"

instance SensoryComponent HasVision

instance AttributeComponent HasVision

makeHasComponentClass ''HasVision

{-# INLINE positionVisibleToEntity #-}
positionVisibleToEntity :: UsingHasVision worldType Individual => worldType Individual -> Position -> Bool
positionVisibleToEntity critter pos =
  if has hasVision critter
    then HS.member pos (critter ^?! hasVision . viewshed . visiblePositions)
    else False

{-# INLINE entityFieldOfView #-}
entityFieldOfView :: UsingHasVision worldType Individual => Fold (worldType Individual) Position
entityFieldOfView = hasVision . viewshed . visiblePositions . folded

data BlocksVision = BlocksVision deriving (Eq, Show, Generic)

instance Component BlocksVision where
  type CanonicalName BlocksVision = "blocksVision"

makeHasComponentClass ''BlocksVision

instance FlagComponent BlocksVision
