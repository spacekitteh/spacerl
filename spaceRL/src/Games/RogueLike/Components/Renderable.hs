module Games.RogueLike.Components.Renderable where

import Control.Lens
import Data.Hashable
import GHC.Generics
import Games.ECS
import Games.RogueLike.Types.Glyph
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.Voxel
import Data.Int    
data Renderable = Renderable {_glyph :: Glyph, _zHeight :: !Int8, _voxel :: !Voxel}
  deriving stock (Eq, Generic)
  deriving anyclass Hashable
makeLenses ''Renderable



{-# INLINE renderRenderableToMessage #-}
renderRenderableToMessage :: Renderable -> Message
renderRenderableToMessage (Renderable g _ _) = renderGlyphToMessage g

instance Show Renderable where
  {-# INLINE show #-}
  show = show . renderRenderableToMessage

instance Component Renderable where
  type CanonicalName Renderable = "renderable"
  type Storage Renderable = InternedComponentStore

makeHasComponentClass ''Renderable


data AnimatedRenderable = AnimatedRenderable {_glyphSequence :: GlyphAnimationSequence, _animationFrame :: Int, _timeUntilNextFrame :: Double}
    deriving stock (Eq, Generic, Show)
    deriving anyclass Hashable
makeLenses ''AnimatedRenderable

instance Component AnimatedRenderable where
    type CanonicalName AnimatedRenderable = "currentAnimation"
        
makeHasComponentClass ''AnimatedRenderable
