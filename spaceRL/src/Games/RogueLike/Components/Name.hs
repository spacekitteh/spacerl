module Games.RogueLike.Components.Name where
import           Games.ECS
import GHC.Generics
import Control.Lens    
import Games.ECS.World.TH
import           Data.Text (Text)
import           Data.Hashable
import           Data.String
import Prettyprinter
import Games.RogueLike.Types.Message
import Data.Interned
import Data.Interned.Text
import Games.RogueLike.Types.Identifiers
import Control.DeepSeq
-- | An entity might have a human-friendly name.
newtype Name = Name AName
  deriving newtype (Eq, IsString, Hashable)
  deriving stock Generic
instance Pretty Name where
  pretty (Name n) = pretty (unintern n)
  {-# INLINE pretty #-}
renderName :: Name -> Message
renderName = pretty
{-# INLINE renderName #-}
instance Show Name where
  {-# INLINE show #-}
  show = show . renderName

instance Component Name where
  type CanonicalName Name = "name"
makeHasComponentClass ''Name
newtype ScrambledName = ScrambledName Text deriving newtype (Eq, Show, IsString, Hashable)
                                           deriving stock Generic

instance Component ScrambledName where
  type CanonicalName ScrambledName = "scrambledName"
makeHasComponentClass ''ScrambledName

renderEntityName :: (UsingName worldType Individual) => worldType Individual  -> Message
renderEntityName critter = annotate (ContentAnnotation (EntityKnownName (critter ^. entityReference))) (pretty (critter ^?! name)) -- TODO: Take care of missing names and scrambled names

