module Games.RogueLike.Components.ReceivesInput where
import GHC.Generics
    
import           Games.ECS
import Control.Lens
import Games.RogueLike.Types.Message


data ReceivesInput = ReceivesInput deriving stock (Eq, Generic)

renderReceivesInput :: ReceivesInput -> Message
renderReceivesInput ReceivesInput = pretty ("receives input" :: String)
instance Show ReceivesInput where
  show = show . renderReceivesInput
instance Component ReceivesInput where
  type CanonicalName ReceivesInput = "receivesInput"
makeHasComponentClass ''ReceivesInput
instance FlagComponent ReceivesInput
{-#INLINE receivesPlayerInput #-}
receivesPlayerInput ::
  (UsingReceivesInput worldType Individual)
  => EntityFilter worldType
receivesPlayerInput = entitiesWith withReceivesInput 
