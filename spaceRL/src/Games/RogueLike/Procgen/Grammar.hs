{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE UndecidableSuperClasses #-}  
-- | 

module Games.RogueLike.Procgen.Grammar where
import Data.Kind
import Control.Category
import Control.Arrow
--comonads, representables
import Data.Kind (Type)
import Control.Lens


data ProcGenGraph 
    
class (ProcGenChunk (LayerChunkType l)) => ProcGenChunkLayer l where
    type LayerChunkType l :: Type
    type CoordinateType l :: Type
    chunkSize :: CoordinateType l
    layerDependsOn :: Traversal' l (forall f. ProcGenChunkLayer f => f)
    getChunks :: CoordinateType l -> CoordinateType l -> Fold l (LayerChunkType l)
    

                 
data ProcGenLayerMonadT m a = ProcGenLayerMonad (m a)
                 
                 
class (ProcGenChunkLayer (ChunkLayerType c), c ~ LayerChunkType (ChunkLayerType c)) => ProcGenChunk c where
    type ChunkLayerType c :: Type
    layer :: c -> ChunkLayerType c
    
--instance (ProcGenChunk c1, ProcGenChunk c2) => ProcGenChunk (c1,c2) where
    

data Layer inputChunkType outputChunkType = Layer

instance Category Layer where
    id = Layer
    Layer . Layer = Layer
instance Arrow Layer where
    arr _ = Layer
    (Layer) *** (Layer) = Layer

    
