module Games.RogueLike.Assets.REXPaint
    (
     RexPaintLayer, theLayer,
     RexPaintFile, versionNumber, numberOfLayers,
     readRexPaintFileFromByteString
     )
    where
import Data.Text.CP437.Graphical
import Data.Interned    
import Control.Lens
import qualified Data.ByteString.Lazy as BS    
import qualified Data.ByteString.Lazy.Char8 as BSC
import Data.Colour
import GHC.Generics    
import Data.Int
import Data.Text (unpack)
import qualified Data.Text as T    
import Data.Binary
import Data.Binary.Get
import Data.Binary.Put    
import Debug.Trace
import Data.String (IsString (..))
import qualified Data.Vector as V (Vector)
import Codec.Compression.GZip (decompress)    
import Data.Vector.Generic as V hiding (Vector) 
import Data.Word
import Games.RogueLike.Types.Colour
import Games.RogueLike.Types.Coordinates (WorldCoord, WorldCoordinateScalar)
import Games.RogueLike.Types.Glyph
import System.ByteOrder (fromLittleEndian)

newtype RexPaintLayer = RexPaintLayer
  { _theLayer :: V.Vector (V.Vector Glyph)
  } deriving stock (Eq, Generic)

    
getGlyph :: Get Glyph
getGlyph = do
      glyphIndex <- getInt32le
      let sym = fromString [Data.Text.CP437.Graphical.byteToUtf {-toEnum -} . fromIntegral $ glyphIndex]
      fg <- simpleColourToColour <$> get
      bg <- simpleColourToColour <$> get
      pure $! Glyph sym fg bg
putGlyph :: Glyph -> Put
putGlyph (Glyph sym fg bg) = do
      putInt32le (fromIntegral . {-fromEnum -} utfToByte .  Prelude.head  . unpack . unintern $ sym)
      put (colourToSimpleColour fg)
      put (colourToSimpleColour bg)
instance Binary RexPaintLayer where
    get = do
      width <- getInt32le
      height <- getInt32le
      layerVector <- replicateM (fromIntegral width)
                     (  do
                         replicateM (fromIntegral height)
                          getGlyph)
      pure $! RexPaintLayer layerVector
    put (RexPaintLayer l) = do
      putInt32le (fromIntegral $ V.length l)
      putInt32le (fromIntegral $ V.length (l ! 0))
      V.mapM_ (\column -> V.mapM_ putGlyph column ) l
                             
makeLenses ''RexPaintLayer

type instance Index RexPaintLayer = (Word32, Word32)

type instance IxValue RexPaintLayer = Glyph

instance Ixed RexPaintLayer where
  {-# INLINE ix #-}
  ix (x', y') = theLayer . ix (fromIntegral x') . ix (fromIntegral y') -- REXPaint files are COLUMN-MAJOR!

instance Show RexPaintLayer where
    show (RexPaintLayer l) = let asText = fmap ( T.pack . Prelude.concat . (fmap show)) l
                             in unpack . T.unlines . T.transpose . V.toList $ asText
                                   
                                   
                
data RexPaintFile = RexPaintFile
  { _versionNumber :: Int32,
    _numberOfLayers :: Word32,
    _imageLayers :: V.Vector RexPaintLayer
  } deriving stock (Eq, Show, Generic)

-- | Assumes a non-compressed file
instance Binary RexPaintFile where
  get = do
      _versionNumber <- getInt32le
      _numberOfLayers <- getWord32le
      _imageLayers <- V.replicateM (fromIntegral _numberOfLayers) get
      pure $! RexPaintFile {..}
  put (RexPaintFile version layerCount layers) = do
                               putInt32le version
                               putWord32le layerCount
                               V.mapM_ put layers
                      

      
makeLenses ''RexPaintFile                  
type instance Index RexPaintFile = (Word32, Word32, Word32)
type instance IxValue RexPaintFile = Glyph
instance Ixed RexPaintFile where
    {-# INLINE ix #-}
    ix (x', y', z') = imageLayers . ix (fromIntegral z') . ix (x', y') 


-- | Deserialise a `.xp` file.
readRexPaintFileFromByteString :: BS.ByteString -> RexPaintFile
readRexPaintFileFromByteString = decode . decompress
