-- | In this module, we only have datatypes of two types:
-- 1. Entity references
-- 2. Interned text

module Games.RogueLike.Types.Identifiers where
import Games.ECS
import Data.Hashable
import Data.Interned.Text
import Data.String
import GHC.Generics
import Control.DeepSeq
import Data.Interned
import Data.Text (Text)
import Data.Coerce    
import qualified Data.Text as T
import GHC.Read    
newtype AName = AName {_aName :: InternedText} deriving newtype (Eq, Ord, Show, IsString, Hashable, XMLPickleAsAttribute)
                                          deriving stock Generic

instance NFData AName where rnf = rwhnf                                                   
instance Interned AName where
    newtype Description AName = DT Text deriving newtype (Eq, Hashable)

    type Uninterned AName = Text
    describe = DT
    identify i t = AName $ identify i t
    cache = nameCache
{-# NOINLINE nameCache #-}
nameCache :: Cache AName
nameCache = mkCache
            
instance Uninternable AName where
    unintern (AName n) = unintern n

instance Read AName where
    readPrec  = fmap (AName . intern) $ readPrec
    readListPrec = readListPrecDefault
newtype TriggerName = TriggerName {_triggerName :: AName} deriving newtype (Eq, Ord, Show, IsString, Hashable, NFData)
                                          deriving stock Generic
newtype Flag = Flag {_flagName :: AName}
  deriving stock (Eq, Ord, Generic, Show)         -- There should be a component with just a set of these; with
                                                  -- FlagSystem which checks for known FlagComponents before
                                                  -- checking for these.
                                                  
  deriving newtype (Hashable, XMLPickleAsAttribute, NFData)



newtype EventDescription = EventDescription {_eventDescription :: AName} deriving newtype (Eq, Ord, Show, IsString, Hashable, XMLPickleAsAttribute, NFData)
                                          deriving stock Generic

newtype DamageTypeName = DamageTypeName {_damageTypeName :: AName} deriving newtype (Eq, Ord, Show, IsString, Hashable, XMLPickleAsAttribute, NFData)
                                          deriving stock Generic
newtype FactionName = FactionName {_factionName :: AName} deriving newtype (Eq, Ord, Show, IsString, Hashable, XMLPickleAsAttribute, NFData)
                                          deriving stock Generic
-- | E.g. The part relative to the parent entity. So, e.g., "Left leg" or "Ring finger", but NOT "Left ring finger".
newtype PartName = PartName {_partName :: AName} deriving newtype (Eq, Show, Ord, Hashable, IsString)
                                         deriving newtype (XMLPickleAsAttribute, NFData)
                                         deriving stock Generic


-- | E.g. "Leg" or "Finger"
newtype PartTypeName = PartTypeName {_partTypeName :: AName} deriving newtype (Eq, Show, Ord, Hashable, IsString, XMLPickleAsAttribute, NFData)
                                                 deriving stock Generic
-- | E.g. "Hauling resources"
newtype TutorialName = TutorialName {_tutorialName :: AName} deriving newtype (Eq, Show, Ord, Hashable, IsString, XMLPickleAsAttribute, NFData)
                                                 deriving stock Generic

newtype ResourceClassName = ResourceClassName {_resourceClassName :: AName} deriving newtype (Eq, Show, Ord, Hashable, IsString, XMLPickleAsAttribute, NFData)
                                                 deriving stock Generic
newtype TargetClassName = TargetClassName {_targetClassName :: AName} deriving newtype (Eq, Show, Ord, Hashable, IsString, XMLPickleAsAttribute, NFData)
                                                 deriving stock Generic
newtype ActionName = ActionName {_actionName :: AName} deriving newtype (Eq, Show, Ord, Hashable, IsString, XMLPickleAsAttribute, NFData)
                                                 deriving stock Generic
