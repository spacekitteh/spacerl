{-# LANGUAGE PatternSynonyms #-}
module Games.RogueLike.Types.Particle where
import Games.RogueLike.Types.Glyph
import Data.Hashable
import Games.ECS
import Games.RogueLike.Components.Kinematics ( Path)
import Games.RogueLike.Types.Damage
import Games.RogueLike.Types.Identifiers
import GHC.Generics
import Control.Lens    


    
data ParticleKinematicBehaviour =
    -- | Attaches to the target
    StainsTarget
    | MovementSequence Path
  deriving stock (Eq, Ord, Show, Generic)
pattern StationaryParticle :: ParticleKinematicBehaviour
pattern StationaryParticle = MovementSequence Empty
                             
data ParticleAppearance = ParticleAppearance GlyphAnimationSequence

data Particle = Particle ParticleKinematicBehaviour ParticleAppearance
    -- | Lifetime remaining
    (Maybe Double)
