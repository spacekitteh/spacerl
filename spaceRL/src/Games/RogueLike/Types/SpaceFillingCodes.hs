{-# LANGUAGE MagicHash #-}
{-# OPTIONS_GHC "-fllvm" "-O3" "-mbmi2" #-}
{-# LANGUAGE BangPatterns #-}
module Games.RogueLike.Types.SpaceFillingCodes (mortonEncode3d, hilbertEncode3d, mortonEncode2d, hilbertEncode2d, mortonDecode3d, hilbertDecode3d, mortonDecode2d, hilbertDecode2d, MortonCoded (MortonCoded), HilbertCoded (HilbertCoded), mortonToHilbert, hilbertToMorton, hilbertDecodeV3, mortonDecodeV3, hilbertEncodeV3, mortonEncodeV3, SpaceFillingCurve(..), HilbertMap(HilbertMap), insertWith) where
import GHC.Exts (IsList(..))

import Control.Lens
import Data.Bits
-- import Data.Bits.Pdep
-- import Data.Bits.Pext
import Data.Coerce
import Data.Hashable
import Data.Int
import Data.Primitive -- Data.Vector.Generic
import Data.Proxy
import Data.Word
import GHC.Generics hiding (from)
import GHC.Prim (pdep64#, pext64#)
import GHC.Types
import qualified Data.IntMap.Strict as IM
import qualified Data.IntMap.Lazy as IML    
import GHC.Word
import Games.ECS.Serialisation
import Games.RogueLike.Types.Coordinates
import Linear.V3
import Control.DeepSeq
-- from http://threadlocalmutex.com/?p=149
mortonToHilbertTable, hilbertToMortonTable :: ByteArray -- Vector Word8
mortonToHilbertTable =
  byteArrayFromListN
    96
    [ 48 :: Word8,
      33,
      27,
      34,
      47,
      78,
      28,
      77,
      66,
      29,
      51,
      52,
      65,
      30,
      72,
      63,
      76,
      95,
      75,
      24,
      53,
      54,
      82,
      81,
      18,
      3,
      17,
      80,
      61,
      4,
      62,
      15,
      0,
      59,
      71,
      60,
      49,
      50,
      86,
      85,
      84,
      83,
      5,
      90,
      79,
      56,
      6,
      89,
      32,
      23,
      1,
      94,
      11,
      12,
      2,
      93,
      42,
      41,
      13,
      14,
      35,
      88,
      36,
      31,
      92,
      37,
      87,
      38,
      91,
      74,
      8,
      73,
      46,
      45,
      9,
      10,
      7,
      20,
      64,
      19,
      70,
      25,
      39,
      16,
      69,
      26,
      44,
      43,
      22,
      55,
      21,
      68,
      57,
      40,
      58,
      67
    ]
hilbertToMortonTable =
  byteArrayFromListN
    96
    [ 48 :: Word8,
      33,
      35,
      26,
      30,
      79,
      77,
      44,
      78,
      68,
      64,
      50,
      51,
      25,
      29,
      63,
      27,
      87,
      86,
      74,
      72,
      52,
      53,
      89,
      83,
      18,
      16,
      1,
      5,
      60,
      62,
      15,
      0,
      52,
      53,
      57,
      59,
      87,
      86,
      66,
      61,
      95,
      91,
      81,
      80,
      2,
      6,
      76,
      32,
      2,
      6,
      12,
      13,
      95,
      91,
      17,
      93,
      41,
      40,
      36,
      38,
      10,
      11,
      31,
      14,
      79,
      77,
      92,
      88,
      33,
      35,
      82,
      70,
      10,
      11,
      23,
      21,
      41,
      40,
      4,
      19,
      25,
      29,
      47,
      46,
      68,
      64,
      34,
      45,
      60,
      62,
      71,
      67,
      18,
      16,
      49
    ]

pdep :: Word64 -> Word64 -> Word64
pdep (W64# src#) (W64# mask#) = W64# (pdep64# src# mask#)

pext :: Word64 -> Word64 -> Word64
pext (W64# src#) (W64# mask#) = W64# (pext64# src# mask#)

{-# INLINEABLE transformCurve #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Word8 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Word16 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Word32 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Word64 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Word -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Int8 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Int16 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Int32 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Int64 -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy Int -> Word64 -> ByteArray -> Word64 #-}
{-# SPECIALIZE INLINE transformCurve :: Proxy WorldCoordinateScalar -> Word64 -> ByteArray -> Word64 #-}

transformCurve :: forall word. (Prim word, Bounded word, FiniteBits word) => Proxy word -> Word64 -> ByteArray -> Word64
transformCurve Proxy input table = go SPEC (3 * ((finiteBitSize @word minBound) - 1)) 0 0
  where
    go !sPEC !bitsLeft _transform !out | bitsLeft < 0 = out
    go !sPEC bitsLeft transformed out | otherwise = go sPEC (bitsLeft - 3) transform'' out'
      where
        transform' = fromIntegral $ ((table `indexByteArray` (fromIntegral (transformed .|. ((input `shiftR` bitsLeft) .&. 7)))) :: Word8)
        out' = (out `shiftL` 3) .|. (transform' .&. 7)
        transform'' = transform' .&. (complement 7)

{-# INLINEABLE mortonToHilbert #-}
{-# SPECIALIZE INLINE mortonToHilbert :: Proxy WorldCoordinateScalar -> MortonCoded -> HilbertCoded #-}
mortonToHilbert :: forall word. (Prim word, Bounded word, FiniteBits word) => Proxy word -> MortonCoded -> HilbertCoded
mortonToHilbert Proxy (MortonCoded w) = HilbertCoded $ transformCurve (Proxy @word) w mortonToHilbertTable

{-# INLINEABLE hilbertToMorton #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy WorldCoordinateScalar -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Word8 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Word16 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Word32 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Word64 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Word -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Int8 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Int16 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Int32 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Int64 -> HilbertCoded -> MortonCoded #-}
{-# SPECIALIZE INLINE hilbertToMorton :: Proxy Int -> HilbertCoded -> MortonCoded #-}

hilbertToMorton :: forall word. (Prim word, Bounded word, FiniteBits word) => Proxy word -> HilbertCoded -> MortonCoded
hilbertToMorton Proxy (HilbertCoded w) = MortonCoded $ transformCurve (Proxy @word) w hilbertToMortonTable

newtype HilbertCoded = HilbertCoded {_unHilbertCoded :: Word64}
  deriving newtype (Eq, Ord, Show, Hashable, Integral, Real, Num, Enum, NFData, Bits, FiniteBits)
  deriving (Generic)

instance XMLPickleAsAttribute HilbertCoded where
  pickleAsAttribute name = xpWrap (hilbertEncodeV3) (hilbertDecodeV3) (pickleAsAttribute name)

newtype MortonCoded = MortonCoded {_unMortonCoded :: Word64}
  deriving newtype (Eq, Ord, Show, Hashable, Integral, Real, Num, Enum, NFData, Bits, FiniteBits)
  deriving (Generic)

instance XMLPickleAsAttribute MortonCoded where
  pickleAsAttribute name = xpWrap (mortonEncodeV3) (mortonDecodeV3) (pickleAsAttribute name)

bmi_x_mask_3d, bmi_y_mask_3d, bmi_z_mask_3d, bmi_x_mask_2d, bmi_y_mask_2d :: Word64
bmi_x_mask_3d = 0x9249249249249249
bmi_y_mask_3d = 0x2492492492492492
bmi_z_mask_3d = 0x4924924924924924
bmi_x_mask_2d = 0x5555555555555555
bmi_y_mask_2d = 0xAAAAAAAAAAAAAAAA

class (Ord sfc, FiniteBits sfc, FiniteBits scalar) => SpaceFillingCurve scalar sfc where
    coded :: Iso' (V3 scalar) sfc
    
instance SpaceFillingCurve WorldCoordinateScalar MortonCoded where
    {-# INLINE coded #-}
    coded = iso (\(V3 !x' !y' !z') -> mortonEncode3d x' y' z') decoder
        where
          decoder w = V3 x'' y'' z''
              where
                (!x'', !y'', !z'') = mortonDecode3d w

instance SpaceFillingCurve WorldCoordinateScalar HilbertCoded where
    {-# INLINE coded #-}
    coded = coded . (iso (mortonToHilbert (Proxy @WorldCoordinateScalar)) (hilbertToMorton (Proxy @WorldCoordinateScalar)))



            
newtype HilbertMap a = HilbertMap {_unHilbertMap :: IM.IntMap a}
    deriving newtype (Eq, Ord, Show, Hashable, NFData)
    deriving stock (Functor, Foldable, Traversable, Generic, Generic1)

                
instance FunctorWithIndex HilbertCoded HilbertMap where
  {-# INLINE imap #-}
  imap f (HilbertMap m) = HilbertMap $ IM.mapWithKey ( (\i a -> f (fromIntegral i) a)) m
instance FoldableWithIndex HilbertCoded HilbertMap where
  {-# INLINE ifoldMap #-}
  {-# INLINE ifoldr #-}
  {-# INLINE ifoldl' #-}
  ifoldMap f (HilbertMap s) = IM.foldMapWithKey (f . fromIntegral) s
  ifoldr f b (HilbertMap s) = IM.foldrWithKey (f . fromIntegral) b s
  ifoldl' f b (HilbertMap s) = IM.foldlWithKey' (flip (f . fromIntegral)) b s

instance TraversableWithIndex  HilbertCoded HilbertMap where
  {-# INLINE itraverse #-}
  itraverse f (HilbertMap s) = fmap (\i -> HilbertMap i) $ itraverse (f . fromIntegral) s                            
                              



             
type instance Index (HilbertMap a) = HilbertCoded
type instance IxValue (HilbertMap a) = a


    
instance Ixed (HilbertMap a) where
    {-# INLINE ix #-}
    ix (HilbertCoded k) f (HilbertMap m) = HilbertMap <$> ix (fromIntegral k) f m
    
instance At (HilbertMap a) where
    {-# INLINE at #-}
    at (HilbertCoded k) f em@(HilbertMap m) =
        f mv <&> \r -> case r of
                         Nothing -> maybe em (const (coerce $ IM.delete (fromIntegral k) m)) mv
                         Just v' -> coerce (IM.insert (fromIntegral k) v' m)
        where
          mv = IM.lookup (fromIntegral k) m
    
instance AsEmpty (HilbertMap a) where
    {-# INLINE _Empty #-}
    _Empty = nearly (HilbertMap IM.empty) (\(HilbertMap m) -> IM.null m)

instance IsList (HilbertMap a) where
    type Item (HilbertMap a) = (V3 WorldCoordinateScalar, a)
    {-# INLINE fromList #-}
    fromList l = HilbertMap $ IM.fromList (fmap (\(e, a) -> (fromIntegral (e^.coded @_ @HilbertCoded), a)) l)
    {-# INLINE toList #-}
    toList  = (fmap (\(e, a) -> ( (fromIntegral @_ @HilbertCoded e )^.from coded, a))) . IM.toList . _unHilbertMap



              
{-# INLINE insertWith #-}
insertWith :: (a -> a -> a) -> HilbertCoded -> a -> HilbertMap a -> HilbertMap a
insertWith f !k !v (HilbertMap m) = HilbertMap (IM.insertWith f (fromIntegral k) v m)

                                     
{-# SPECIALIZE INLINE mortonEncode3d :: WorldCoordinateScalar -> WorldCoordinateScalar -> WorldCoordinateScalar -> MortonCoded #-}
{-# INLINEABLE mortonEncode3d #-}
mortonEncode3d :: forall w. (Bounded w, Integral w) => w -> w -> w -> MortonCoded
mortonEncode3d !x !y !z =
  MortonCoded $
    (pdep (((fromIntegral x) :: Word64) + (fromIntegral (minBound :: w)) :: Word64) bmi_x_mask_3d)
      .|. (pdep (((fromIntegral y) :: Word64) + (fromIntegral (minBound :: w)) :: Word64) bmi_y_mask_3d)
      .|. (pdep (((fromIntegral z) :: Word64) + (fromIntegral (minBound :: w)) :: Word64) bmi_z_mask_3d)

{-# SPECIALIZE INLINE hilbertEncode3d :: WorldCoordinateScalar -> WorldCoordinateScalar -> WorldCoordinateScalar -> HilbertCoded #-}
{-# INLINEABLE hilbertEncode3d #-}
hilbertEncode3d :: forall w. (Prim w, FiniteBits w, Bounded w, Integral w) => w -> w -> w -> HilbertCoded
hilbertEncode3d x y z = mortonToHilbert (Proxy @w) $ mortonEncode3d x y z

{-# INLINE hilbertEncodeV3 #-}
hilbertEncodeV3 :: V3 WorldCoordinateScalar -> HilbertCoded
hilbertEncodeV3 v = hilbertEncode3d (v ^. _x) (v ^. _y) (v ^. _z)

{-# INLINE mortonEncodeV3 #-}
mortonEncodeV3 :: V3 WorldCoordinateScalar -> MortonCoded
mortonEncodeV3 v = mortonEncode3d (v ^. _x) (v ^. _y) (v ^. _z)

{-# SPECIALIZE INLINE mortonEncode2d :: WorldCoordinateScalar -> WorldCoordinateScalar -> MortonCoded #-}
{-# INLINEABLE mortonEncode2d #-}
mortonEncode2d :: forall w. (Bounded w, Integral w) => w -> w -> MortonCoded
mortonEncode2d !x !y =
  MortonCoded $
    (pdep (((fromIntegral x) :: Word64) + (fromIntegral (minBound :: w)) :: Word64) bmi_x_mask_2d)
      .|. (pdep (((fromIntegral y) :: Word64) + (fromIntegral (minBound :: w)) :: Word64) bmi_y_mask_2d)

{-# SPECIALIZE INLINE hilbertEncode2d :: WorldCoordinateScalar -> WorldCoordinateScalar -> HilbertCoded #-}
{-# INLINEABLE hilbertEncode2d #-}
hilbertEncode2d :: forall w. (Prim w, FiniteBits w, Bounded w, Integral w) => w -> w -> HilbertCoded
hilbertEncode2d x y = mortonToHilbert (Proxy :: Proxy w) $ mortonEncode2d x y

{-# SPECIALIZE INLINE mortonDecode3d :: MortonCoded -> (WorldCoordinateScalar, WorldCoordinateScalar, WorldCoordinateScalar) #-}
{-# INLINEABLE mortonDecode3d #-}
mortonDecode3d :: (Bounded w, Integral w) => MortonCoded -> (w, w, w)
mortonDecode3d (MortonCoded !m) = (x, y, z)
  where
    x = (fromIntegral $ pext m bmi_x_mask_3d) - minBound
    y = (fromIntegral $ pext m bmi_y_mask_3d) - minBound
    z = (fromIntegral $ pext m bmi_z_mask_3d) - minBound

{-# SPECIALIZE INLINE hilbertDecode3d :: HilbertCoded -> (WorldCoordinateScalar, WorldCoordinateScalar, WorldCoordinateScalar) #-}
{-# INLINEABLE hilbertDecode3d #-}
hilbertDecode3d :: forall w. (Prim w, FiniteBits w, Bounded w, Integral w) => HilbertCoded -> (w, w, w)
hilbertDecode3d = mortonDecode3d . hilbertToMorton (Proxy @w)

{-# INLINE mortonDecodeV3 #-}
mortonDecodeV3 :: MortonCoded -> V3 WorldCoordinateScalar
mortonDecodeV3 m = V3 x' y' z'
  where
    (!x', !y', !z') = mortonDecode3d m

{-# INLINE hilbertDecodeV3 #-}
hilbertDecodeV3 :: HilbertCoded -> V3 WorldCoordinateScalar
hilbertDecodeV3 = mortonDecodeV3 . hilbertToMorton (Proxy @WorldCoordinateScalar)

{-# SPECIALIZE INLINE mortonDecode2d :: MortonCoded -> (WorldCoordinateScalar, WorldCoordinateScalar) #-}
{-# INLINEABLE mortonDecode2d #-}
mortonDecode2d :: (Bounded w, Integral w) => MortonCoded -> (w, w)
mortonDecode2d (MortonCoded !m) = (x, y)
  where
    x = (fromIntegral $ pext m bmi_x_mask_2d) - minBound
    y = (fromIntegral $ pext m bmi_y_mask_2d) - minBound

{-# SPECIALIZE INLINE hilbertDecode2d :: HilbertCoded -> (WorldCoordinateScalar, WorldCoordinateScalar) #-}
{-# INLINEABLE hilbertDecode2d #-}
hilbertDecode2d :: forall w. (Prim w, FiniteBits w, Bounded w, Integral w) => HilbertCoded -> (w, w)
hilbertDecode2d = mortonDecode2d . hilbertToMorton (Proxy @w)
