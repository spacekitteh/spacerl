{-# LANGUAGE UndecidableSuperClasses #-}
{-# LANGUAGE StrictData #-}
module Games.RogueLike.Types.DiceRoll where
import Control.DeepSeq    
import Control.Lens
import Data.Int
import Data.Word
import Prettyprinter
import GHC.Generics
import System.Random (RandomGen, uniformR)
import qualified Data.Sequence as DS
import Data.Hashable
import Data.Text (Text, pack)
import Data.Void
import Data.Foldable
import Text.Megaparsec
import Text.Megaparsec.Char
--import Text.Megaparsec.Char.Lexer
import Games.ECS.Serialisation
import Games.RogueLike.Util.Parsing hiding (parens)
import Control.Monad

data SelectRollChoice = Lowest | Highest
  deriving stock (Eq, Ord,Generic)
  deriving anyclass (Hashable, NFData)
data SelectRollStyle = Keep | Drop
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (Hashable, NFData)

instance Pretty SelectRollStyle where
  {-# INLINE pretty #-}
  pretty Keep = "keep"
  pretty Drop = "drop"
instance Show SelectRollStyle where
  {-# INLINE show #-}
  show = show . pretty



pRollStyle :: BasicParser SelectRollStyle
pRollStyle = choice
 [ Keep <$ (symbol' "keep" <|> symbol' "keeping"  ),
   Drop <$ (symbol' "drop" <|> symbol' "dropping" )
 ] <* spaceConsumer


instance Pretty SelectRollChoice where
  {-# INLINE pretty #-}
  pretty Highest = "highest"
  pretty Lowest = "lowest"
instance Show SelectRollChoice where
  {-# INLINE show #-}
  show = show . pretty

pRollChoice :: BasicParser SelectRollChoice
pRollChoice = choice
 [ Highest <$ (symbol' "highest" <|> symbol' "high" <|> symbol' "h"),
   Lowest <$ (symbol' "lowest" <|> symbol' "low" <|> symbol' "l")
 ] <* spaceConsumer


-- | A simple algebra for generating random numbers by simulated die rolls. The rolls can be biased by keeping or discarding the best or worst rolls.
data DiceAlgebra' nat diceValueType where
  Constant :: {_constant :: diceValueType} -> DiceAlgebra' nat diceValueType
  Plus :: {_lhs :: DiceAlgebra' nat diceValueType, _rhs :: DiceAlgebra' nat diceValueType} -> DiceAlgebra' nat diceValueType
  Minus :: {_lhs :: DiceAlgebra' nat diceValueType, _rhs :: DiceAlgebra' nat diceValueType} -> DiceAlgebra' nat diceValueType
  Multiplier :: {_multiplier :: diceValueType,  _toMultiply :: DiceAlgebra' nat diceValueType} -> DiceAlgebra' nat diceValueType
  Roll :: {_numDice :: nat, _diceSize :: diceValueType, _keepOptions :: Maybe (SelectRollStyle, nat, SelectRollChoice)} -> DiceAlgebra' nat diceValueType -- ^ Roll <a>d<b>
 deriving stock (Eq, Ord, Generic,Generic1)
 deriving anyclass Hashable
 deriving anyclass NFData          
makeLenses ''DiceAlgebra'
type DiceAlgebra = DiceAlgebra' Int Double

pDVT :: RealFloat diceValueType => BasicParser diceValueType
pDVT = pReal

pNat :: Num nat => BasicParser nat
pNat = pUnsignedIntegral

pConst :: RealFloat diceValueType => BasicParser (DiceAlgebra' nat diceValueType)
pConst = Constant <$> pDVT <* notFollowedBy (try (symbol' "d") <|> try (symbol' "k") <|> try (symbol' "h") <|> (symbol' "l"))

pRoll :: (Num nat, RealFloat diceValueType) => BasicParser (DiceAlgebra' nat diceValueType)
pRoll = do
  _numDice <- pNat
  void (symbol "d")
  _diceSize <- pDVT
  _keepOptions <- optional . try $ do
    s <- pRollStyle
    (n,c) <- choice [ (try do
                        n <- pNat
                        c <- pRollChoice
                        pure (n,c)),
                      do
                        c <- pRollChoice
                        n <- pNat
                        pure (n,c)]
    spaceConsumer
    pure (s,n,c)
  pure Roll {..}

pMultiplier :: (Num nat, RealFloat diceValueType) => BasicParser (DiceAlgebra' nat diceValueType)
pMultiplier = do
  _multiplier <- pDVT
  void (symbol "*")
  _toMultiply <- pDiceAlgebra'
  pure Multiplier {..}

pPlus :: (Num nat, RealFloat diceValueType) => BasicParser (DiceAlgebra' nat diceValueType)
pPlus = do
  _lhs <- optParens pDiceAlgebra'
  void (symbol "+")
  _rhs <- optParens pDiceAlgebra'
  pure Plus {..}

pMinus :: (Num nat, RealFloat diceValueType) => BasicParser (DiceAlgebra' nat diceValueType)
pMinus = do
  _lhs <- optParens pDiceAlgebra'
  void (symbol "-")
  _rhs <- optParens pDiceAlgebra'
  pure Minus {..}

pDiceAlgebra' :: (Num nat, RealFloat diceValueType) => BasicParser (DiceAlgebra' nat diceValueType)
pDiceAlgebra' = spaceConsumer >> choice [try pConst, try pRoll, try pMultiplier, try pPlus, try pMinus] 

instance (Pretty nat, Pretty int) => Pretty (DiceAlgebra' nat int) where
  {-# INLINABLE pretty #-}
  pretty (Constant i) = pretty i
  pretty (Plus a b) = parens (pretty a) <+> "+" <+> pretty b
  pretty (Minus a b) = pretty a <+> "-" <+> pretty b
  pretty (Multiplier c r) = pretty c <> "*" <> parens (pretty r)
  pretty (Roll a x Nothing) = pretty a <> "d" <> pretty x
  pretty (Roll a x (Just (style, n, choice))) = pretty a <> "d" <> pretty x <+> pretty style <+> pretty choice <+> pretty n

instance (Pretty nat, Pretty dvt) => Show (DiceAlgebra' nat dvt) where
  {-# INLINE show #-}
  show = show . pretty

instance (Num nat, RealFloat diceValueType) => Read (DiceAlgebra' nat diceValueType) where
  {-# INLINE readsPrec #-}
  readsPrec _ s = case parseMaybe (pDiceAlgebra' <* eof) (pack s) of
    Nothing -> []
    Just a -> [(a,"")]
instance (Pretty nat, Pretty diceValueType, Num nat, RealFloat diceValueType) => XMLPickler [Node] (DiceAlgebra' nat diceValueType) where
  {-# INLINE xpickle #-}
  xpickle = ("dice algebra parsing","") <?+> xpContent xpPrim

  
{-# INLINE rollAdXkYpB #-}
rollAdXkYpB :: nat -> diceValueType -> SelectRollStyle -> nat -> SelectRollChoice -> diceValueType -> DiceAlgebra' nat diceValueType
rollAdXkYpB a x style y choice b = Plus (Roll a x (Just (style, y, choice))) (Constant b)

{-# INLINE rollAdXpB #-}
rollAdXpB :: nat -> diceValueType -> diceValueType -> DiceAlgebra' nat diceValueType
rollAdXpB a x b = a `d` x `p` b --Plus (Roll a x Nothing) (Constant b)

infixl 9 `d`
{-# INLINE d #-}
d :: nat -> diceValueType -> DiceAlgebra' nat diceValueType
d a x = Roll a x Nothing

infixl 8 `p`
{-# INLINE p #-}
p :: DiceAlgebra' nat diceValueType -> diceValueType -> DiceAlgebra' nat diceValueType
p d b  = Plus d (Constant b)

infixl 8 `m`
{-# INLINE m #-}
m :: DiceAlgebra' nat diceValueType -> diceValueType -> DiceAlgebra' nat diceValueType
m d b  = Minus d (Constant b)


{-# INLINABLE doRoll #-}
doRoll :: (RandomGen g) => DiceAlgebra -> g -> (Double, g)
doRoll (Constant i) g =  (i, g)
doRoll (Plus a b) g =  (a'+b', g'') where
  (a', g') = doRoll a g
  (b', g'') =  doRoll b g'
doRoll (Minus a b) g = (a'-b', g'') where
  (a', g') = doRoll a g
  (b', g'') = doRoll b g'
doRoll (Multiplier i r) g = (i*res, g') where
  (res, g') = (doRoll r g)
doRoll (Roll a x Nothing) g = 
   foldl' (\(acc, g') _ -> let (res, g'') = uniformR (1,x) g' in (acc + res, g'')) (0, g) [1..a] 
doRoll  (Roll a x (Just (style, n, choice))) g = let
    (vals, gres) =  foldl' (\(acc, g') _ -> let (res, g'') = uniformR (1,x) g' in (acc DS.|> res, g'')) (DS.Empty :: DS.Seq Double, g) [1..a]
    sortedVals = DS.unstableSort vals
  in case (style,choice) of
        (Keep, Highest) -> do
         ( sum (DS.drop (a - n) sortedVals), gres)
        (Keep, Lowest) -> do
          (sum (DS.take n sortedVals), gres)
        (Drop, Highest) -> do
          (sum (DS.take (a - n) sortedVals), gres)
        (Drop, Lowest) -> do
          (sum (DS.drop n sortedVals), gres)







