module Games.RogueLike.Types.Coordinates where

import Data.Int
import Games.ECS.Serialisation
import Linear.V2
import Linear.V3

type WorldCoordinateScalar = Int16

type HashingWorldCoordinateScalar = Int16

type ScreenCoord = V2 WorldCoordinateScalar

type UICoord = V3 WorldCoordinateScalar -- For z-height

type WorldCoord = V3 WorldCoordinateScalar
