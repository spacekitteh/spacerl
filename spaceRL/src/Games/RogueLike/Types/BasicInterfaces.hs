module Games.RogueLike.Types.BasicInterfaces (Adjacency (Adjacency), IsPosition (..), base, distanceTo, adjacentPosition) where

import Control.Lens
import Data.Hashable
import Data.Sequence
import Games.ECS
import Games.RogueLike.Types.Coordinates
import GHC.Generics
data Adjacency a = Adjacency
  { _base :: a,
    _distanceTo :: WorldCoordinateScalar,
    _adjacentPosition :: a
  }
  deriving (Eq, Show, Generic)

makeLenses ''Adjacency

-- | Hashing is assumed to preserve spatial locality (e.g. Morton coding)
class Hashable a => IsPosition a where
  -- | The adjacent positions to the input.
  adjacentPositions :: a -> (forall f. Traversable f => f (Adjacency a))

  -- | A shortest path from @a@ to @b@. If there are multiple shortest paths, then the choice is to be made deterministically.
  geodesic ::
    -- | @a@
    a ->
    -- | @b@
    a ->
    -- | The geodesic, in terms of a sequence of adjacent steps.
    Seq (Adjacency a)
