module Games.RogueLike.Types.Voxel (module Games.RogueLike.Types.Voxel, module Games.RogueLike.Types.Colour) where
import           Games.RogueLike.Types.Colour
import Games.RogueLike.Types.Texture
import Data.Interned.Text
import           GHC.Generics
import           Control.Lens
import Games.RogueLike.Types.Message
import Data.Hashable
import Control.DeepSeq 

data Voxel = TexturedVoxel {_textureMap :: !TextureMap}
             | PlainVoxel {_colour :: !Colour} 
     deriving stock (Eq, Show, Generic)
     deriving anyclass Hashable
makeLenses ''Voxel
instance NFData Voxel where rnf = rwhnf

           
blankVoxel :: Voxel
blankVoxel = PlainVoxel transparent
