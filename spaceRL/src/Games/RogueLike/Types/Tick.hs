module Games.RogueLike.Types.Tick where
import Numeric.Natural
import GHC.Generics
import Polysemy
import Polysemy.State
    
type GameTime = Natural
    
data Tick = Tick
            | TimerTock deriving Generic

class (Monad m) => MonadGameTime m where
  getGameTime :: m Natural
  putGameTime :: GameTime -> m ()


instance (Member (State GameTime) r) => MonadGameTime (Sem r) where
  {-# INLINE getGameTime #-}
  getGameTime = get
  {-# INLINE putGameTime #-}
  putGameTime = put
