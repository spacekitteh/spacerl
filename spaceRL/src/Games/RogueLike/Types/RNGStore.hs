{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE RoleAnnotations #-}

module Games.RogueLike.Types.RNGStore (RNGReason, RNGStore (RNGStore), initialiseRNGStore, RollsDice (..), getRNGForReason) where
import GHC.Generics
import Control.DeepSeq    
import Control.Lens    
import Control.Concurrent.STM.TVar
import Control.Exception.Assert.Sugar
import Control.Monad.STM
import Data.Bifunctor
import Data.Coerce
import qualified Data.HashMap.Strict as HMS
import Data.Hashable
import Data.Interned
import Data.Interned.Text
import Data.String (IsString)
import Data.Word
import GHC.Generics
import Games.ECS
import Games.ECS.Serialisation
import Games.RogueLike.Types.DiceRoll
import Polysemy
import Polysemy.AtomicState
import Prettyprinter
import System.IO.Unsafe (unsafePerformIO)
import System.Random
import System.Random.SplitMix

newtype RNGReason = RNGReason {_rngReason :: InternedText}
  deriving newtype (Eq, Ord, Hashable, IsString, XMLPickleAsAttribute)
  deriving stock (Generic)
instance NFData RNGReason where rnf = rwhnf
instance XMLPickler [Node] RNGReason where
  {-# INLINE xpickle #-}
  xpickle = xpWrap RNGReason _rngReason xpickle

instance Pretty RNGReason where
  {-# INLINEABLE pretty #-}
  pretty (RNGReason reason) = pretty (unintern reason)

instance Show RNGReason where
  {-# INLINEABLE show #-}
  show = show . pretty

type RNGGen = SMGen

instance XMLPickler [Node] RNGGen where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim

type RNGStoreImpl = HMS.HashMap RNGReason (TVar RNGGen)

data RNGStore = RNGStore {{-_baseGen :: RNGGen,-} _baseSeed :: !Int, _storeMap :: !RNGStoreImpl} deriving stock (Generic)
instance NFData RNGStore where rnf = rwhnf

convertStdGenToSplitMix :: Coercible RNGGen SMGen => RNGGen -> SMGen
convertStdGenToSplitMix = coerce

convertSplitMixToStdGen :: Coercible SMGen RNGGen => SMGen -> RNGGen
convertSplitMixToStdGen = coerce

type RNGStoreSerialisationMap = HMS.HashMap RNGReason SMGen

lowerRNGStoreMap :: RNGStoreImpl -> RNGStoreSerialisationMap
lowerRNGStoreMap = fmap (convertStdGenToSplitMix . unsafePerformIO . readTVarIO)

liftRNGStoreMap :: RNGStoreSerialisationMap -> RNGStoreImpl
liftRNGStoreMap = fmap (unsafePerformIO . newTVarIO . convertSplitMixToStdGen)

instance XMLPickler [Node] RNGStore where
  {-# INLINE xpickle #-}
  xpickle = xpWrap (\(base, theMap) -> RNGStore {-(convertSplitMixToStdGen base)-} base (liftRNGStoreMap theMap)) (\(RNGStore base theMap) -> ({-convertStdGenToSplitMix-} base, lowerRNGStoreMap theMap)) (xpPair ((xpElemNodes "baseSeed" (xpContent xpPrim))) (xpElemNodes "reasonMap" xpickle))

instance Show (RNGStore) where
  {-# INLINE show #-}
  show _ = "RNGStore"

makeLenses ''RNGStore

initialiseRNGStore :: Int -> RNGStore
initialiseRNGStore i = RNGStore i {-(mkSMGen (fromIntegral i))-} HMS.empty

{-# INLINEABLE addRNGReasons #-}
addRNGReasons :: RNGStore -> [RNGReason] -> STM RNGStore
addRNGReasons store [] = pure store
addRNGReasons (RNGStore {-bg-} seed sm) (r@(RNGReason reason) : reasons) = assert ((not $ HMS.member r sm) `blame` "Trying to create an RNG reason that already exists in the RNG store!" `swith` r) $ do
  let seedForReason = hashWithSalt seed (unintern reason)
  newGenVar <- newTVar (mkSMGen (fromIntegral seedForReason))
  --let (newGen1, newGen2) = split bg
  --newGenVar <- newTVar newGen2
  let newSM = HMS.insert r newGenVar sm
  addRNGReasons (RNGStore seed newSM) reasons

--addRNGReasons (RNGStore newGen1 newSM) reasons

{-# INLINEABLE getRNGForReason #-}
getRNGForReason :: RNGReason -> RNGStore -> STM (TVar RNGGen, RNGStore)
getRNGForReason reason store | Just res <- store ^. storeMap . at reason = pure (res, store)
getRNGForReason reason store = do
  newStore <- addRNGReasons store [reason]
  pure (newStore ^?! storeMap . at reason . _Just, newStore)

type instance Index RNGStore = RNGReason

type instance IxValue RNGStore = TVar RNGGen

instance Ixed RNGStore where
  {-# INLINE ix #-}
  ix i = storeMap . ix i

instance At RNGStore where
  {-# INLINE at #-}
  at i = storeMap . at i

class (Monad m) => RollsDice m where
  -- | The RNGReason is so we can have multiple parallel seeds for different reasons (e.g. map generation,
  -- combat damage, etc). You typically don't want combat damage to be able to affect map generation. This is
  -- purely for reproducible seeds.
  roll :: RNGReason -> DiceAlgebra -> m Double

instance (Members '[AtomicState RNGStore, Embed IO] r) => RollsDice (Sem r) where
  {-# INLINE roll #-}
  roll reason d = do
    oldStore <- atomicGet
    (genVar, newStore) <- embed . atomically $ getRNGForReason reason oldStore
    atomicPut newStore
    embed . atomically $ stateTVar genVar (doRoll d)
