{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
module Games.RogueLike.Types.PhysicalTypes  where
import           GHC.Generics
import Games.RogueLike.Types.Message
import Control.Lens
import Data.Coerce    
import Control.DeepSeq    
import Data.Hashable
import Control.Lens.Fold    
import Data.XML.Pickle
import Games.ECS.Serialisation
import Prettyprinter
import Data.Functor.Identity
import Data.Monoid
import Numeric.Natural
import GHC.Real
import Data.Default.Class
-- import Data.Metrology
-- import Data.Metrology.Show ()    
-- import Data.Metrology.SI
-- import Data.Metrology.SI.Mono
-- import qualified Data.Dimensions.SI as D
    
-- type instance DefaultUnitOfDim D.Mass = Kilo :@ Gram
-- type instance DefaultUnitOfDim D.Time = Second
-- type instance DefaultUnitOfDim D.Length = Meter
-- type instance DefaultUnitOfDim D.Current = Ampere
-- type instance DefaultUnitOfDim D.Temperature = Kelvin

-- instance XMLPi
    

-- TODO: Use dimensional types


deriving newtype instance Fractional (Sum Double)
newtype Obstruction a = MovementPenalty {_unMovementPenalty :: a}
                   deriving stock (Eq, Show, Ord, Read, Generic, Functor)
                   deriving anyclass (Hashable, NFData)
                   deriving newtype (Semigroup, Monoid)

instance {-#OVERLAPS #-} Hashable (Obstruction (Sum Double)) where
    hashWithSalt salt (MovementPenalty (Sum a)) = salt `hashWithSalt` a

isInfinitePenalty :: (Eq a, Fractional a) => Obstruction a -> Maybe a
isInfinitePenalty (MovementPenalty x) | fromRational infinity ==  x = Just x
isInfinitePenalty _ = Nothing
                      
pattern ObstructsCompletely :: (Eq a, Fractional a) => Obstruction a
pattern ObstructsCompletely <- (isInfinitePenalty -> Just _ ) where
    ObstructsCompletely = MovementPenalty (fromRational infinity)
makePrisms ''Obstruction

instance (Fractional a) => Default (Obstruction a) where
    def = MovementPenalty (fromRational infinity)
instance  {-# OVERLAPPING #-} (Eq (Obstruction a), Default (Obstruction a), XMLPickler [Node] a) => XMLPickler [Node] (Obstruction a) where
    xpickle = xpDefault def (xpElemNodes "movementPenalty" (xpWrap MovementPenalty _unMovementPenalty  xpickle))
           
_ObstructsCompletely :: forall a b. (Real a, Fractional a, Fractional b) =>  Prism (Obstruction a) (Obstruction b) a a
_ObstructsCompletely = prism (MovementPenalty . fromRational . toRational) (\case
                                                           ObstructsCompletely -> Right (fromRational infinity)
                                                           MovementPenalty a -> Left (MovementPenalty (fromRational . toRational $ a))
                                                         )
                                                             
                                                           

-- {-# INLINE obstructs #-}             
-- obstructs :: (Eq a, Monoid a) => Fold (Obstruction a) Bool
-- obstructs = folding (\a -> Identity (a == mempty))

instance Pretty a => Pretty (Obstruction a) where
    pretty (MovementPenalty a) = "obstructs with movement penalty" <+> pretty a

newtype Mass = Mass Double
  deriving stock Generic
  deriving newtype (Eq, Ord, Num,  Real, Fractional, Hashable, Read, NFData)

instance XMLPickler [Node] Mass where
    xpickle = xpClean $ xpContent xpPrim
{-# INLINE renderMass #-}
renderMass :: Mass -> Message
renderMass (Mass m) = (pretty m) <+> "kg"
instance Show Mass where
  {-# INLINE show #-}
  show = show . renderMass

newtype Density = Density Double
  deriving stock Generic
  deriving newtype (Eq, Ord, Num,  Real, Fractional, Hashable, Read, NFData)

instance XMLPickler [Node] Density where
    xpickle = xpClean $ xpContent xpPrim
{-# INLINE renderDensity #-}
renderDensity :: Density -> Message
renderDensity (Density m) = (pretty m) <+> "kg\x2062\&m⁻³"
instance Show Density where
  {-# INLINE show #-}
  show = show . renderDensity
         
