{-# LANGUAGE StrictData #-}
module Games.RogueLike.Types.Effects where

--import Games.RogueLike.Types.Lore
import Control.Lens
import GHC.Generics    
import Data.Function
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import Data.Hashable
import Data.Interned.Text
import Data.String
import Data.Word
import Games.ECS
import Games.ECS.Serialisation
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.Damage    
import Games.RogueLike.Types.Identifiers
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.RNGStore
import Prettyprinter
import Control.DeepSeq

newtype XP = XP Double
  deriving newtype (Eq, Ord, Show, Num, Hashable, NFData)
  deriving stock (Generic)

newtype EffectDuration = EffectDuration Word
  deriving newtype (Eq, Ord, Show, Num, Hashable, NFData)
  deriving stock (Generic)

data EffectTarget
  = TargetIndividual {_targetsEntity :: Entity}
  | TargetPosition {_targetsPosition :: Position}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

makeLenses ''EffectTarget

instance Pretty EffectTarget where
  {-# INLINE pretty #-}
  pretty = viaShow

data DoEffect a = DoEffect
  { _causalEntity :: Entity,
    _targets :: (HS.HashSet EffectTarget),
    _effectDetails :: a
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass NFData

makeLenses ''DoEffect

class Effect eff a m | a -> eff m where
  runEffectOnCritter ::
    (World world) =>
    world Storing ->
    world Individual ->
    eff ->
    DoEffect a ->
    m (world Individual)

{-
WIP: Description of event system

An event is a named token which can "happen". It may be "broadcast". Entities may be interested in certain
events, and can modify the event or even create new events to take its place. When an event is -finally-
handled, the result is to cause a series of /effects/ to occur (but effects can occur "higher up" in the event chain).

See the Incursion tech article:
https://web.archive.org/web/20150214122242/http://www.incursion-roguelike.org/TechPaper%20(Web%20Version).htm
(the source code is available on bitbucket: https://bitbucket.org/rmtew/incursion-roguelike/)

So is this just a message queue with registered interests?
-}

instance (Show a) => Pretty (DoEffect a) where
  {-# INLINE pretty #-}
  pretty = viaShow

deriving instance (Hashable a) => Hashable (DoEffect a)

{-# INLINE newEffect #-}
newEffect :: Foldable f => Entity -> f EffectTarget -> a -> DoEffect a
newEffect cause targs eff = (DoEffect cause (setOf folded targs) eff)

data ObservationReason
  = Seen
  | Touched
  | ImpartedForce
  | CausedDamage
  | BecauseISaySo
  deriving (Eq, Read, Show, Ord, Generic, NFData)

deriving instance Hashable ObservationReason

{-# INLINEABLE renderReason #-}
renderReason :: ObservationReason -> Message
renderReason ImpartedForce = "imparted force"
renderReason CausedDamage = "caused damage"
renderReason BecauseISaySo = "because I say so"
renderReason Seen = "seen"
renderReason Touched = "touched"

newtype Rank = Rank Word8
  deriving newtype (Eq, Ord, Show)
  deriving stock (Generic)
  deriving anyclass (Hashable, NFData)

data FactionRank = FactionRank {_socialRank :: Rank, _combatRank :: Rank} deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass NFData

deriving instance Hashable FactionRank

makeLenses ''FactionRank

-- TODO Add things like "avoid friendly fire on this faction"
data Opinion = Opinion Double deriving (Eq, Ord, Show, Generic, NFData)

deriving instance Hashable Opinion

data FactionReaction = Flee | Ignore | Attack | Help deriving (Eq, Ord, Show, Generic, NFData)

deriving instance Hashable FactionReaction

data FactionRelation = FactionRelation {_opinion :: Opinion, _reaction :: FactionReaction} deriving stock (Eq, Ord, Show, Generic)
                     deriving anyclass NFData

makeLenses ''FactionRelation

deriving instance Hashable FactionRelation

data Orders = PerformAttack Path | Patrol Path | Idle
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

newtype Allegiance = Allegiance {_allegianceFaction :: FactionName}
  deriving newtype (Eq, Show, Ord)
  deriving stock (Generic)
  deriving newtype (XMLPickleAsAttribute)
  deriving anyclass (Hashable, NFData)
instance XMLPickler [Node] Allegiance where
  xpickle = xpElemNodes "allegiance" (xpWrap Allegiance _allegianceFaction xpickle)
-- instance {-# OVERLAPS #-} XMLPickler [Node] Allegiance where
--   {-# INLINE xpickle #-}
--   xpickle = xpElemAttrs "allegiance" (xpAttribute "entRef" (xpWrap (Allegiance . EntRef) (unEntRef . _allegianceEntityReference) xpPrim))
-- <hasAllegiances><allegiance><allegiance entRef=\"2\"/></allegiance><ranks/></hasAllegiances>

makeLenses ''Allegiance
