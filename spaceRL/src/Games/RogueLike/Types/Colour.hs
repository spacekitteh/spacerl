module Games.RogueLike.Types.Colour where

import Data.Colour (AlphaColour, AffineSpace, over, black, opaque, withOpacity, alphaChannel)
import qualified Data.Colour
import Data.Colour.SRGB (RGB (..), toSRGB24, sRGB24, toSRGB)
import qualified Data.Colour.SRGB.Linear as DCSL
import Data.Word
import Data.Binary.Get
import Data.Text (unpack, pack, filter)
import GHC.Generics
import Data.Binary    
import Data.Hashable
import Games.ECS.Serialisation
import Data.Colour.Names (readColourName)
import Data.Char (isSpace)
import Control.DeepSeq    
newtype Colour = Colour {_rawColour :: (Data.Colour.AlphaColour Double)} deriving newtype (Eq, Show, Read, Semigroup, Monoid)
  deriving stock Generic

instance NFData Colour where rnf x= seq x ()
instance Ord Colour where
    compare a b = toRGBA a `compare` toRGBA b
                                    
newtype SimpleColour = SimpleColour (Data.Colour.Colour Double)

instance Binary SimpleColour where
    get = do
      r <- getWord8
      g <- getWord8
      b <- getWord8
      pure $! SimpleColour (sRGB24 r g b)
    put (SimpleColour c) = do
      let RGB r g b = toSRGB24 c
      putWord8 r
      putWord8 g
      putWord8 b

simpleColourToColour :: SimpleColour -> Colour
simpleColourToColour (SimpleColour c) = Colour . opaque $ c
colourToSimpleColour :: Colour -> SimpleColour
colourToSimpleColour c = SimpleColour (sRGB24 r g b) where
    (r,g,b) = toRGB c
transparent :: Colour
transparent = Colour Data.Colour.transparent

newtype EitherMonadFail a = EitherMonadFail {_unEitherMonadFail :: (Either String a)} deriving newtype (Functor, Applicative, Monad, Show)
instance MonadFail EitherMonadFail where
    fail = EitherMonadFail . Left
              
pickleExtColour :: (Ord a, Floating a, Read a, Show a) => PU [Node] (Data.Colour.Colour a)
pickleExtColour = ("Data.Colour.Colour", "") <?+> xpAlt (const 1) -- TODO: Test for named colours upon pickling
                                              [ -- name pickler
                                                (xpWrapEither (_unEitherMonadFail . readColourName . unpack . Data.Text.filter (not . isSpace)) (pack . show) (xpElemText "name")),
                                                -- rgb pickler
                                                xpWrap (\(r,g,b) -> DCSL.rgb r g b) ( \c -> case DCSL.toRGB c of RGB r g b -> (r,g,b) )
                                                           (xp3Tuple
                                                            (("red channel", "") <?+> xpElemNodes "r" (("red channel value", "") <?+> xpContent xpPrim))
                                                            (("green channel", "") <?+> xpElemNodes "g" (xpContent xpPrim))
                                                            (("blue channel", "") <?+> xpElemNodes "b" (xpContent xpPrim)))
                                              ]
pickleAlphaColour :: (Ord a, Floating a, Read a, Show a) => PU [Node] (Data.Colour.AlphaColour a)
pickleAlphaColour = ("Data.Colour.AlphaColour", "") <?+> xpWrap (\(c, o) -> c `withOpacity` o) (\ac -> (ac `over` Data.Colour.black, alphaChannel ac))
                      (xp2Tuple
                        pickleExtColour
                        (xpDefault 1 (xpElemNodes "opacity" (xpContent xpPrim))))
instance XMLPickler [Node] Colour where
  {-# INLINE xpickle #-}
  xpickle = xpWrap Colour _rawColour $ pickleAlphaColour
            
              
white, black :: Colour
white = fromRGBA (255,255,255,255)
black = fromRGB (255,255,255)
--      xpWrap (Colour . unAsString) (AsString . _rawColour) (xpContent xpPrim)

{-# INLINE toRGB #-}
toRGB :: Colour -> (Word8, Word8, Word8)
toRGB (Colour colour) = let (RGB r g b) = toSRGB24 (colour `over` Data.Colour.black) in (r, g, b)
                                          
{-# INLINE toRGBA #-}
toRGBA :: Colour -> (Double, Double, Double, Double)
toRGBA (Colour colour) = let a = alphaChannel colour
                             c = (colour `over` Data.Colour.black)
                             (RGB r g b) = toSRGB c
                           in (r,g,b,a)

{-# INLINE fromRGB #-}                                          
fromRGB :: (Word8, Word8, Word8) -> Colour
fromRGB (r,g,b) = Colour . opaque  $ sRGB24 r g b
                
{-# INLINE fromRGBA #-}
fromRGBA :: (Word8, Word8, Word8, Word8) -> Colour
fromRGBA (r, g, b, a) = Colour $ withOpacity  (sRGB24 r g b) (fromIntegral a / fromIntegral (maxBound :: Word8))
                                          
instance Hashable Colour where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (Colour col) = hashWithSalt salt (r,g,b) where
    (RGB r g b) = DCSL.toRGB (col `over` Data.Colour.black)
