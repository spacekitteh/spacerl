module Games.RogueLike.Types.Glyph (module Games.RogueLike.Types.Glyph, module Games.RogueLike.Types.Colour) where
import           Games.RogueLike.Types.Colour
import           Data.Text (Text)
import Data.Interned
import Data.String    
import Data.Interned.Text
import           GHC.Generics
import           Control.Lens
import Games.RogueLike.Types.Message
import Data.Hashable
import Games.ECS.Serialisation
import Data.Colour (opaque, blend)
import Data.Sequence
import Control.DeepSeq
data GlyphAnimationSequence' a = GlyphAnimationSequence {_frames :: (Seq a)}
                              deriving stock (Eq, Ord, Functor, Foldable, Traversable, Generic, Show)
                              deriving anyclass (Hashable, NFData)
makeLenses ''GlyphAnimationSequence'

-- todo: add a GlyphID?           
data Glyph = Glyph {_internedSymbol :: !InternedText, _foregroundColour :: !Colour, _backgroundColour :: !Colour}
    deriving stock (Eq, Ord, Generic)
instance NFData Glyph where rnf = rwhnf    
instance Hashable Glyph where
    hashWithSalt salt (Glyph sym _ _) = hashWithSalt salt sym
makeLenses ''Glyph

instance XMLPickler [Node] Glyph where
    xpickle = ("Glyph","")<?+> xpWrap  (\(sym,fg,bg) -> Glyph sym fg bg) (\(Glyph sym fg bg) -> (sym,fg,bg))
                (xp3Tuple
                 (("Symbol for glyph","") <?+> xpElemNodes "symbol" xpickle)
                 (xpDefault white (xpElemNodes "foregroundColour" xpickle))
                 (xpDefault transparent (xpElemNodes "backgroundColour" xpickle)))
           
data GlyphFrame = GlyphFrame { _glyph :: Glyph, _frameLength ::  Double} deriving stock (Eq, Generic, Show)
                deriving anyclass (Hashable, NFData)
type GlyphAnimationSequence = GlyphAnimationSequence' GlyphFrame

interned :: (Uninternable int, Uninterned int ~ unint) => Iso' int unint
interned = iso unintern intern
{-# INLINE interned #-}
{-# INLINE symbol #-}
symbol :: Lens' Glyph Text
symbol = internedSymbol . interned

{-# INLINE renderGlyphToMessage #-}
renderGlyphToMessage :: Glyph -> Message
renderGlyphToMessage g = annotate (ContentAnnotation $ WithColour (g ^. foregroundColour) (g ^. backgroundColour)) (pretty $ g ^. symbol)

instance Show Glyph where
  {-# INLINE show #-}
  show = show . renderGlyphToMessage

-- | Symbols and foreground colours are taken from the right.
instance Semigroup Glyph where
  {-# INLINE (<>) #-}
  (Glyph sa fa (Colour ba)) <> (Glyph sb fb (Colour bb)) | sb== " " || fb == transparent =  Glyph sa fa (Colour (blend 0.1 ba bb))
  (Glyph _ _ (Colour ba)) <> (Glyph s fb (Colour bb)) = Glyph s fb (Colour (blend 0.1 ba bb))
instance Monoid Glyph where
  {-# INLINE mempty #-}
  mempty = Glyph " " mempty mempty


           
