{-# LANGUAGE StrictData #-}
module Games.RogueLike.Types.Damage where
import Control.DeepSeq
import Control.Lens
import Data.Hashable
import GHC.Generics
import Games.ECS
import Games.ECS.World.TH
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.DiceRoll
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.RNGStore
import Prettyprinter

type HealthAmount = Double

data DamageSpecification = DamageSpecification {_amount :: Calculation, _damageType :: (Maybe Entity)}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

makeLenses ''DamageSpecification

constantDamageOfType :: RNGReason -> Double -> Maybe Entity -> DamageSpecification
constantDamageOfType dmgReason amt ty = DamageSpecification (constantCalculation dmgReason amt) ty

damageSpecificationByDiceRollCalculation :: RNGReason -> DiceAlgebra -> Maybe Entity -> DamageSpecification
damageSpecificationByDiceRollCalculation reason calc ty = DamageSpecification (diceRollCalculation reason calc) ty

annotateWithDamageSpecification :: DamageSpecification -> Message -> Message
annotateWithDamageSpecification (DamageSpecification amt Nothing) = annotateWithCalculation amt
annotateWithDamageSpecification (DamageSpecification amt (Just ty)) = annotateWithKnownName ty . annotateWithCalculation amt

renderVerboseDamageSpecification :: DamageSpecification -> Message
renderVerboseDamageSpecification (DamageSpecification amt Nothing) = "damage calculated by" <+> (annotateWithCalculation amt) (pretty amt)
renderVerboseDamageSpecification (DamageSpecification amt (Just ty)) = (annotateWithKnownName ty "damage") <+> "calculated by" <+> (annotateWithCalculation amt) (pretty amt)

-- | Information about damage to an entity
data Damage = Damage
  { _resolvedDamageAmount :: Maybe HealthAmount,
    _damageSpecification :: DamageSpecification
  }
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (Hashable, NFData)

makeLenses ''Damage

{-# INLINE damageCalculation #-}
damageCalculation :: Lens' Damage Calculation
damageCalculation = damageSpecification . amount

{-# INLINE resolveDamage #-}
resolveDamage :: (RollsDice m) => Damage -> m Damage
resolveDamage dmg = do
  damageToDo <- case dmg ^. resolvedDamageAmount of
    Nothing -> resolveCalculation (dmg ^. damageCalculation)
    Just d -> pure d
  pure (dmg & resolvedDamageAmount .~ Just damageToDo)

{-# INLINEABLE renderDamage #-}
renderDamage :: Damage -> Message
renderDamage (Damage (Just damageToDo) spec) = annotateWithDamageSpecification spec (pretty damageToDo)
renderDamage (Damage Nothing calc) = renderDamage (Damage (Just 0) calc)

{-# INLINEABLE renderVerboseDamage #-}
renderVerboseDamage :: Damage -> Message
renderVerboseDamage (Damage Nothing calc) = renderVerboseDamageSpecification calc
renderVerboseDamage (Damage (Just amt) calc) = viaShow amt <+> "HP,  from" <+> renderVerboseDamageSpecification calc

instance Show Damage where
  {-# INLINE show #-}
  show = show . renderVerboseDamage

instance Pretty Damage where
  {-# INLINE pretty #-}
  pretty = viaShow
