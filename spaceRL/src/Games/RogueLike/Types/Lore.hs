module Games.RogueLike.Types.Lore where
import Control.DeepSeq
import Data.Interned.Text
import Data.Hashable
import Data.String
import GHC.Generics
import Control.Lens
import Games.RogueLike.Types.Effects (DoEffect)
import Games.RogueLike.Types.Tick

newtype GameYear = GameYear Integer
  deriving stock (Eq, Show, Ord, Generic)
  deriving anyclass (Hashable, NFData)

data HistoricalEra = HistoricalEra
  { _eraStart :: !GameYear,
    _eraEnd :: !GameYear
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

makeLenses ''HistoricalEra

data HistoricalTime
  = Year GameYear
  | Era HistoricalEra
  | InGameTime GameTime
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)


data InGameEvent' a = InGameEvent {_effectData :: DoEffect a } deriving stock (Eq, Show, Ord, Generic)
  deriving anyclass (Hashable, NFData)
makeLenses ''InGameEvent'
