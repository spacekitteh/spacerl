module Games.RogueLike.Types.Pool where

import Games.ECS
import GHC.Generics
import Control.Lens
import Data.Hashable

    


data Pool a = Pool {_amount :: !a, _maximum :: !a}
          deriving stock (Eq, Ord, Generic)
          deriving anyclass Hashable
                   
instance (Show a, Read a, Ord a) => XMLPickler [Node] (Pool a) where
    xpickle = xpAlt (\(Pool amt maxAmt) -> if amt >= maxAmt then 0 else 1)
              [ xpWrap (\mx -> Pool mx mx) (\(Pool _ mx) -> mx) (xpElemNodes "maximumAmount" (xpContent xpPrim)), --Implicit pickler for when max health
                xpWrap (\(a,b) -> Pool a b) (\(Pool a b) -> (a,b)) (xp2Tuple (xpElemNodes "amount" (xpContent xpPrim)) (xpElemNodes "maximumAmount" (xpContent xpPrim)))]     

makeLenses ''Pool              
