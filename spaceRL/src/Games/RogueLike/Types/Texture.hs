{-# LANGUAGE StrictData #-}
module Games.RogueLike.Types.Texture where
import Data.String
import Data.Interned.Text
import GHC.Generics
import Data.Hashable
import Control.Lens
import Control.DeepSeq

newtype TexturePath = TexturePath InternedText deriving newtype (Eq, Show, IsString, Hashable)
                                               deriving stock Generic
newtype TextureName = TextureName InternedText deriving newtype (Eq, Show, IsString, Hashable)
                                               deriving stock Generic


data Texture = Texture {_texturePath :: TexturePath, _textureName :: TextureName}
             deriving stock (Eq, Show, Generic)
             deriving anyclass Hashable
makeLenses ''Texture
instance NFData Texture where rnf = rwhnf
data TextureMap = TextureMap {_posX :: Texture,
                              _posY :: Texture,
                              _posZ :: Texture,
                              _negX :: Texture,
                              _negY :: Texture,
                              _negZ :: Texture,
                              _textureMapName :: TextureName}
                deriving stock (Eq, Show, Generic)
                deriving anyclass Hashable
makeLenses ''TextureMap
instance NFData TextureMap where rnf = rwhnf
