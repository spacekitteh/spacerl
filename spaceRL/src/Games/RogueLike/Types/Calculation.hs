{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE StrictData #-}
module Games.RogueLike.Types.Calculation where
import Control.DeepSeq
import Control.Lens
import Control.Lens.Cons
import Data.Hashable
import Data.Sequence (Seq, singleton)
import GHC.Generics
import Games.ECS
import Games.RogueLike.Types.DiceRoll
import Games.RogueLike.Types.RNGStore
import Prettyprinter
import Games.RogueLike.Types.Identifiers
-- https://old.reddit.com/r/roguelikedev/comments/7xui6b/faq_fridays_revisited_30_message_logs/dubz02d/

data CalculationStep = DiceRollStep {_diceRollForStep :: DiceAlgebra, _reason :: RNGReason}
                     | ContextualChoice AName Calculation Calculation
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (Hashable, NFData)



instance Pretty CalculationStep where
  {-# INLINEABLE pretty #-}
  pretty (DiceRollStep dr reason) = "Dice roll for" <+> pretty reason <+> "is" <+> pretty dr

instance Show CalculationStep where
  {-# INLINE show #-}
  show = show . pretty

-- TODO FIXME: Make Ord instance based on the expected value
newtype Calculation = Calculation {_calculationSteps :: Seq CalculationStep}
  deriving stock (Eq, Ord, Generic)
  deriving (Hashable) via (HashableSeq CalculationStep)
  deriving newtype (Semigroup, Monoid)
  deriving anyclass NFData

makeLenses ''Calculation
makeLenses ''CalculationStep
instance Pretty Calculation where
  {-# INLINEABLE pretty #-}
  pretty (Calculation steps) = pretty (steps ^.. folded)

instance Show Calculation where
  {-# INLINE show #-}
  show = show . pretty

data Challenge = Challenge {_scoreToBeat :: Double, _diceRoll :: DiceAlgebra}
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

makeLenses ''Challenge

{-# INLINEABLE constantCalculation #-}
constantCalculation :: RNGReason -> Double -> Calculation
constantCalculation reason d = diceRollCalculation reason (Constant d)

{-# INLINEABLE diceRollCalculation #-}
diceRollCalculation :: RNGReason -> DiceAlgebra -> Calculation
diceRollCalculation reason d = Calculation . singleton $ DiceRollStep d reason

-- TODO FIXME make this nicer and actually work
{-# INLINEABLE resolveCalculation #-}
resolveCalculation :: RollsDice m => Calculation -> m Double
resolveCalculation Calculation {_calculationSteps = (DiceRollStep die reason) :< _res} = roll reason die
