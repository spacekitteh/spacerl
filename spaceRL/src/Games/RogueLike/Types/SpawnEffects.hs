{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE StrictData #-}
{-# OPTIONS_GHC -Wunused-imports #-}

module Games.RogueLike.Types.SpawnEffects (module Games.RogueLike.Types.SpawnEffects, module Games.RogueLike.Types.Effects) where
import Control.Lens
import Control.DeepSeq    
import Control.Monad    
import GHC.Generics    
import Data.Hashable
import Games.ECS
import Games.RogueLike.Components.Kinematics (Position)
import Games.RogueLike.Effects
import Games.RogueLike.Types.Damage
import Games.RogueLike.Types.Effects
import Games.RogueLike.Types.Identifiers
import Games.RogueLike.Types.Lore


-- | The text description is supposed to characterise /everything/ about the effect, including effect parameters.

{-
data ACustomEffect = forall eff. (XMLPickler [Node] eff, Show eff, Effect eff EffectSpecifics) => ACustomEffect InternedText eff
instance XMLPickler [Node] ACustomEffect where
  {-# INLINE xpickle #-}
  xpickle = xpThrow "Custom effects aren't supported for pickling yet"

instance Eq ACustomEffect where
  {-# INLINE (==) #-}
  (ACustomEffect a _) == (ACustomEffect b _) = a == b
instance Ord ACustomEffect where
  {-# INLINE compare #-}
  compare (ACustomEffect a _) (ACustomEffect b _) = compare a b
instance Show ACustomEffect where
  {-# INLINE show #-}
  show (ACustomEffect desc _) = show desc
instance Hashable ACustomEffect where
  {-# INLINE hashWithSalt #-}
  hashWithSalt s (ACustomEffect desc eff) = hashWithSalt s desc
-}

type InGameEvent = InGameEvent' EffectSpecifics



data TriggerEffect
  = -- | E.g. stepping onto a trap will activate it.
    ActivateTriggerable
  | -- | Stop it from activating
    DeactivateTriggerable
  | -- | Reset it so it can trigger again
    ResetTriggerable
  deriving stock (Eq, Ord, Show, Generic, Enum)
  deriving anyclass (Hashable, NFData)


data SquadEffect
  = DoJoinSquad
  | DoLeaveSquad
  deriving stock (Eq, Ord, Show, Generic, Enum)
  deriving anyclass (Hashable, NFData)

data AllegienceEffect
  = DoAddAllegiance
  | DoRemoveAllegiance
  deriving stock (Eq, Ord, Show, Generic, Enum)
  deriving anyclass (Hashable, NFData)

data FactionEffect
  = DoChangeFactionRelation Allegiance Allegiance FactionRelation
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

data ObservationEffect
  = -- | The entity is the entity who gains the knowledge
    DoRevealHidden Entity ObservationReason
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

data SocialEffect
  = DoSquadEffect SquadEffect Entity
  | DoGiveOrders Orders
  | DoAllegienceEffect AllegienceEffect Allegiance
  | DoFactionEffect FactionEffect
  | DoObservationEffect ObservationEffect
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)

data EffectSpecifics {-CustomEffect ACustomEffect Bool -- ^ Extensibility! We want scripts to be able to define custom
                                                     -- effects. The Bool is for if it affects entities at
                                                     -- targeted positions.
                     \| -}
  = DoStatusEffect StatusEffect EffectDuration
  | DoTriggerEffect TriggerEffect TriggerName
  | DoUXEffect UXEffect
  | DoSocialEffect SocialEffect
  | DoDamage {_damage :: Damage, _instrument :: Entity}
  | HealDamage {_damage :: Damage, _instrument :: Entity}
  | -- | Cause is the weapon etc which caused the blood to be shed. Target
    -- entity is the critter whose blood is spilled. The
    -- type of blood to spawn is determined by examining
    -- the passed entity. Finally, the target position is
    -- where it's spawned.
    Bloodstain {_bleeder :: Entity}
  | --  | GiveOrders Orders

    -- | An entity was killed by a prior event.
    EntityDeath (Maybe InGameEvent)
  | -- | Resurrect, with the given health.
    Resurrect Damage
  | SetFlag Flag
  | RemoveFlag Flag
  | -- | A sound is emitted in the gameworld
    PlaySound
  | --  | ChangeFactionRelation Allegiance Allegiance FactionRelation
    Teleport Position
  | RemoveBodyPart Entity
  | AddBodyPart Entity
  | UseItem Entity
  | -- | Intensity, duration
    StartFire (Maybe Word) (Maybe EffectDuration)
  | -- | Intensity to remove
    ExtinguishFire (Maybe Word)
  | MagicMapping
  | -- | Soak with a specific liquid or gas or whatever.
    Soak Entity
  | GrantXP XP
  | -- | Craft or convert the selected entities into a new instance
    -- of the given prototype, at the possibly-targeted-position.
    -- Could be implemented as a delete and then spawn.
    ConvertIntoNew Entity
  | -- | Spawn the prototype at the targeted position
    SpawnPrototype PrototypeID
  | -- | Remove an entity from the.game world
    RemoveEntity Entity
  | -- | Log an "in-game" event, which may or may not be displayed
    -- in the message log. This is for recording things like
    -- near-misses, deaths, levelling up, etc; if a critter has
    -- object permanence, then it remembers what has happened to
    -- it.
    LogEvent (InGameEvent)
  deriving stock (Eq, Ord, Generic, Show)
  deriving anyclass (Hashable, NFData)

pattern GiveOrders :: Orders -> EffectSpecifics
pattern GiveOrders orders = DoSocialEffect (DoGiveOrders orders)

pattern ChangeFactionRelation :: Allegiance -> Allegiance -> FactionRelation -> EffectSpecifics
pattern ChangeFactionRelation a b rel = DoSocialEffect (DoFactionEffect (DoChangeFactionRelation a b rel))

pattern JoinSquad :: Entity -> EffectSpecifics
pattern JoinSquad squad = DoSocialEffect (DoSquadEffect DoJoinSquad squad)

pattern LeaveSquad :: Entity -> EffectSpecifics
pattern LeaveSquad squad = DoSocialEffect (DoSquadEffect DoLeaveSquad squad)

pattern AddAllegiance :: Allegiance -> EffectSpecifics
pattern AddAllegiance allegiance = DoSocialEffect (DoAllegienceEffect DoAddAllegiance allegiance)

pattern RemoveAllegiance :: Allegiance -> EffectSpecifics
pattern RemoveAllegiance allegiance = DoSocialEffect (DoAllegienceEffect DoRemoveAllegiance allegiance)

-- | The entity is the entity who gains the knowledge
pattern RevealHidden :: Entity -> ObservationReason -> EffectSpecifics
pattern RevealHidden wisegui reason = DoSocialEffect (DoObservationEffect (DoRevealHidden wisegui reason))

-- | Possibly launch a tutorial
pattern TutorialOpportunity :: TutorialName -> EffectSpecifics
pattern TutorialOpportunity name = DoUXEffect (OfferTutorialOpportunity name)

-- | Cue a sound to be played to the user, such as a menu button click or an explosion
pattern PlayAudio :: EffectSpecifics
pattern PlayAudio = DoUXEffect PlayAudioEffect

-- | Can hurt friends and self and has trouble moving
pattern Confuse :: EffectDuration -> EffectSpecifics
pattern Confuse duration = DoStatusEffect ConfuseTarget duration

-- | Can't see
pattern Blind :: EffectDuration -> EffectSpecifics
pattern Blind duration = DoStatusEffect BlindTarget duration

-- | Can't act
pattern Stun :: EffectDuration -> EffectSpecifics
pattern Stun duration = DoStatusEffect StunTarget duration

-- | Can't hear
pattern Deafen :: EffectDuration -> EffectSpecifics
pattern Deafen duration = DoStatusEffect DeafenTarget duration

-- | Target bleeds with a given (cosmetic) intensity.
pattern CauseBleeding ::
  -- | Bleeding intensity
  Double ->
  EffectDuration ->
  EffectSpecifics
pattern CauseBleeding intensity duration = DoStatusEffect (CauseTargetToBleed intensity) duration

-- | E.g. stepping onto a trap will activate it.
pattern ActivateTrigger :: TriggerName -> EffectSpecifics
pattern ActivateTrigger name = DoTriggerEffect ActivateTriggerable name

-- | E.g. defuse a land mine
pattern StopTrigger :: TriggerName -> EffectSpecifics
pattern StopTrigger name = DoTriggerEffect DeactivateTriggerable name

-- | E.g. re-arm a trap
pattern ResetTrigger :: TriggerName -> EffectSpecifics
pattern ResetTrigger name = DoTriggerEffect ResetTriggerable name

makeLenses ''EffectSpecifics

{-# INLINEABLE effectIsCosmetic #-}
effectIsCosmetic :: EffectSpecifics -> Bool
effectIsCosmetic (DoUXEffect _) = True
effectIsCosmetic (DoStatusEffect _ _) = False
effectIsCosmetic (DoTriggerEffect _ _) = False
effectIsCosmetic MagicMapping = True
effectIsCosmetic (LogEvent _) = True -- Is this right?
effectIsCosmetic (DoDamage _ _) = False
effectIsCosmetic (HealDamage _ _) = False
effectIsCosmetic (Bloodstain _) = True
effectIsCosmetic (DoSocialEffect _) = False
effectIsCosmetic (EntityDeath _) = False -- TODO FIXME This ought to check if it isn't a cosmetic item (e.g. a cosmetic particle)?
effectIsCosmetic (SpawnPrototype _) = False -- TODO FIXME Likewise
effectIsCosmetic (RemoveEntity _) = False -- TODO FIXME Likewise
effectIsCosmetic (UseItem _) = False -- TODO FIXME Likewise
effectIsCosmetic (Teleport _) = False
effectIsCosmetic (StartFire _ _) = False
effectIsCosmetic (ExtinguishFire _) = False
effectIsCosmetic (PlaySound) = False -- Remember this is in-world sound
effectIsCosmetic (GrantXP _) = False
effectIsCosmetic (Soak _) = False
effectIsCosmetic other = error $ "effectIsCosmetic is not implemented for " ++ show other

type EffectSystem = MessageQueue (DoEffect EffectSpecifics)

type SpawnsEffects m = MonadAtomicMessageQueue (DoEffect EffectSpecifics) m

{-# INLINE newEffectSystem #-}
newEffectSystem :: EffectSystem
newEffectSystem = newMessageQueue

{-# INLINE queueDoEffect #-}

-- | Queue an effect.
queueDoEffect :: (SpawnsEffects m) => (DoEffect EffectSpecifics) -> m ()
queueDoEffect = queueMessage

{-# INLINE queueDoEffects #-}

-- | Queue an effect.
queueDoEffects :: (Foldable f, SpawnsEffects m) => f (DoEffect EffectSpecifics) -> m ()
queueDoEffects effs = forM_ effs queueMessage

{-# INLINE queueImmediateEffect #-}
queueImmediateEffect :: (SpawnsEffects m) => (DoEffect EffectSpecifics) -> m ()
queueImmediateEffect = queueImmediateMessage

{-# INLINEABLE readEffect #-}

-- | Pops the effect queue.
readEffect :: (SpawnsEffects m) => m (Maybe (DoEffect EffectSpecifics))
readEffect = readMessage (\(DoEffect {_effectDetails}) -> effectIsCosmetic _effectDetails)

{-# INLINE doEffects #-}

-- | Queue a series of effects.
doEffects :: (Foldable f, SpawnsEffects m) => Entity -> f EffectTarget -> f EffectSpecifics -> m ()
doEffects cause targs effs = forM_ effs (\eff -> doEffect cause targs eff)

{-# INLINE doEffect #-}

-- | Queue an effect.
doEffect :: (Foldable f, SpawnsEffects m) => Entity -> f EffectTarget -> EffectSpecifics -> m ()
doEffect cause targs eff = queueDoEffect (newEffect cause targs eff)

{-# INLINE doImmediateEffect #-}

-- | Queue an effect to the front of the queue.
doImmediateEffect :: (Foldable f, SpawnsEffects m) => Entity -> f EffectTarget -> EffectSpecifics -> m ()
doImmediateEffect cause targs eff = queueImmediateEffect (newEffect cause targs eff)

{-# INLINE logEvent #-}
logEvent :: (Foldable f, SpawnsEffects m) => Entity -> f EffectTarget -> InGameEvent -> m ()
logEvent cause targs msg = doImmediateEffect cause targs (LogEvent msg)
