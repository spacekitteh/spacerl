module Games.RogueLike.Types.Valence where
import GHC.Generics
import Control.DeepSeq

    
data Valence = Bad | Neutral | Good deriving stock (Eq, Ord, Show, Enum, Bounded, Generic)
             deriving anyclass NFData
    
data ValencePerspective = HappensToMe | HappensToAlly | HappensToNeutral | HappensToEnemy
