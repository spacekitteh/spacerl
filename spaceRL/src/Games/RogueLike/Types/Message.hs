module Games.RogueLike.Types.Message (module Prettyprinter, module Games.RogueLike.Types.Message) where
import Control.Lens
import GHC.Generics    
import Games.ECS
import Games.RogueLike.Types.Calculation
import Games.RogueLike.Types.Colour
import Games.RogueLike.Types.Valence    
import Prettyprinter (Doc, PageWidth, annotate, emptyDoc, list, pretty, viaShow, (<+>))
import Control.DeepSeq

data MessageContentAnnotation
  = EntityKnownName Entity
  | Effect Valence
  | EntitySaid Entity
  | BareEntityID
  | WithColour {_fg :: Colour, _bg :: Colour}
  | ByCalculation {_calculation :: Calculation}
  deriving stock (Eq, Show, Generic)
  deriving anyclass NFData

makeLenses ''MessageContentAnnotation
           
annotateWithCalculation :: Calculation -> Message -> Message
annotateWithCalculation calc = annotate (ContentAnnotation (ByCalculation calc))

annotateWithKnownName :: Entity -> Message -> Message
annotateWithKnownName ty = annotate (ContentAnnotation (EntityKnownName ty))
                           
data MessageVerbosity
  = Debug String
  | Warning String
  | Standard String
  | Error String 
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass NFData
data MessageAnnotation
  = LogVerbosity MessageVerbosity
  | ContentAnnotation MessageContentAnnotation
  deriving stock (Eq, Show, Generic)
  deriving anyclass NFData
type Message = Doc MessageAnnotation
deriving anyclass instance NFData Message
instance XMLPickler [Node] (PageWidth -> Message) where
  xpickle = xpThrow "PageWidth not supported in messages"

instance XMLPickler [Node] (Int -> Message) where
  xpickle = xpThrow "nesting not supported in messages"

{-# INLINE renderBareEntity #-}
renderBareEntity :: Entity -> Message
renderBareEntity = annotate (ContentAnnotation BareEntityID) . viaShow
