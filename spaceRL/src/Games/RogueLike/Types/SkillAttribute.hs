module Games.RogueLike.Types.SkillAttribute where

class SkillAttribute a where
  -- | Convert to a challenge modifier
  modifier :: a -> Int
  -- | A default attribute value such that its modifier is 0.
  standardValue :: a

