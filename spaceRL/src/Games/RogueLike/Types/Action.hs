module Games.RogueLike.Types.Action where
import Games.ECS
import Control.Lens
import GHC.Generics
import Games.RogueLike.Types.Identifiers    
data Action = Action ActionName deriving stock (Eq, Show, Generic)
makeLenses ''Action
instance Component Action where
    type CanonicalName Action = "action"
