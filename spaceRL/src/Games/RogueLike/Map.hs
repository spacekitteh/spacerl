{-# LANGUAGE ApplicativeDo #-}
-- |
-- Module      :  Games.RogueLike.Map
-- Description : Basic map interface
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- The basic API to interact with maps.
module Games.RogueLike.Map where
      
