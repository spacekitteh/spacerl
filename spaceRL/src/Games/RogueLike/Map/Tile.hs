{-# OPTIONS_GHC -Werror=incomplete-patterns #-}
module Games.RogueLike.Map.Tile where
import Control.Applicative    
import Control.Lens
import Control.Monad    
import Data.Kind
import Data.Map
import Data.Random.Distribution.Categorical
import GHC.Generics
import Games.ECS.Entity
import Games.ECS.Prototype
import Games.ECS.World
import Games.RogueLike.Components.Archetypes.Physical
import Games.RogueLike.Types.Coordinates (WorldCoord)
import qualified Data.Map as Map

-- | A simple tile categorisation for the rest of the engine to work with, without having to worry about the
-- differences between map styles.
data SimpleTileCategorisation
  =
  -- | Acts as a barrier.
    Wall
  -- | Something that can be stood on.
  | Floor
  -- | A location where an entity spawns.
  | EntitySpawnLocation
  -- | If you can access different Z-levels from this tile.
  | Stairs
  -- | A door which can open and close.
  | Door
    -- | Is the door locked?
    !(Maybe Bool)
  -- | Is it empty space?
  | TheVoid
  -- | Could be water, etc.
  | Liquid
  -- | A damaging object to touch.
  | Trap
  -- | Something which can trigger an "event".
  | EventTrigger
  -- | Something else.
  | Other
  deriving stock (Eq, Ord, Show, Read, Generic)



-- | Broad properties of a tile categorisation.
class  TileCategorisation t where
  -- | Reduce a complex tile categorisation to a simple tile categorisation. This allows the rest of the
  -- engine to reason about broad properties of the tile, without having to know about specific map styles and
  -- plans. Must preserve `emptyTile`; that is, `toSimpleTileCategorisation emptyTile == pure TheVoid`
  toSimpleTileCategorisation :: MonadPlus f => t -> f SimpleTileCategorisation

  -- | Tile categorisation value which represents "nothing".
  emptyTile :: t
                                
  -- | Whether the tile category definitely blocks movement.
  movementBlocker :: MonadPlus f => t -> f Bool
  movementBlocker tile = do
      categorisation <- toSimpleTileCategorisation tile
      movementBlocker categorisation
  -- | Whether the tile category is definitely a critter spawn location.
  critterSpawnLocation :: MonadPlus f => t -> f Bool
  critterSpawnLocation tile = do
    categorisation <- toSimpleTileCategorisation tile
    critterSpawnLocation categorisation

                         
instance TileCategorisation SimpleTileCategorisation where
  toSimpleTileCategorisation = pure
  emptyTile = TheVoid
  movementBlocker Wall = pure True
  movementBlocker Floor  = pure False
  movementBlocker Stairs = pure False
  movementBlocker (Door Nothing) = Control.Applicative.empty -- May be locked
  movementBlocker (Door (Just True)) = pure True
  movementBlocker (Door (Just False)) = pure False
  movementBlocker TheVoid = pure False
  movementBlocker Other = Control.Applicative.empty
  movementBlocker EntitySpawnLocation = Control.Applicative.empty
  movementBlocker Liquid = Control.Applicative.empty
  movementBlocker Trap = Control.Applicative.empty -- Not necessarily a movement blocker.
  movementBlocker EventTrigger = Control.Applicative.empty
                                 
  critterSpawnLocation EntitySpawnLocation = pure True
  critterSpawnLocation TheVoid = pure False
  critterSpawnLocation _ = Control.Applicative.empty
    


cp437SimpleTileCategorisation :: Map Char [SimpleTileCategorisation]
cp437SimpleTileCategorisation = Map.fromList [
                                 ( '#' , pure Wall),
                                 ( '.' , pure Floor),
                                 ( '∙' , pure Floor),
                                 ( '·' , pure Floor),
                                 ( '║' , pure Wall),
                                 ( '╗' , pure Wall),
                                 ( '╝' , pure Wall),
                                 ( '╚' , pure Wall),
                                 ( '╔' , pure Wall),
                                 ( '╩' , pure Wall),
                                 ( '╦' , pure Wall),
                                 ( '╠' , pure Wall),
                                 ( '═' , pure Wall),
                                 ( '╬' , pure Wall),
                                 ( ' ' , pure TheVoid),
                                 ( '\x0000' , pure TheVoid),
                                 ( '>' , pure Stairs),
                                 ( '<' , pure Stairs),
                                 ( '\'' , [EntitySpawnLocation, Door (Just False)]), -- Open door
                                 ( '/' ,  [ EntitySpawnLocation, Door (Just False)]), -- Open door
                                 ( '+' , [EntitySpawnLocation, Door Nothing]), -- Closed door. Might be locked.
                                 ( '~' , pure Liquid), -- Water, usually
                                 ( '≈' , pure Liquid), -- Deep water?
                                 ( '\x00A0' , pure TheVoid), -- Non-breaking space character
                                 ( '@' , pure EntitySpawnLocation),
                                 ( '☺' , pure EntitySpawnLocation),
                                 ( '☻' , pure EntitySpawnLocation),
                                 ( '^' , [Trap, EntitySpawnLocation, EventTrigger]),
                                 ( '▲' , pure Stairs),
                                 ( '▼' , pure Stairs)
                                 ]

-- | A CP437 glyph usually has a canonical broad meaning. Some glyphs imply several categorisations at once.
-- 
-- If there isn't an obvious choice, 'empty' is returned.This indicates that game-specific logic should
-- dictate. In particular, typical glyphs for monsters are not categorised.
cp437ToSimpleTileCategorisation :: Alternative f =>  Char -> f SimpleTileCategorisation
cp437ToSimpleTileCategorisation c = case Map.lookup c cp437SimpleTileCategorisation of
                                      Nothing -> Control.Applicative.empty
                                      Just res -> asum . fmap pure $ res
                                                  
