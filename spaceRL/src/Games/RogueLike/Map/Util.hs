module Games.RogueLike.Map.Util where
import Control.Lens
import Control.Monad    
import Games.ECS
import Games.RogueLike.Types.SpawnEffects    
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Types.PhysicalTypes    
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Types.Glyph
import Control.Monad.IO.Class
import Linear.V3 (V3(..))
import Data.Colour (opaque)
import qualified Data.Colour.Names as DCN
import Games.RogueLike.Types.Colour
import Data.Text (singleton)
import Data.Interned.Text
import Games.RogueLike.Types.Voxel

{-# NOINLINE theDot #-}
theDot :: InternedText
theDot = (singleton '.') ^. from interned
{-# NOINLINE theHash #-}
theHash :: InternedText
theHash = (singleton '#') ^. from interned

spawnFloors :: forall worldType m. ( UsingRenderable worldType Individual, UsingPosition worldType Individual, UsingCanWalkOn worldType Individual, UsingSupportsWeight worldType Individual, MonadIO m) => Position -> Position -> worldType Storing -> m (worldType Storing)
spawnFloors ll tr existingWorld = let
  col = Colour (opaque DCN.gray)
  theRenderable = Renderable (Glyph theDot col transparent {-(Colour (opaque DCN.black))-}) (-10) (PlainVoxel col) in do
  let positions = [Position (V3 fx fy fz) | fx <- [(ll^.x) .. (tr^.x)], fy <- [(ll^.y) .. (tr^. y)], fz <- [(ll^.z) .. (tr^.z)]]

  ents <- forM positions (\pos -> do
                             ent <- createNewEntity
                             pure (ent & addPosition .~ pos & addCanWalkOn .~ CanWalkOn & addSupportsWeight .~ SupportsInfiniteWeight & addRenderable .~ theRenderable))
          
  pure (foldr storeEntity  existingWorld ents)

spawnWalls :: forall worldType m. ( UsingRenderable worldType Individual, UsingPosition worldType Individual, UsingBlocksVision worldType Individual, UsingBlocksMovement worldType Individual, MonadIO m) => Position -> Position -> PrototypeID -> worldType Storing -> m (worldType Storing)
spawnWalls ll tr wallType existingWorld = let
  col = Colour (opaque DCN.cyan)
  theRenderable = Renderable (Glyph theHash col transparent) (-5)  (PlainVoxel col) in do
  let positions = [Position (V3 fx fy fz) | fx <- [(ll^.x) .. (tr^.x)], fy <- [(ll^.y) .. (tr^. y)], fz <- [(ll^.z) .. (tr^.z)]]
  --doImmediateEffect (EntRef 0) positions (SpawnPrototype wallType)
  ents <- forM positions (\pos -> do
                             ent <- createNewEntity
                             pure (ent & addPosition .~ pos
                                         & addRenderable .~ theRenderable
                                         & addBlocksVision .~BlocksVision
                                         & addBlocksMovement .~ BlocksMovement ObstructsCompletely))

  pure (foldr storeEntity  existingWorld ents)

spawnBoundaryWalls :: forall worldType m. ( UsingRenderable worldType Individual, UsingPosition worldType Individual, UsingBlocksVision worldType Individual, UsingBlocksMovement worldType Individual, MonadIO m) => Position -> Position -> worldType Storing -> m (worldType Storing)
spawnBoundaryWalls ll tr existingWorld = let
  col = Colour (opaque DCN.cyan)
  inPos coord f = elem f [ll^.coord, tr^.coord]
  inX = inPos x
  inY = inPos y
  inZ = inPos z
  inXY fx fy = inX fx && inY fy
  inXZ fx fz = inX fx && inZ fz
  inYZ fy fz = inY fy && inZ fz
  theRenderable = Renderable (Glyph theHash col transparent) (-5)  (PlainVoxel col) in do
  let positions = [Position (V3 fx fy fz) | fx <- [(ll^.x) .. (tr^.x)], fy <- [(ll^.y) .. (tr^. y)], fz <- [(ll^.z) .. (tr^.z)], ((inXY fx fy ) || (inXZ fx fz ) || (inYZ fy fz ))   ]

  ents <- forM positions (\pos -> do
                             ent <- createNewEntity
                             pure (ent & addPosition .~ pos
                                         & addRenderable .~ theRenderable
                                         & addBlocksVision .~BlocksVision
                                         & addBlocksMovement .~ BlocksMovement ObstructsCompletely))
          
  pure (foldr storeEntity  existingWorld ents)
