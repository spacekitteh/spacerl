{-# LANGUAGE RoleAnnotations #-}

module Games.RogueLike.Map.WorldRegion where
import Control.Lens
import GHC.Generics    
import Data.Interned.Text
import Data.String
import Games.ECS
import Games.RogueLike.Types.RNGStore
import Numeric.Natural
    
newtype RegionIdentifier = RegionIdentifier Natural deriving stock (Eq)

newtype RegionName = RegionName InternedText
  deriving stock (Eq, Ord)
  deriving newtype (Show, IsString)



data WorldRegionDefaults where
    WithWorldRegionDefaults :: {_floorPrototype :: PrototypeID, _wallPrototype :: PrototypeID} -> WorldRegionDefaults
 deriving stock (Eq, Ord, Generic)
 

makeLenses ''WorldRegionDefaults
           
type role WorldRegion nominal

data WorldRegion name where
  WorldRegion :: {_regionID :: RegionIdentifier, _regionName :: RegionName, _regionDefaults :: WorldRegionDefaults} -> WorldRegion name


