{-# LANGUAGE PolyKinds #-}
-- |
-- Module      :  Games.RogueLike.Map.Generator
-- Description : Abstract map generators
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- Different map styles may be desired, and so we define our interface for map generators.
module Games.RogueLike.Map.Generator where
import Games.RogueLike.Types.Coordinates (WorldCoord)
import Games.RogueLike.Types.RNGStore    
import Games.RogueLike.Map.Tile
import Games.RogueLike.Map.Spawns    
import Control.Lens
import Data.Kind ( Type )
import Data.Distributive
import Data.Functor.Rep
import Data.Distributive    
import Control.Comonad.Representable.Store
import GHC.Generics (Generic)
import Linear
import Data.Patricia.Word.Lazy as P

import Data.Functor.Foldable


-- | A procedural generation grammar class for maps, for when an abstract map plan is first generated, then a concrete map generated from it.
class (Ord (TileCategory m), TileCategorisation (TileCategory m)) => MapPlan m where
  -- | The identifier type for map tile categories
  type TileCategory m ::  Type
  
  -- | Given a plan, look up the tile category at an arbitrary position.
  mapTileType :: m -> WorldCoord -> TileCategory m

  -- | The spawn table associated with the plan.
  spawnClassMap :: Lens' m (SpawnClassMap (TileCategory m))

  -- | Places where an entity may enter the map.
  mapEntrances :: m -> [WorldCoord]

  -- | Places where an entity may leave the map.
  mapExits :: m -> [WorldCoord]

  -- | Some suitable pathfinding acceleration waypoints, in addition to map entrances and exits.
  extraMapWaypoints :: m -> [WorldCoord]

  -- | Vertices of the bounding box of the map
  mapBoundingBox :: m -> (WorldCoord, WorldCoord)

                  

data MapGen' tileCat a = MapGen {_spawnMap :: (SpawnClassMap (TileCategory tileCat)), _mapData :: Patricia a, _dimensions :: (WorldCoord, WorldCoord)}
                       deriving stock (Functor, Foldable, Traversable)
                                
deriving instance (Eq (TileCategory tileCat), Eq a) => Eq (MapGen' tileCat a)

         
makeLenses ''MapGen'
           
type MapGen tileCat = MapGen' tileCat tileCat
    
-- instance Distributive (MapGen' tileCat) where
--     distribute = distributeRep
--     collect = collectRep




{-
  histo+futumorphisms?    i.e. chronomorphisms
   have history, and can change multiple future layers at once

1. start with seed.
2. unfold seed, using futumorphism to go multiple layers at a time in case of e.g. prefabs?
3. fold the structure, using histomorphism for memoisation/tabulation of results, constructing a list of entities to add to the world

question: can the map indexing system octree be reformulated to perform fusion?

-}
{-
Graph search, neighbours and representable. The neighbour function seems like a representation for a zipper for a graph?
-}

              
{-

i need a spine-lazy patricia trie (e.g. from radix-trees)
i need to adapt a space-filling curve to its index
then i need to implement the Functor, Distributive, Representable, Applicative, Monad, Comonad typeclasses for it
after tabulation, should prune the trie to the relevant bounding box
to get the bounding box, i need to compute all positions on the bounding box, and convert them to positions on the space filling curve
then, using e.g. a zebra tree, find the relevant covering intervals
with an iteration depth parameter for accuracy
-}
              
-- instance Representable (MapGen' tileCat) where
--     type Rep (MapGen' tileCat) = WorldCoord

--     index = 
    
