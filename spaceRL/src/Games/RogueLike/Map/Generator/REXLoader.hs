module Games.RogueLike.Map.Generator.REXLoader where
import Control.Lens
import Games.RogueLike.Assets.REXPaint
import Games.RogueLike.Map.Generator
import Games.RogueLike.Map.Tile
import Games.RogueLike.Types.Identifiers    
import System.OsPath hiding (pack)
import GHC.Generics
import Data.Map (Map)
import qualified Data.Map as Map    
import Games.RogueLike.Types.Glyph
import Control.Monad
import Control.Monad.Catch
import Control.Monad.IO.Class    
    
import qualified Data.Map as Map
import Data.Text (pack)
import Data.Interned
import Data.Interned.Text
import Data.ByteString.Lazy as BS hiding (pack)

data REXPaintMapKey = REXPaintMapKey (Map InternedText [SimpleTileCategorisation]) deriving stock (Eq, Ord, Show, Generic)

defaultREXPaintMapKey :: REXPaintMapKey
defaultREXPaintMapKey = REXPaintMapKey (Map.mapKeys (\glyph -> intern (pack [glyph])) cp437SimpleTileCategorisation)
                    
data REXPaintMapTile = REXPaintMapTile Glyph REXPaintMapKey deriving stock (Eq, Ord, Show, Generic)
                    
instance TileCategorisation REXPaintMapTile where
    toSimpleTileCategorisation (REXPaintMapTile g (REXPaintMapKey k)) = do
      let cats = Map.lookup (g^.internedSymbol) k
      case cats of
        Nothing -> mzero
        Just res -> msum (fmap pure res)
      


    
    
data REXPaintMap = REXPaintMap {_mapFile :: OsPath, _loadedMap :: RexPaintFile, _mapKey :: REXPaintMapKey} deriving stock (Eq, Show, Generic)
makeLenses ''REXPaintMap                 
loadREXPaintMap :: (MonadThrow m, MonadIO m) => REXPaintMapKey -> OsPath -> m REXPaintMap
loadREXPaintMap mapKey filepath = do
  path <- decodeUtf filepath
  contents <- readRexPaintFileFromByteString <$> liftIO (BS.readFile path)
  pure $ REXPaintMap filepath contents mapKey


instance MapPlan REXPaintMap where
    type TileCategory REXPaintMap = REXPaintMapTile
