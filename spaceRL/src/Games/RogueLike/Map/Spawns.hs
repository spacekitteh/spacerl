module Games.RogueLike.Map.Spawns where
import Control.Lens
import Data.Map as Map
    
import Data.Random.Distribution.Categorical
import GHC.Generics
import Games.ECS.Entity
import Games.ECS.Prototype
import Games.ECS.World
import GHC.Exts (IsList(..))    
import Games.ECS.Serialisation

-- | A spawn frequency table, indexed by tile categorisation.
newtype SpawnClassMap tileToken = SpawnClassMap {_theMap :: (Map tileToken (Categorical Double PrototypeID))}
  deriving newtype (Eq, Show, IsList)
  deriving stock (Generic)

instance {-# OVERLAPPABLE #-} (Ord tileToken, XMLPickleAsAttribute tileToken) => XMLPickler [Node] (SpawnClassMap tileToken) where
    {-# INLINE xpickle #-}
    xpickle = ("SpawnClassMap","") <?+> (xpWrap (SpawnClassMap . Map.fromList) (Map.toList . _theMap) (mapPickler "tile" "token" xpickle)          )
