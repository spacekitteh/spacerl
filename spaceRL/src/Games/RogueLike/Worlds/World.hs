module Games.RogueLike.Worlds.World where

import Control.Lens

import GHC.Generics
import Games.RogueLike.Worlds.Parameters
import Games.RogueLike.Worlds.Region    
import Data.Functor.Coyoneda
    
data World' a = World {
      _basicWorldParameters :: WorldParameters,
      _regions :: [WorldRegion]
      } deriving stock (Eq, Show, Generic)

makeLenses ''World'
           
