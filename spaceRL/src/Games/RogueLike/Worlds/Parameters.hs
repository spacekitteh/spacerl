module Games.RogueLike.Worlds.Parameters where

import Control.Lens
import GHC.Generics
    
data WorldParameters = WorldParameters  deriving stock (Eq, Show, Generic)
makeLenses ''WorldParameters
