module Games.RogueLike.Worlds.Region where

import Control.Lens
import GHC.Generics
    
data WorldRegion = WorldRegion deriving stock (Eq, Show, Generic)

makeLenses ''WorldRegion                 
