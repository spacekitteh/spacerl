module Games.RogueLike.UI.InputEvent where
import GHC.Generics
import Data.Hashable
import Games.RogueLike.Components.Kinematics
import Data.Word
import Data.Kind
    
class InputContext c where
  type AcceptableInput c :: Type
  
-- data InputContext n = InputContext n
--   deriving stock (Eq, Ord, Generic, Show, Read, Functor, Foldable, Traversable)
--   deriving anyclass Hashable
    
data RelativeMovementCommand
  = MoveNorth 
  | MoveSouth
  | MoveWest
  | MoveEast
  | MoveNE
  | MoveNW
  | MoveSE
  | MoveSW
  | Ascend
  | Descend
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable
                             
data MovementCommand
  = MoveRelative RelativeMovementCommand (Maybe Word8)
  | GoTo Position
  deriving stock (Eq, Ord, Generic, Show, Read)
  deriving anyclass Hashable

-- | Commands which operate on locks.
data LockCommand
    = LockTheLock
    | UnlockTheLock
    | TestIfTheLockIsLocked
    | PickTheLock
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable

data DoorInteractionCommand
  = OpenDoor
  | CloseDoor
  | DoorLockInteraction LockCommand
  | SmashDownDoor
  deriving stock (Eq, Ord,  Generic, Show, Read)
  deriving anyclass Hashable

-- | Screen-relative directions, for UI purposes.
data RelativeDirection = Up | Down | Left | Right
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable
           
data MenuNavigationCommand
  = Scroll RelativeDirection
  | MoveSelection RelativeDirection
  | Select
  | Cancel
  deriving stock (Eq, Ord,  Generic, Show, Read)
  deriving anyclass Hashable
data InventoryCommand
  = OpenInventory
  | CloseInventory
  | AddToInventory
  | DiscardFromInventory
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable
data SocialInteractionCommand
  = StartTalking
  | StopTalking
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable
data EquippableCommand
  = Equip
  | UnEquip
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable

data SenseCommand
    = LookAt
    | ExamineClosely
    | Listen
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable
data GameStatusCommand
  = NewGame
  | ResumeSave
  | Save
  deriving stock (Eq, Ord, Bounded, Enum, Generic, Show, Read)
  deriving anyclass Hashable
             


           
