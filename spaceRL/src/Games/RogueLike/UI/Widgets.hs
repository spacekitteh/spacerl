module Games.RogueLike.UI.Widgets where
import Games.RogueLike.Types.Message
import Games.RogueLike.Types.Coordinates
import Games.RogueLike.Components.Renderable
import Data.Text
import GHC.Generics
import Control.Lens
import Data.Hashable
import Data.Sequence as Seq
import Data.HashMap.Strict as HMS
import Control.Monad.IO.Class
import Linear
  ( V2 (..),
    V3 (..),
  )
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory
import Games.ECS.World
import Games.ECS.Entity
import Games.RogueLike.Types.Glyph
  ( Glyph,
    backgroundColour,
    foregroundColour,
    symbol,
    toRGB,
  )    
type WidgetName = Text

data Widget = BorderedPanel {_title :: Message, _panelContents :: WidgetLayout}
              | ProgressBar {_minVal :: Double, _maxVal :: Double, _curVal :: Double}
              | TextLog {_log :: Seq Message}
              | MapView {_mapContents :: HMS.HashMap WorldCoord Renderable, _mapSettings :: MapSettings}
              deriving (Show, Generic)

data WidgetLayout = HorizontalWidgets {_childWidgets :: Seq WidgetLayout}
                  | VerticalWidgets {_childWidgets :: Seq WidgetLayout}
                  | SingleWidget Widget WidgetName -- Centered
                  | NoWidget
                  deriving (Show, Generic)

data MapRenderSettings  = MapRenderSettings {_mapWidth :: WorldCoordinateScalar, _mapHeight :: WorldCoordinateScalar} deriving stock (Eq, Ord, Show, Generic)
data MapSettings = MapSettings {_mapRenderSettings :: MapRenderSettings} deriving stock (Eq, Ord, Show, Generic)
                           
makeLenses ''Widget
makeLenses ''WidgetLayout



makeLenses ''MapRenderSettings
makeLenses ''MapSettings

                  
{-# INLINEABLE produceMap #-} -- We want to be able to specialise based on the world type.
-- | Render a map centred on a given entity.
produceMap :: (
               World worldType,
               UsingPosition worldType Individual,
               UsingHasObjectPermanence worldType Individual
              )
             => MapSettings -> Entity -> worldType Storing ->
             -- | A function which takes the reference entity, a world position, and a glyph, and returns a map pixel.
             (worldType Individual -> Position -> Glyph -> a)
           -> [[a]]
produceMap mapSettings referenceEntity world toDrawCoord = rows where
    width = mapSettings^.mapRenderSettings.mapWidth
    height = mapSettings^.mapRenderSettings.mapHeight
    rows = [cellsInRow r | !r <- [(minY + width - 1), (minY + height - 2) .. minY]]    
    (Just playerEntity) = world ^? entity referenceEntity
    -- TODO: Clamp if at edges of map)
    minX = (playerEntity ^?! position . x) - (width `div` 2)
    minY = (playerEntity ^?! position . y) - (height `div` 2)
    zlevel = playerEntity ^?! position . z
    cellsInRow y' = [drawCoord (V2 (x') (y')) | !x' <- [minX .. (minX + width - 1)]]
    drawCoord (V2 x' y') =
      let absPos = Position (V3 x' y' zlevel)
          !knownRenderablesAtPos = Seq.fromList $ playerEntity ^.. observationsAtPosition absPos . observedRenderable . _Just
          !renderables = unstableSortOn (view zHeight) knownRenderablesAtPos
      -- | TODO: Add a "IsStatic" component. Only "remember" non-visible entities if they have IsStatic?
          !renderData = foldMap (view glyph) renderables
          !res = (toDrawCoord playerEntity absPos renderData)
      in  res `seq` res
           
