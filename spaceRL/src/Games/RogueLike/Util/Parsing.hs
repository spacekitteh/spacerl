module Games.RogueLike.Util.Parsing where
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import Data.Scientific
import Data.Text (Text)
import Data.Void
import Data.String
import Data.CaseInsensitive
type BasicParser = Parsec Void Text

pLineComment :: MonadParsec e s m => m ()
pLineComment = empty

pBlockComment :: MonadParsec e s m => m ()
pBlockComment = empty

spaceConsumer :: (Token s ~ Char, MonadParsec e s m) => m ()
spaceConsumer = L.space space1 pLineComment pBlockComment

lexeme :: (Token s ~ Char, MonadParsec e s m) => m a -> m a
lexeme = L.lexeme spaceConsumer

pUnsignedIntegral :: (MonadParsec e s m, Token s ~ Char, Num a) => m a
pUnsignedIntegral = lexeme L.decimal

pUnsignedReal :: (RealFloat a, MonadParsec e s m, Token s ~ Char) => m a
pUnsignedReal = lexeme ((toRealFloat <$> L.scientific ) <|> L.decimal <|> L.float )



pReal ::(RealFloat a, MonadParsec e s m, Token s ~ Char) => m a
pReal = L.signed empty pUnsignedReal

pIntegral :: (MonadParsec e s m, Token s ~ Char, Num a) => m a
pIntegral = L.signed empty pUnsignedIntegral

symbol :: (Tokens s ~ a, Token s ~ Char,  MonadParsec e s m) => Tokens s -> m a
symbol = L.symbol spaceConsumer

symbol' :: (FoldCase a, Tokens s ~ a, Token s ~ Char,  MonadParsec e s m) => Tokens s -> m a
symbol' = L.symbol' spaceConsumer

parens :: (IsString (Tokens s), Token s ~ Char, MonadParsec e s m) => m a -> m a
parens = between (symbol "(") (symbol ")")

optParens :: (IsString (Tokens s), Token s ~ Char, MonadParsec e s m) => m a -> m a
optParens p = ( parens p) <|> p
