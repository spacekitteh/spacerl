module Games.RogueLike.Effects.UXEffects where
import Games.RogueLike.Effects.Effect
import Games.RogueLike.Types.Identifiers
import GHC.Generics
import Data.Hashable
import Control.DeepSeq    


-- | Strictly speaking, these are things which affect how the user experiences the game, and can be ignored or disabled by the player based on options
data UXEffect
  = -- | E.g. menu button click sound, or explosion sound
    PlayAudioEffect
  | -- | Possibly launch a tutorial
    OfferTutorialOpportunity TutorialName
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)
    
instance Effect UXEffect where
    generalConsensusOnEffect _ _ = Nothing
    effectIsCosmetic _ = True
