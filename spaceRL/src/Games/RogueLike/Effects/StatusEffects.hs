module Games.RogueLike.Effects.StatusEffects where
import GHC.Generics
import Data.Hashable
import Control.DeepSeq    
import Control.Lens
import Games.RogueLike.Effects.Effect
import Games.RogueLike.Types.Valence
-- | Status effects temporarily alter the abilities of an entity.
data StatusEffect
  = -- | Can hurt friends and self and has trouble moving
    ConfuseTarget
  | -- | Can't see
    BlindTarget
  | -- | Can't act
    StunTarget
  | -- | Can't hear
    DeafenTarget
  | -- | Target bleeds. Don't forget to add bloodstains!
    CauseTargetToBleed
      -- | Intensity
      Double
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (Hashable, NFData)
makePrisms ''StatusEffect           

instance Effect StatusEffect where
    generalConsensusOnEffect _ HappensToEnemy = Just Good
    generalConsensusOnEffect _ HappensToNeutral = Just Neutral
    generalConsensusOnEffect _ _ = Just Bad
    effectIsCosmetic _ = False
