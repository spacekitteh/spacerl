module Games.RogueLike.Effects.Effect where
import Games.RogueLike.Types.Valence
class Effect e where
    generalConsensusOnEffect :: e -> ValencePerspective -> Maybe Valence
    effectIsCosmetic :: e -> Bool                            
