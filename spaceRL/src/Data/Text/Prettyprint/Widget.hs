module Data.Text.Prettyprint.Widget where

import           Data.Text.Prettyprint.Doc


type TUIDoc = Doc Formatting

data Formatting
