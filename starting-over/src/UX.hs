module UX where
import World
import GameState
import GameStep    
import Games.RogueLike.Types.Glyph
  ( Glyph,
    backgroundColour,
    foregroundColour,
    symbol,
    toRGB,
  )
import Linear
  ( V2 (..),
    V3 (..),
  )
import Data.Sequence
import Control.Lens
import Games.ECS    
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.UI.Widgets    
import Data.Int
import Control.DeepSeq


-- | Resource names for UI elements
data ResName = MainMap
             | Inventory
             | DeathDialog
             | TheGameLog
             | SideBar SideBarWidgets deriving (Eq, Show, Ord)


data SideBarWidgets = HealthBar
                    | Experience
                      deriving (Eq, Show, Ord)
               

                               

updateMap :: GameState -> GameState
updateMap = id


mainMapSettings :: MapSettings
mainMapSettings = MapSettings {_mapRenderSettings = MapRenderSettings {_mapWidth = 50, _mapHeight = 50}}


drawMap' :: GameState -> (Thing -> Position -> Glyph -> a) -> [[a]]
drawMap' (GameState pref !g _idxing _ _ _) toDrawCoord = produceMap mainMapSettings pref g toDrawCoord
