{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -Wunused-imports #-}

module GameState (initialGameState, simpleJack, goblin{-, goblinPrototype-}) where
import GameStep
import Numeric    
import Games.RogueLike.Types.Identifiers    
import Control.Concurrent.STM
import GHC.Exts
-- import Data.Sequence
import Games.RogueLike.Types.PhysicalTypes
import Control.Exception.Assert.Sugar
import Control.Lens
import Control.Monad
import Data.Colour (opaque, withOpacity)
import qualified Data.Colour.Names as DCN
import Data.Colour.SRGB
import qualified Data.HashMap.Strict as HMS
import qualified Data.HashSet as HS
import Data.Set.Ordered as OSet (singleton)
import GHC.Compact
import GHC.Generics (Generic)
import Games.ECS
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.BrawlerAI
import Games.RogueLike.Components.Combat
import Games.RogueLike.Components.DamagesOnContact
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.IsPlayer
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Material
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Name
import Games.RogueLike.Components.ReceivesInput
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Map.Util
import Games.RogueLike.Systems.AnimationSystem
import Games.RogueLike.Systems.AwarenessSystem
import Games.RogueLike.Systems.BrawlerAI
import Games.RogueLike.Systems.EffectSystem
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Systems.Vision
import Games.RogueLike.Types.Calculation
-- import Polysemy.ConstraintAbsorber.MonadState

import Games.RogueLike.Types.Coordinates
import Games.RogueLike.Types.Damage
import Games.RogueLike.Types.DiceRoll
import Games.RogueLike.Types.Glyph
import Games.RogueLike.Types.RNGStore
import Games.RogueLike.Types.SpawnEffects
import Games.RogueLike.Types.Tick
import Games.RogueLike.Types.Voxel
import Games.RogueLike.UI.InputEvent
import Linear.V3
import Paths_starting_over
import Polysemy
import Polysemy.AtomicState
import Polysemy.State
import System.IO.Unsafe
import World
  ( Game,
    Thing,
  )
--import Data.Map



initialGameState :: IO GameState
initialGameState = do
  gs <-  initialGameStateCompacted
  sizeOfRegion <- compactSize gs
  putStrLn ("Initial game state size is " ++ showGFloat (Just 2) (((fromIntegral sizeOfRegion) :: Double) / 1024) "kB")
         
  pure $! step (getCompact gs ) True []


{-# NOINLINE initialGameStateCompacted #-}
initialGameStateCompacted :: IO (Compact GameState)
initialGameStateCompacted =  do
  withEverything <- withFloors
  compactWithSharing $ GameState (simpleJack ^. entityReference) withEverything (initialiseIndexingSystem' withEverything ) newGameLog 0 (initialiseRNGStore 123)


{-# NOINLINE simpleJack #-}
simpleJack :: Thing
simpleJack =
  (unsafePerformIO createNewEntity)
    & addName
      .~ "Simple Jack"
    & addHasAllegiances
      .~ HasAllegiances (OSet.singleton (Allegiance "Player faction")) HMS.empty
    & addPosition
      .~ Position (V3 6 9 0)
    & addIsPlayer
      .~ IsPlayer
    & addRenderable
      .~ Renderable (Glyph "@" (Colour (opaque DCN.white)) transparent) 0 (PlainVoxel (Colour (opaque DCN.white)))
    & addReceivesInput
      .~ ReceivesInput
    & addCanMove
      .~ CanMove
    & addHasVision
      .~ HasVision 13 newViewshed
    & addHealthStatus
      .~ HealthStatus 10000 10000
    & addNowAwareOf
      .~ blankNowAwareOf
    & addBlocksMovement
      .~ BlocksMovement (MovementPenalty 99999)
    & addHasObjectPermanence
      .~ blankObjectPermanence
    & addCombatStats
      .~ CombatStats (damageSpecificationByDiceRollCalculation "Melee damage" (2 `d` 3) Nothing)
    & addDoesBumpDamage
      .~ DoesBumpDamage
    & addMadeOf
      .~ MadeOf "mammal-flesh"

{-# NOINLINE wall #-}
wall :: Thing
wall =
  (unsafePerformIO createNewEntity)
    & addPosition
      .~ Position (V3 1 1 0)
    & addRenderable
      .~ Renderable (Glyph "#" (Colour (opaque DCN.gray)) transparent) 0 (PlainVoxel (Colour (opaque DCN.gray)))
    & addBlocksMovement
      .~ BlocksMovement ObstructsCompletely
    & addBlocksVision
      .~ BlocksVision

{-# NOINLINE spikeTrap #-}
spikeTrap :: Thing
spikeTrap =
  (unsafePerformIO createNewEntity)
    & addPosition
      .~ Position (V3 4 2 0)
    & addRenderable
      .~ Renderable (Glyph "^" (Colour (opaque DCN.magenta)) transparent) 0 (PlainVoxel (Colour (opaque DCN.magenta)))
    & addBlocksMovement
      .~ BlocksMovement (MovementPenalty 100)
    & addDamagesOnContact
      .~ DamagesOnContact (damageSpecificationByDiceRollCalculation "Spike trap" (3 `d` 2) Nothing)
    & addName
      .~ Name "Spike trap"

{-# NOINLINE spikeTrap2 #-}
spikeTrap2 :: Thing
spikeTrap2 =
  (unsafePerformIO createNewEntity)
    & addPosition
      .~ Position (V3 6 8 0)
    & addRenderable
      .~ Renderable (Glyph "^" (Colour (opaque DCN.magenta)) transparent) 0 (PlainVoxel (Colour (opaque DCN.magenta)))
    & addBlocksMovement
      .~ BlocksMovement (MovementPenalty 100)
    & addName
      .~ Name "Spike trap 2"
    & addDamagesOnContact
      .~ DamagesOnContact (damageSpecificationByDiceRollCalculation "Spike trap" (3 `d` 2) Nothing)

--    & addHiddenFrom .~ (HiddenFrom (HS.fromList [EntRef 13120]) (Just [CausedDamage]))
{-# NOINLINE spikeTrap3 #-}
spikeTrap3 :: Thing
spikeTrap3 =
  (unsafePerformIO createNewEntity)
    & addPosition
      .~ Position (V3 5 8 0)
    & addRenderable
      .~ Renderable (Glyph "^" (Colour (opaque DCN.magenta)) transparent) 0 (PlainVoxel (Colour (opaque DCN.magenta)))
    & addBlocksMovement
      .~ BlocksMovement (MovementPenalty 100)
    & addName
      .~ Name "Spike trap 3"
    & addDamagesOnContact
      .~ DamagesOnContact (damageSpecificationByDiceRollCalculation "Spike trap" (3 `d` 2) Nothing)
    & addHiddenFrom
      .~ ( HiddenFrom
             (fromList [simpleJack ^. entityReference, goblin ^. entityReference])
             ( neverReveal
                 & addRevealCondition' (RevealCondition CausedDamage (Challenge 3 (rollAdXpB 1 100 0)))
                 & addRevealCondition' (RevealCondition Seen (Challenge 100 (rollAdXpB 2 52 0)))
             )
         )

{-# NOINLINE spikeTrap4 #-}
spikeTrap4 :: Thing
spikeTrap4 =
  (unsafePerformIO createNewEntity)
    & addPosition
      .~ Position (V3 4 8 0)
    & addRenderable
      .~ Renderable (Glyph "^" (Colour (opaque DCN.magenta)) transparent) 0 (PlainVoxel (Colour (opaque DCN.magenta)))
    & addBlocksMovement
      .~ BlocksMovement (MovementPenalty 100)
    & addName
      .~ Name "Spike trap 4"
    & addDamagesOnContact
      .~ DamagesOnContact (damageSpecificationByDiceRollCalculation "Spike trap" (3 `d` 2) Nothing)
    & addHiddenFrom
      .~ ( HiddenFrom
             (fromList [simpleJack ^. entityReference, goblin ^. entityReference])
             ( neverReveal
                 & addRevealCondition' (RevealCondition CausedDamage (Challenge 3 (rollAdXpB 1 100 0)))
                 & addRevealCondition' (RevealCondition Seen (Challenge 100 (rollAdXpB 2 52 0)))
             )
         )

{-# NOINLINE goblin #-}
goblin :: Thing
goblin =
  (unsafePerformIO createNewEntity)
    & addName
      .~ "Shittle the Goblin"
    & addHasAllegiances
      .~ HasAllegiances (OSet.singleton (Allegiance "Goblins")) HMS.empty
    & addPosition
      .~ Position (V3 (1) 40 0)
    & addRenderable
      .~ Renderable (Glyph "g" (Colour (opaque DCN.green)) transparent) 0 (PlainVoxel (Colour (opaque DCN.green)))
    & addCanMove
      .~ CanMove
    & addHasVision
      .~ HasVision 9 newViewshed
    & addHealthStatus
      .~ HealthStatus 10 10
    & addNowAwareOf
      .~ blankNowAwareOf
    & addWantsToMove
      .~ WantsToMoveToEntity (simpleJack ^. entityReference)
    & addBlocksMovement
      .~ BlocksMovement (MovementPenalty 99999)
    & addCombatStats
      .~ CombatStats (damageSpecificationByDiceRollCalculation "Melee damage" (1 `d` 4) Nothing)
    & addBrawlerAI
      .~ BrawlerAI
    & addMadeOf
      .~ MadeOf "goblin-flesh"


{-# NOINLINE myWorld #-}
myWorld :: IO Game
myWorld = do
  let w = newWorld
          & storeEntity simpleJack
          & storeEntity wall
          & storeEntity spikeTrap
          & storeEntity spikeTrap2
          & storeEntity spikeTrap3
          & storeEntity goblin
  definitionsPath <- getDataFileName "raws/Definitions.xml"
  Prelude.Right withMaterials <- includePrototypesFromRaws @"materials" @"material" w definitionsPath
  Prelude.Right withFactions <- includePrototypesFromRaws @"factions" @"faction" withMaterials definitionsPath
  Prelude.Right withRemainingPrototypes <- includePrototypesFromRaws @"prototypes" @"prototype" withFactions definitionsPath
  pure withRemainingPrototypes

withFloors :: IO Game
{-# NOINLINE withFloors #-}
withFloors = do
               myWorld' <- myWorld
               ((spawnFloors (Position (V3 (-20) (-20) 0)) (Position (V3 100 100 0))) >=> (spawnWalls (Position (V3 (-20) (-20) 0)) (Position (V3 0 10 0)) "wall") >=> (spawnBoundaryWalls (Position (V3 (-30) (-30) 0)) (Position (V3 50 50 0)))) myWorld'
