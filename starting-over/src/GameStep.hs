module GameStep where
import Control.Concurrent.STM
-- import Data.Sequence
import Games.RogueLike.Types.PhysicalTypes
import Control.DeepSeq    
import Control.Exception.Assert.Sugar
import Control.Lens
import Games.ECS.Entity.EntityMap    
import Control.Monad
import Data.Colour (opaque, withOpacity)
import qualified Data.Colour.Names as DCN
import Data.Colour.SRGB
import qualified Data.HashMap.Strict as HMS
import qualified Data.HashSet as HS
import Data.Set.Ordered as OSet (singleton)
import GHC.Compact
import GHC.Generics (Generic)
import Games.ECS
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.BrawlerAI
import Games.RogueLike.Components.Combat
import Games.RogueLike.Components.DamagesOnContact
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.IsPlayer
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Material
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Name
import Games.RogueLike.Components.ReceivesInput
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Map.Util
import Games.RogueLike.Systems.AnimationSystem
import Games.RogueLike.Systems.AwarenessSystem
import Games.RogueLike.Systems.BrawlerAI
import Games.RogueLike.Systems.EffectSystem
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Systems.MapIndexingSystem
import Games.RogueLike.Systems.Vision
import Games.RogueLike.Types.Calculation
-- import Polysemy.ConstraintAbsorber.MonadState

import Games.RogueLike.Types.Coordinates
import Games.RogueLike.Types.Damage
import Games.RogueLike.Types.DiceRoll
import Games.RogueLike.Types.Glyph
import Games.RogueLike.Types.RNGStore
import Games.RogueLike.Types.SpawnEffects
import Games.RogueLike.Types.Tick
import Games.RogueLike.Types.Voxel
import Games.RogueLike.UI.InputEvent
import Linear.V3
import Paths_starting_over
import Polysemy
import Polysemy.AtomicState
import Polysemy.State
import System.IO.Unsafe
import World
  ( Game,
    Thing,
  )
import Data.Map
import qualified HsLua as Lua

type PrototypeMap = Map PrototypeID Entity    
data GameState = GameState
  { _playerRef :: !Entity,
    _theGame :: !Game,
    _mapIndex :: !MapIndexingSystem,
    _theGameLog :: !GameLog,
    _gameTime :: !GameTime,
    _rngStore :: !RNGStore
  }
  deriving (Show, Generic)
  deriving NFData
makeLenses ''GameState

-- saveGame :: GameState -> Element
-- saveGame gs=

getPlayerPosition :: GameState -> Position
getPlayerPosition gs = (gs ^. theGame) ^?! entity (gs ^. playerRef) . position

{-# NOINLINE effectSystem #-}
effectSystem :: TVar EffectSystem
effectSystem = unsafePerformIO $ newTVarIO newEffectSystem

instance (Members '[AtomicState (MessageQueue a)] r) => MonadAtomicMessageQueue a (Sem r) where
  stateMessageQueue = atomicState'
  modifyMessageQueue = atomicModify'

{-# SPECIALIZE INLINE step :: GameState -> Bool -> [DoEffect EffectSpecifics] -> GameState #-}                       
step :: (Foldable f) => GameState -> Bool -> f (DoEffect EffectSpecifics) -> GameState
step (GameState pref g sys gl oldGameTime oldRngStore) !turnTaken !effs = assert ((has (entity pref . isPlayer) game) `blame` "Player index corrupted! It instead points to something else" `swith` (game ^? entity pref)) $ force $ GameState pref (force game) newIndexingSys newgl newGameTime (unsafePerformIO $ readTVarIO rngStoreVar)
  where
    rngStoreVar = unsafePerformIO $ newTVarIO oldRngStore
    (newgl, (newIndexingSys, (RevealsByObservers _, (newGameTime, (game))))) =
      unsafePerformIO $
        runFinal $
          embedToFinal @IO $
            runAtomicStateTVar rngStoreVar $
              runAtomicStateTVar effectSystem $
                runState gl $
                  runState sys $
                    runState (RevealsByObservers (emptyEntityMap)) $
                      runState @GameTime oldGameTime $
                        if turnTaken then actionsToTake g else passiveActions ProcessCosmeticEffects g

    passiveActions !passiveOnly !oldGame =
      runSystem @"AnimationSystem" oldGame
        >>= runSystem @"VisionSystem"
        >>= runEffectsSystem passiveOnly

    actionsToTake :: (Members '[Embed IO, State GameLog, State MapIndexingSystem, State RevealsByObservers, State GameTime, AtomicState EffectSystem, AtomicState RNGStore] r) => Game -> Sem r Game
    actionsToTake !oldGame = do
      modify @GameTime (+ 1)
      queueDoEffects effs
      runSystem @"MovementSystem" oldGame
        >>= runBrawlerAI
        >>= passiveActions ProcessAllEffects

{-# INLINEABLE movementToDifferential #-}
movementToDifferential :: RelativeMovementCommand -> V3 WorldCoordinateScalar
movementToDifferential MoveNorth = V3 0 1 0
movementToDifferential MoveSouth = V3 0 (-1) 0
movementToDifferential MoveEast = V3 1 0 0
movementToDifferential MoveWest = V3 (-1) 0 0
movementToDifferential MoveNW = V3 (-1) 1 0
movementToDifferential MoveNE = V3 1 1 0
movementToDifferential MoveSW = V3 (-1) (-1) 0
movementToDifferential MoveSE = V3 1 (-1) 0
movementToDifferential Ascend = V3 0 0 1
movementToDifferential Descend = V3 0 0 (-1)

{-# INLINE respondToMovementCommand #-}
respondToMovementCommand :: RelativeMovementCommand -> Game -> Game
respondToMovementCommand !command !w =
  let deltaPos = movementToDifferential command
   in force ( w
          & receivesPlayerInput
            . addWantsToMove
            .~ WantsToMoveBy
              (Velocity deltaPos)
      )

respondToMovementCommand' :: MovementCommand -> GameState -> GameState
respondToMovementCommand' (MoveRelative command Nothing) (GameState pref w idx gl gt rngs) = GameState pref (respondToMovementCommand command w) idx gl gt rngs
                       
