module Frontend where
import Games.ECS
import GameState
import World    
import Data.Kind
import Games.RogueLike.UI.InputEvent
    
class Monad (EventMonad f) => FrontEnd f where
    type InputEvent f :: Type
    type EventMonad f :: (Type -> Type)
--    respondToInputRequest :: (forall ty. InputEvent f -> Maybe ty) ->
    respondToInputRequest :: InputContext c => c -> InputEvent f -> (EventMonad f) (Maybe (AcceptableInput c))
    dispatchArbitraryInput :: InputEvent f -> (EventMonad f) ()
--    translateInput :: InputEvent f -> 
