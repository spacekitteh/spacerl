{-# OPTIONS_GHC -fmax-simplifier-iterations=8 -fsimplifier-phases=6#-}
{-# LANGUAGE NoFieldSelectors #-}

{- OPTIONS_GHC -dth-dec-file -ddump-to-file -}
module World (module World
             , module Games.RogueLike.Components.IsPlayer
             , module Games.RogueLike.Components.Kinematics
             , module Games.RogueLike.Components.Renderable
             , module Games.RogueLike.Components.HealthStatus) where


import Control.Lens
import Control.DeepSeq    
import Data.Colour.Names
import qualified Data.HashMap.Strict as HMS
import qualified Data.HashSet as HS
import Data.Maybe (fromMaybe)
import Games.RogueLike.Components.Target
import Games.RogueLike.Components.Targetable    
import GHC.Generics
import Games.ECS
import Games.RogueLike.Components.DamagesOnContact
import Games.RogueLike.Components.Allegiance
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.IsPlayer
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Name
import Games.RogueLike.Components.ReceivesInput
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.MovementComponents
import Games.RogueLike.Components.Material
import Games.RogueLike.Components.BrawlerAI
import Games.RogueLike.Components.Combat
import Games.RogueLike.Components.Initiative
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Types.Glyph
import Linear
  ( V2 (..),
    V3 (..),
    V4 (..),
    distance,
  )

{-data Fgsfds s where
  Fgsfds :: EntRefField s ->
    AComponent "position" s Position ->
    AComponent "name" s Name ->
    Fgsfds s
    deriving (Generic)

makeWorld ''Fgsfds 
deriving instance Show (Fgsfds Individual)
deriving instance Show (Fgsfds Storing)
-}
type Game = GameWorld Storing

type Thing = GameWorld Individual

data GameWorld s where
  GameWorld ::
    EntRefField s ->
    AComponent "isPrototype" s IsPrototype ->
    AComponent "spawnedFromPrototype" s SpawnedFromPrototype ->        
    AComponent "position" s Position ->
    AComponent "name" s Name ->
    AComponent "isPlayer" s IsPlayer ->
    AComponent "renderable" s Renderable ->
    AComponent "currentAnimation" s AnimatedRenderable -> 
    AComponent "receivesInput" s ReceivesInput ->
    AComponent "wantsToMove" s WantsToMove ->
    AComponent "canMove" s CanMove ->
    AComponent "madeOf" s MadeOf ->
    AComponent "blocksMovement" s BlocksMovement ->
    AComponent "hasVision" s HasVision ->
    AComponent "combatStats" s CombatStats ->
    AComponent "brawlerAI" s BrawlerAI -> 
    AComponent "doesBumpDamage" s DoesBumpDamage -> 
    AComponent "healthStatus" s HealthStatus ->
    AComponent "damagesOnContact" s DamagesOnContact ->
    AComponent "justDied" s JustDied ->
    AComponent "isDead" s IsDead ->
    AComponent "hiddenFrom" s HiddenFrom ->
    AComponent "nowAwareOf" s NowAwareOf ->
    AComponent "positionChanged" s PositionChanged ->
    AComponent "blocksVision" s BlocksVision ->
--    AComponent "movementPenalty" s MovementPenalty ->
    AComponent "canWalkOn" s CanWalkOn ->
    AComponent "supportsWeight" s SupportsWeight ->
    AComponent "hasObjectPermanence" s HasObjectPermanence ->
    AComponent "basicMaterialData" s BasicMaterialData ->
    AComponent "hasAllegiances" s HasAllegiances ->
    AComponent "isFaction" s IsFaction ->
    AComponent "isSquad" s IsSquad ->
    AComponent "squadMember" s SquadMember ->
    AComponent "hasOrders" s HasOrders ->
    AComponent "initiative" s Initiative ->
    AComponent "myTurn" s MyTurn ->
    AComponent "canTarget" s CanTarget ->
    AComponent "targetableByDefault" s TargetableByDefault ->
    AComponent "target" s Target -> 
    GameWorld s
  deriving (Generic)

-- TODO make the "addPosition" etc take initial values
makeWorld ''GameWorld

deriving instance Show (GameWorld Individual)

deriving instance Show (GameWorld Storing)

deriving instance Eq (GameWorld Individual)
deriving instance Eq (GameWorld Storing)

deriving instance NFData (GameWorld Storing)
deriving instance NFData (GameWorld Individual)
         
