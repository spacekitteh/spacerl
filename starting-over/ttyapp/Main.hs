{-# LANGUAGE NumericUnderscores #-}

module Main where

-- import           SDL

-- import           Control.Monad                  ( unless )
import Games.RogueLike.UI.Widgets (mapRenderSettings, mapWidth, mapHeight)
import Brick
  ( App (..),
    AttrMap,
    BrickEvent (AppEvent, MouseDown, VtyEvent),
    EventM,
    Location (..),
    Widget,
    vLimit, hLimit,
    attrMap,
    attrName,
    clickable,
    customMain,
    emptyWidget,
    hBox,
    halt,
    modifyDefAttr,
    neverShowCursor,
    on,
    overrideAttr,
    showCursor,
    showFirstCursor,
    str,
    vBox,
    withBorderStyle,
    (<+>),
    (<=>),
  )
import Brick.BChan (newBChan, writeBChan)
-- import           GHC.Generics
--import GHC.Debug.Stub
import qualified Brick.Widgets.Border as B
import qualified Brick.Widgets.Border.Style as BS
import Control.DeepSeq    
import qualified Brick.Widgets.Center as C
import Brick.Widgets.Dialog (dialog, renderDialog)
import Brick.Widgets.List as BWL
import Brick.Widgets.ProgressBar
import Control.Concurrent
  ( forkIO,
    threadDelay,
  )
-- import Data.Tagged
-- import Data.Tagged ()

import Control.Concurrent.STM
import Control.Exception.Assert.Sugar
import Control.Lens
import Control.Lens
  ( filtered,
    findOf,
    firstOf,
    folded,
    has,
    notElemOf,
    to,
    (&),
    (^.),
    (^..),
    (^?!),
  )
import Control.Monad
import Control.Monad
  ( forever,
    void,
  )
import Control.Monad.IO.Class (liftIO)
import Control.Monad.State.Class
import qualified Data.Colour.Names as DCN
import Data.Foldable
import qualified HsLua.Core as Lua
import qualified HsLua.Classes as Lua    
import Data.Int
import Data.Sequence
import Data.Text (unpack)
import Data.Word
import Debug.Trace
import GHC.Compact
import GameState
import GameStep    
import Games.ECS hiding (State)
import Games.RogueLike.Components.Awareness
import Games.RogueLike.Components.HealthStatus
import Games.RogueLike.Components.Kinematics
import Games.RogueLike.Components.Renderable
import Games.RogueLike.Components.Sensory
import Games.RogueLike.Systems.GameLog
import Games.RogueLike.Types.Glyph
  ( Glyph,
    backgroundColour,
    foregroundColour,
    symbol,
    toRGB,
  )
import Games.RogueLike.Types.SpawnEffects
import Games.RogueLike.Types.Tick
import Games.RogueLike.UI.InputEvent
  ( MovementCommand (MoveRelative),
    RelativeMovementCommand (MoveEast, MoveNorth, MoveSouth, MoveWest),
  )
import Graphics.Vty
import Graphics.Vty as V
  ( Attr,
    Event (EvKey),
    Key (KChar, KDown, KEsc, KLeft, KRight, KUp),
    black,
    defAttr,
    defaultConfig,
    rgbColor,
    white,
  )
import Graphics.Vty.CrossPlatform as V (mkVty)
import Linear
  ( V2 (..),
    V3 (..),
  )
import UX
import World



-- App definition
app :: App (GameState) Tick ResName
app =
  App
    { appDraw = drawUI,
      appChooseCursor = showFirstCursor, -- neverShowCursor,
      appHandleEvent = handleEvent,
      appStartEvent = pure (),
      appAttrMap = const theAttrMap
    }

step' g = step g True []

timerStep' g = step g False []

handleEvent :: BrickEvent ResName Tick -> EventM ResName GameState ()
handleEvent (AppEvent Tick) = force <$> modify step'
handleEvent (AppEvent TimerTock) = force <$> modify timerStep'
handleEvent (VtyEvent (V.EvKey (V.KChar ' ') [])) = modify step'
handleEvent (VtyEvent (V.EvKey V.KUp [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveNorth Nothing)))
handleEvent (VtyEvent (V.EvKey V.KDown [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveSouth Nothing)))
handleEvent (VtyEvent (V.EvKey V.KRight [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveEast Nothing)))
handleEvent (VtyEvent (V.EvKey V.KLeft [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveWest Nothing)))
handleEvent (VtyEvent (V.EvKey (V.KChar 'k') [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveNorth Nothing)))
handleEvent (VtyEvent (V.EvKey (V.KChar 'j') [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveSouth Nothing)))
handleEvent (VtyEvent (V.EvKey (V.KChar 'l') [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveEast Nothing)))
handleEvent (VtyEvent (V.EvKey (V.KChar 'h') [])) =
  modify (step' . (respondToMovementCommand' (MoveRelative MoveWest Nothing)))
handleEvent (VtyEvent (V.EvKey (V.KChar 'r') [])) = do
  gs <- liftIO initialGameState
  put gs
handleEvent (VtyEvent (V.EvKey (V.KChar 'q') [])) = halt
handleEvent (VtyEvent (V.EvKey V.KEsc [])) = halt
handleEvent (MouseDown MainMap !mainMap modifiers (Location (x', y'))) = do
  g <- get
  let (Position playerPos) = getPlayerPosition g
      pos = Position (playerPos + (V3 ((fromIntegral x') - ((mainMapSettings^.mapRenderSettings.mapWidth) `div` 2)) (((mainMapSettings^.mapRenderSettings.mapWidth) `div` 2) - (fromIntegral y') - 1) 0))
  put $ step g True [newEffect (simpleJack ^. entityReference) [(TargetPosition pos)] (SpawnPrototype ("goblin"))]
handleEvent _ = pure ()

-- Drawing

drawUI :: GameState -> [Widget ResName]
drawUI gs@(GameState !pref !g _ _ _ _) = ui
  where
    ui' = ((Brick.<+>) (C.center $ drawMap gs renderGlyphToBrickAttribute) (C.center $ drawSideBar gs)) <=> (drawGameLog gs)
    (Just player) = g ^? entity pref
    !ui =
      if has isDead player
        then [renderDialog (dialog (Just (str "Game over!")) Nothing 16) (str "  YOU DIED  "), ui']
        else [ui']

drawSideBar :: GameState -> Widget ResName
drawSideBar _gs@(GameState pref !g _ _ _ _) = withBorderStyle BS.unicodeRounded $ B.borderWithLabel (str "Status") $ statusBars
  where
    statusBars = healthBar -- <=> manaBar etc
    healthBar = overrideAttr progressIncompleteAttr (attrName "healthBar" <> attrName "incomplete") $ overrideAttr progressCompleteAttr (attrName "healthBar" <> attrName "complete") $ progressBar (Just $ "HP: " ++ show curHealth ++ "/" ++ show maxHealth) (fromRational . toRational $ curHealth / maxHealth)
    (Just !thePlayer) = g ^? entity pref
    curHealth = thePlayer ^?! healthStatus . health
    maxHealth = thePlayer ^?! healthStatus . maximumHealth

drawGameLog :: GameState -> Widget ResName
drawGameLog gs = vLimit 7 $ withBorderStyle BS.unicodeRounded $ B.borderWithLabel (str "Log") renderedList
  where
    theList = BWL.listMoveToEnd $ BWL.list TheGameLog (gs ^. theGameLog . messages {-allMessages-}) 1 -- TODO: Introduce a "show debug messages" option and change allMessages back to just messages
    renderedList = BWL.renderList (\_ e -> str (show e)) False theList

drawMap :: GameState -> (Thing -> Position -> Glyph -> Widget ResName) -> Widget ResName
drawMap !g !toDrawCoord =
  --
  vLimit (fromIntegral $ mainMapSettings^.mapRenderSettings . mapHeight) $ hLimit (fromIntegral $ mainMapSettings ^.mapRenderSettings . mapWidth) $ withBorderStyle BS.unicodeRounded $
    B.borderWithLabel (str "spaceRL") $
      clickable MainMap $
        vBox $
          fmap hBox $
            (drawMap' g toDrawCoord)

renderGlyphToBrickAttribute :: Thing -> Position -> Glyph -> Widget ResName
renderGlyphToBrickAttribute playerEntity absPos renderData =
  (str $ renderData ^. {-glyph .-} symbol . to unpack)
    & modifyDefAttr
      ( \_ ->
          if positionVisibleToEntity playerEntity absPos
            then (glyphAttr (renderData))
            -- \^. glyph
            else (rgbColor (255 :: Int) 00 255) `on` (rgbColor (25 :: Int) 25 25) -- )
      )

glyphAttr :: Glyph -> Attr
glyphAttr theGlyph = fg `on` bg
  where
    fg = myColourToVtyColour (theGlyph ^. foregroundColour)
    bg = myColourToVtyColour (theGlyph ^. backgroundColour)
    myColourToVtyColour colour = toRGB colour & (\(r, g, b) -> rgbColor r g b)

whiteOnBlack :: Attr
whiteOnBlack = white `on` (rgbColor (0 :: Int) 0 0)

theAttrMap :: AttrMap
theAttrMap = attrMap V.defAttr [(attrName "healthBar" <> attrName "complete", (white `on` red) `withStyle` bold), (attrName "healthBar" <> attrName "incomplete", (white `on` (rgbColor (40 :: Word8) 0 0)) `withStyle` bold)]
prog :: Lua.Lua ()
prog = do
      Lua.openlibs  -- load Lua libraries so we can use 'print'
      Lua.invoke "print" ("Hello, World (from Lua)!"::String)
main :: IO ()
main = {-withGhcDebug $ -}do
  Lua.run prog
  

  chan <- newBChan 10
  traceMarkerIO "Initialising game world"
  g <-initialGameState
  traceMarkerIO "Starting vty event loop"
  initialVty <- V.mkVty V.defaultConfig
  let output = outputIface initialVty
  when (supportsMode output Mouse) $
    setMode output Mouse True
--  forkIO $
--    forever $ do
--      writeBChan chan TimerTock -- TODO FIXME gotta check if there is pending player input, and only put this in the queue if there is nothing to process
--      threadDelay 33_333 -- microseconds
  endState <- customMain initialVty (V.mkVty V.defaultConfig) (Just chan) app g

  -- let (GameState !a !b !c !d) = step $ step $ step g
  -- putStrLn (show (endState^.theGameLog.allMessages))
  pure ()
