{ pkgs ? import <nixpkgs> {},
  ghc ? pkgs.haskell.compiler.ghc982, #ghc946,
llvm ? pkgs.llvmPackages_15.llvm
}:

with pkgs;
#mkShell {
#nativeBuildInputs = [haskellPackages.hs-speedscope];
#}
haskell.lib.buildStackProject {
  name = "spaceRL";
  inherit ghc;
#  nativeBuildInputs = [haskellPackages.hs-speedscope];
#addOpenGLRunpath];
  nativeBuildInputs = [pkg-config racket];
  buildInputs = [
cabal-install
#    xlibsWrapper 
freetype fontconfig xorg.xorgproto xorg.libX11 xorg.libXt xorg.libXft xorg.libXext xorg.libSM xorg.libICE
xorg.libX11 xorg.xf86videofbdev libGL libGLU   mesa freeglut mesa_glu mesa_drivers libglvnd
    cairo zlib icu vulkan-loader vulkan-headers shaderc ncurses.dev llvm icu racket #guile_3_0
    glew    SDL2_image SDL2_mixer SDL2_gfx icu SDL2 SDL2_ttf bzip2
    ];
#  LD_LIBRARY_PATH = [ "${libGL}/lib" "${mesa_glu}/lib" "${libglvnd}/lib" "${mesa_drivers}/lib" ];
}
