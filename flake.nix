{
  description = "starting-over and spaceRL";
  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";

  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      haskellNix,
    }:
    flake-utils.lib.eachSystem
      [
        "x86_64-linux"
        "x86_64-darwin"
      ]
      (
        system:
        let
          overlays = [
            haskellNix.overlay
            (final: prev: {
              ghc9121 = prev.ghc9121.override { llvmPackages = pkgs.llvmPackages_19; };
            })

            (final: prev: {
              # This overlay adds our project to pkgs
              startingOver = final.haskell-nix.project' {
                src = ./.;
                compiler-nix-name = "ghc9121llvm";

                # This is used by `nix develop .` to open a shell for use with
                # `cabal`, `hlint` and `haskell-language-server`
                shell.tools = {
                  cabal = { };
                  haskell-language-server = "latest";
                };
                # Non-Haskell shell tools go here
                shell.buildInputs = with pkgs; [
                  nixpkgs-fmt
                  llvmPackages_15.llvm
                  llvmPackages_15.clang
                ];
                # This adds `js-unknown-ghcjs-cabal` to the shell.
                # shell.crossPlatforms = p: [p.ghcjs];

                modules = [
                  {
                    packages.ghc-tcplugins-extra.patches = [ ./ghc-tcplugins-extra-0.4.6.patch ];
                    packages.polysemy-plugin.patches = [ ./polysemy-plugin-0.4.5.2.patch ];
                  }

                  {
                    packages.spaceRL.components.library.build-tools = [
                      pkgs.llvmPackages_15.clang
                      pkgs.llvmPackages_15.llvm
                    ];
                  }
                ];

              };
            })
          ];
          pkgs = import nixpkgs {
            inherit system overlays;
            inherit (haskellNix) config;
          };
          flake = pkgs.startingOver.flake {
            # This adds support for `nix build .#js-unknown-ghcjs:hello:exe:hello`
            # crossPlatforms = p: [p.ghcjs];
          };
        in
        flake
        // {
          devShells.default = pkgs.startingOver.shellFor { buildInputs = [ pkgs.startingOver.roots ]; };
          formatter = pkgs.nixfmt-rfc-style;
          # Built by `nix build .`
          packages.default = flake.packages."starting-over:exe:ttyapp";
        }
      );
}
