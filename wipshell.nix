{ nixpkgs ? import <nixpkgs> {}, 
 ghc ? nixpkgs.ghc,
llvm ? nixpkgs.llvmPackages_12.llvm,
compiler ? "ghc8107" }:
nixpkgs.pkgs.haskell.packages.${compiler}.callPackage ./cabalnix.nix { ohhecs = (nixpkgs.pkgs.haskell.packages.${compiler}.callPackage) ./ohhecs/ohhecs.nix;}
