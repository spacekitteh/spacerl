{-# LANGUAGE TemplateHaskell #-}

-- |
-- Module      :  Games.ECS.Serialisation
-- Description : Generic XML Serialisation.
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- Generic XML serialisation and deserialisation support. Adapted from the /generic-xmlpickler/ library, based on the /hxt/ package.
module Games.ECS.Serialisation
  ( XMLSerialise (..),
    XMLPickler (..),
    XMLPickleAsAttribute (..),
    Node,
    module Data.XML.Pickle,
    AsString (..),
    GXmlPickler (..),
    formatElement,
    AsList (..),
    ElideAsDefault(..),
    optElem,
    optElemD,
    optElemC,
    SerialisationException (..),
    mapPickler, mapPickler',


    xsdNamespaced,xsdElementName,          
    simpleSchemaElement', simpleSchemaElement'', boundedIntegralRestriction, makeBoundedRestriction
  )
where
import Data.Proxy
import Control.Exception  
import Control.Lens
import Data.Char (toLower)
import Data.Coerce
import Data.HashMap.Strict (HashMap)
import Data.HashMap.Strict qualified as HMS
import Data.HashSet (HashSet)
import Data.Hashable
import Data.Int
import Data.Random.Distribution.Categorical (Categorical, fromWeightedList)
import qualified Data.Random.Distribution.Categorical as Categorical
import Data.Interned
import Data.Interned.Text
import Data.Kind
import Data.Sequence (Seq)
import Data.Set (Set)    
import Data.Set.Ordered (OSet)
import Data.Map (Map)
import qualified Data.Map as Map    
import Data.Set.Ordered qualified as OSet
import Data.String
import Data.Text (Text, pack)
import Data.Word
import Data.XML.Pickle
import Data.XML.Types
import GHC.Exts
import GHC.Generics
import GHC.TypeLits
import Data.Default.Class
data SerialisationException = UnpicklingException UnpickleError
                              | XMLConversionException (Set Text) deriving (Show, Exception)        
    
{--- | Serialise to QNames? It must be only a URI fragment.
data SerialisationIdentifier =
  SerialisationIdentifier {_identity :: URI}
                             deriving stock (Eq, Show, Generic)

makeLenses ''SerialisationIdentifier

-- | Serialise to IDREF?
data SerialisationReference =
  SerialisationReference {_reference :: URI}
                             deriving stock (Eq, Show, Generic)

makeLenses ''SerialisationReference
-}

-- | A convenience wrapper around an 'XMLPickler'.
class XMLSerialise a where
  {-# MINIMAL #-}

  -- | Serialise an /a/ as a named t'Element'.
  serialise :: String -> a -> Element
  {-# INLINE serialise #-}
  default serialise :: (XMLPickler [Node] a) => String -> a -> Element
  serialise n a = Element (fromString n) [] $ pickle (xpickle :: PU [Node] a) a

  -- | Deserialise a named t'Element'.
  deserialise :: String -> Element -> Either UnpickleError a
  {-# INLINE deserialise #-}
  default deserialise :: (XMLPickler [Node] a) => String -> Element -> Either UnpickleError a
  deserialise name (Element n _ a) | n == (fromString name) = unpickle (("During deserialisation", "") <?+> xpickle :: PU [Node] a) a
  deserialise name (Element n _ _) = Left $ ErrorMessage ("Error during unpickling: Expected " <> (fromString name) <> ", got " <> (fromString . show $ n))

instance {-# OVERLAPPABLE #-} (XMLPickler [Node] a) => XMLSerialise a where
  {-# INLINE serialise #-}
  serialise n a = Element (fromString n) [] $ pickle (xpickle :: PU [Node] a) a
  {-# INLINE deserialise #-}
  deserialise name (Element n _ a) | n == (fromString name) = unpickle (("During deserialisation", "") <?+> xpickle :: PU [Node] a) a
  deserialise name (Element n _ _) = Left $ ErrorMessage ("Error during unpickling: Expected " <> (fromString name) <> ", got " <> (fromString . show $ n))

-- | A lower-level pickler class.
class XMLPickler t a where
  -- | A combined pickler/unpickler.
  xpickle :: PU t a
  {-# INLINE xpickle #-}
  default xpickle :: (Generic a, GXmlPickler t (Rep a)) => PU t a
  xpickle = ("Via generic pickler", "") <?+> gpickle

  xsdSchemaDescription :: a -> Node

xsdNamespaced :: Text -> Name
xsdNamespaced n = Name n (Just "http://www.w3.org/2001/XMLSchema") (Just "xs")
               
xsdElementName :: Name
xsdElementName = xsdNamespaced "element"
                 
xsdSimpleTypeName :: Name
xsdSimpleTypeName = xsdNamespaced "simpleType"
                    
simpleSchemaElement'' :: Content -> Content -> [(Name,[Content])] -> [Node] -> Element
simpleSchemaElement'' name ty extras nodes = Element xsdElementName ((("name" :: Name,[name]) :  ("type" :: Name,[ty]) :  extras)) nodes
simpleSchemaElement' :: Content -> Content ->  [(Name,[Content])] -> Element
simpleSchemaElement' n ty e = simpleSchemaElement'' n ty e []

makeBoundedRestriction :: Show a => Content -> Maybe a -> Maybe a -> Node
makeBoundedRestriction name lower upper = NodeElement $ Element (xsdNamespaced "restriction") [("base", [name])] others where
    others = low <> high
    low = case lower of
            Nothing -> []
            Just l -> [NodeElement $ Element (xsdNamespaced "minInclusive") [("value", [fromString . show $ l])] [] ]
    high = case upper of
             Nothing -> []
             Just u -> [ NodeElement $ Element (xsdNamespaced "maxInclusive") [("value", [fromString . show $ u])] [] ]
                              
boundedIntegralRestriction :: forall i. (Bounded i, Integral i, Show i) => Proxy i -> Element
boundedIntegralRestriction Proxy = Element xsdSimpleTypeName [] [
                                    NodeElement $ Element (xsdNamespaced "restriction") [("base", ["xs:integer"])] [
                                     NodeElement $ Element (xsdNamespaced "minInclusive") [("value", [fromString . show $ (minBound @i)])] [],
                                     NodeElement $ Element (xsdNamespaced "maxInclusive") [("value", [fromString . show $ (maxBound @i)])] []                                                        
                                    ]
                                   ]

-- | For when a type can be pickled as an XML 'Attribute'.
class XMLPickleAsAttribute a where
  pickleAsAttribute :: Name -> PU [Attribute] a

--  default pickleAsAttribute :: (forall a b. Coercible a b, XMLPickleAsAttribute b) => Name -> PU [Attribute] a
--  {-# INLINE pickleAsAttribute #-}
--  pickleAsAttribute name = xpWrap (coerce @a) (coerce :: _) (pickleAsAttribute name)

instance (XMLPickleAsAttribute a) => XMLPickleAsAttribute (Maybe a) where
  {-# INLINE pickleAsAttribute #-}
  pickleAsAttribute = xpOption . pickleAsAttribute

instance {-# OVERLAPPABLE #-} (Read a, Show a) => XMLPickleAsAttribute a where
  {-# INLINE pickleAsAttribute #-}
  pickleAsAttribute name = xpAttribute name xpPrim



-- | A deriving-via helper.
newtype AsString a = AsString {unAsString :: a} deriving newtype (Eq, Show, Read, IsString)


    
instance (IsString a, Show a) => XMLPickleAsAttribute (AsString a) where
  {-# INLINE pickleAsAttribute #-}
  pickleAsAttribute name = xpWrap (AsString . fromString) (show . unAsString) (pickleAsAttribute name)

instance XMLPickleAsAttribute InternedText where
    pickleAsAttribute name = xpWrap intern unintern (pickleAsAttribute name)
instance XMLPickleAsAttribute Text where
    pickleAsAttribute name = xpAttribute name xpText
--deriving via (AsString InternedText) instance XMLPickleAsAttribute InternedText

{-# INLINE gpickle #-}

-- | A generic pickler.
gpickle :: forall t a. (Generic a, GXmlPickler t (Rep a)) => PU t a
gpickle = (xpWrap) (GHC.Generics.to @a) (GHC.Generics.from @a) (gxpicklef (gpickle @t @a))

newtype ElideAsDefault a = ElideAsDefault {unElideAsDefault :: a} deriving newtype (Eq, Show, Read, IsString, Default)    
instance Default a => XMLPickler [Node] (ElideAsDefault a) where
    xpickle = ("AsDefault","") <?+> xpConst def xpUnit
              

-- | A deriving-via helper.
newtype AsList a = AsList {unAsList :: a} deriving newtype (Eq, Show)

instance (IsList a) => IsList (AsList a) where
  type Item (AsList a) = Identity (Item a)
  {-# INLINE fromList #-}
  {-# INLINE toList #-}
  fromList = AsList . fromList . coerce
  toList = coerce . toList . unAsList

instance (IsList a, XMLPickler [Node] (Item a)) => XMLPickler [Node] (AsList a) where
  {-# INLINE xpickle #-}
  xpickle = ("AsList", "") <?+> (xpWrap (AsList . fromList) (toList . unAsList) $ xpAll xpickle) -- xpSeqWhile (xpIsolate xpickle))--{-xpSeqWhile-} xpFindMatches xpickle)

instance (Ord a, XMLPickler [Node] a) => XMLPickler [Node] (OSet a) where
  {-# INLINE xpickle #-}
  xpickle = ("OSet", "") <?+> (xpDefault OSet.empty (xpWrap OSet.fromList OSet.toAscList $ xpAll (("Set item","") <?+> xpickle)))

deriving via AsList (HashSet v) instance (Eq v, Hashable v, XMLPickler [Node] v) => XMLPickler [Node] (HashSet v)
deriving via AsList (Set v) instance (Eq v, Ord v, XMLPickler [Node] v) => XMLPickler [Node] (Set v)
deriving via AsList (Seq v) instance (XMLPickler [Node] v) => XMLPickler [Node] (Seq v)

mapPickler :: (XMLPickler [Node] v, XMLPickleAsAttribute k) => Name -> Name -> PU [Node] v -> PU [Node] [(k,v)]
mapPickler itemName keyName pickler = ("Map pickler", "") <?+>
             (xpWrap (maybe [] id) (\m -> if null m then Nothing else Just m))  -- don't encode empty maps
               (xpOption $ xpAll $ xpElem itemName (pickleAsAttribute keyName) pickler)
mapPickler' :: ( XMLPickleAsAttribute k) => Name -> Name -> PU [Attribute] v -> PU [Node] [(k,v)]
mapPickler' itemName keyName pickler = ("Map pickler", "") <?+>
             (xpWrap (maybe [] id) (\m -> if null m then Nothing else Just m))  -- don't encode empty maps
               (xpOption $ xpAll $ xpElemAttrs itemName (xpPair (pickleAsAttribute keyName) pickler))

instance {-# OVERLAPPABLE #-} (Ord k, XMLPickleAsAttribute k, XMLPickler [Node] v) => XMLPickler [Node] (Map k v) where
    {-# INLINE xpickle #-}
    xpickle = ("Map","") <?+> (xpWrap Map.fromList Map.toList) (mapPickler "li" "key" xpickle)
             
instance {-# OVERLAPPABLE #-} (Eq k, Hashable k, XMLPickleAsAttribute k, XMLPickler [Node] v) => XMLPickler [Node] (HashMap k v) where
  {-# INLINE xpickle #-}
  xpickle =
    ("HashMap", "")
      <?+> ( xpWrap HMS.fromList HMS.toList
                    (mapPickler "li" "key" xpickle)
             
           )
      

instance XMLPickler [Node] Word where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Word)

instance XMLPickler [Node] Word8 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Word8)
instance XMLPickler [Node] Word16 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Word16)
instance XMLPickler [Node] Word32 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Word32)
instance XMLPickler [Node] Word64 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Word64)
instance XMLPickler [Node] Natural where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
--  xsdSchemaDescription _ = makeBoundedRestriction "integer" (Just (0 :: Natural)) Nothing
instance XMLPickler [Node] Int where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Int)
instance XMLPickler [Node] Int8 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Int8)
instance XMLPickler [Node] Int16 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Int16)            

instance XMLPickler [Node] Int32 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Int32)
instance XMLPickler [Node] Int64 where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
  xsdSchemaDescription _ = NodeElement $ boundedIntegralRestriction (Proxy @Int64)
instance XMLPickler [Node] Integer where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim
--  xsdSchemaDescription _ = makeBoundedRestriction "integer" Nothing Nothing
instance XMLPickler [Node] InternedText where
  {-# INLINE xpickle #-}
  xpickle = ("InternedText", "") <?+> (xpWrap (intern) unintern xpickle)

instance XMLPickler [Node] Float where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim

instance XMLPickler [Node] Double where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim

instance XMLPickler [Node] Bool where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpBool

instance XMLPickler [Node] Text where
  {-# INLINE xpickle #-}
  xpickle = ("Text", "") <?+> xpContent (xpWrap (\t -> if t == "&#032;" then " " else t) (\t -> if t == " " then "&#032;" else t) xpText) --(xpWrap pack unpack (xpString)))

instance XMLPickler [Node] String where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpString

instance XMLPickler [Node] Char where
  {-# INLINE xpickle #-}
  xpickle = xpContent xpPrim

instance {-# OVERLAPPABLE #-} forall t a. (Generic a, GXmlPickler t (Rep a)) => XMLPickler t a where
  {-# INLINE xpickle #-}
  xpickle = ("Via generic pickler", "") <?+> gpickle

instance {-# OVERLAPPABLE #-} (Read a, Show a) => XMLPickler [Node] (AsString a) where
  {-# INLINE xpickle #-}
  xpickle = ("AsString", "") <?+> (xpWrap (AsString . read) (show . unAsString) (xpContent xpString))

instance {-# OVERLAPPABLE #-} (Read a, Show a) => XMLPickler Data.Text.Text a where
  {-# INLINE xpickle #-}
  xpickle = ("prim", "") <?+> xpPrim

-- TODO: Do parametric types.

instance {-# OVERLAPPABLE #-} forall p a. (Eq p, Fractional p, XMLPickler [Node] p, XMLPickleAsAttribute a, Show p, Read p) => XMLPickler [Node] (Categorical p a) where
    {-# INLINE xpickle #-}
    xpickle = ("Categorical distribution", "") <?+> (xpWrap fromWeightedList Categorical.toList) (mapPickler' "possibility" "weight" (pickleAsAttribute "outcome") {-(xpAttribute "" xpPrim)-})



-- | Generic pickling support.
class GXmlPickler t f where
  gxpicklef :: PU t a -> PU t (f a)
  {-# INLINE gxpicklef #-}
  gxpicklef = gxpickleContentsf
  gxpickleContentsf :: PU t a -> PU t (f a)

-- | For individual record fields.
instance (XMLPickler t a) => GXmlPickler t (K1 i  a) where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf _ = xpWrap K1 unK1 (("K1", "") <?+> xpickle)
-- | For empty constructors
instance GXmlPickler [t] U1 where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf _ = xpWrap (const U1) (const ()) xpUnit

-- | For products of fields
instance (GXmlPickler [t] f, GXmlPickler [t] g) => GXmlPickler [t] (f :*: g) where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf f = xpWrap (uncurry (:*:)) (\(a :*: b) -> (a, b)) (((":*:", "left") <?+> gxpicklef f) `xpPair` ((":*:", "right") <?+> gxpicklef f))

-- | For sums of constructors
instance (GXmlPickler t f, GXmlPickler t g) => GXmlPickler t (f :+: g) where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf f = xpMayFail (((":+:", "left") <?+> xpMayFail (gxpicklef f)) `xpSum` ((":+:", "right") <?+> xpMayFail (gxpicklef f)))

-- | For datatypes
instance {-# OVERLAPPABLE #-} (Datatype d, GXmlPickler t f) => GXmlPickler t (D1 d f) where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf f = ("For type " <> (pack $ datatypeName (undefined :: M1 D d f p)), "") <?+> (xpWrap M1 unM1 (gxpicklef f))

-- | For constructors
instance {-# OVERLAPPABLE #-} (Constructor c, GXmlPickler [Node] f) => GXmlPickler [Node] (C1 c f) where
  {-# INLINE gxpickleContentsf #-}
  {-# INLINE gxpicklef #-}
  gxpicklef f = ("Constructor " <> (pack consName) <> ":", "") <?+> xpElemNodes (fromString . formatElement $ consName) (gxpickleContentsf f) where
                 consName =   conName (undefined :: M1 C c f p)                                    
  gxpickleContentsf f = (xpWrap M1 unM1 (gxpicklef f))

-- | For record field selectors with names.
instance {-# OVERLAPPABLE #-} (Selector c, GXmlPickler [Node] f) => GXmlPickler [Node] (S1 c f) where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf f = optElem (((pack $ "While in field " ++ selName (undefined :: S1 c f p), "") <?+> xpWrap M1 unM1 (gxpicklef f))) (undefined :: M1 S c f p)

-- | For Maybe types
instance {-# OVERLAPPING #-} (XMLPickler [Node] a, Selector c) => GXmlPickler [Node] (S1 c (K1 i (Maybe a))) where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf _ = xpWrap (M1 . K1) (unK1 . unM1) (xpOption $ optElem xpickle (undefined :: S1 c f p))

-- | An instance for types with a single constructor. Don't bother with the constructor; just the datafields.
instance {-# OVERLAPPING #-} (Constructor c'', GXmlPickler [Node] g, GXmlPickler [Node] h, Datatype d, GXmlPickler [Node] f,  Datatype d, ty ~ (D1 d (C1 c'' f)), f ~ (g :*: h)) => GXmlPickler [Node] (D1 d (C1 c'' (g :*: h))) where
  {-# INLINE gxpickleContentsf #-}
  --gxpicklef f = xpElemNodes (fromString . formatElement $ conName (undefined :: M1 C c'' f p)) (gxpickleContentsf f)
  gxpickleContentsf f =  (fromString $ "In Single-constructor type " ++ datatypeName (undefined :: ty p) ++ " with constructor " ++ conName (undefined :: (C1 c'' f p)), "") <?+>  (xpWrap (M1 . M1 . uncurry (:*:)  ) ( (\(a :*: b) -> (a, b)) . unM1 . unM1) (((":*:", "left") <?+> gxpicklef f) `xpPair` ((":*:", "right") <?+> gxpicklef f)))
-- | An instance for flag types.
instance {-# OVERLAPPING #-} (Constructor c'',  Datatype d, GXmlPickler [Node] U1,  Datatype d, ty ~ (D1 d (C1 c'' U1))) => GXmlPickler [Node] (D1 d (C1 c'' U1)) where
  {-# INLINE gxpickleContentsf #-}
  --gxpicklef f = xpElemNodes (fromString . formatElement $ conName (undefined :: M1 C c'' f p)) (gxpickleContentsf f)
  gxpickleContentsf f =  (fromString $ "Flag: " ++ datatypeName (undefined :: ty p), "") <?+>   (xpWrap (M1 . M1  )  (const U1)  (gxpicklef f))

-- | An instance for wrapper datatypes/newtypes. Don't bother with the wrapper constructor or fieldname; just the wrapped data.
instance {-# OVERLAPPING #-} ( XMLPickler [Node] a,  Datatype d, ty ~ (D1 d (C1 c'' (S1 c (K1 i a))))) => GXmlPickler [Node] (D1 d (C1 c'' (S1 c (K1 i a)))) where
  {-# INLINE gxpickleContentsf #-}
  gxpickleContentsf f = {-xpElemNodes (fromString . formatElement $ datatypeName (undefined :: ty p))-} (fromString $ datatypeName (undefined :: D1 d f p), "") <?+> (xpWrap (M1 . M1 . M1 . K1) (unK1 . unM1 . unM1 . unM1) (xpickle))

{-# INLINE xpSum #-}

-- | Pickle adapter for ':+:'
xpSum :: PU t (f r) -> PU t (g r) -> PU t ((f :+: g) r)
xpSum l r = xpWrap i o (xpEither l r)
  where
    i (Left x) = L1 x
    i (Right x) = R1 x
    o (L1 x) = Left x
    o (R1 x) = Right x

-- | Pickle adapter for 'Datatype'
{-# INLINE optElemD #-}
optElemD :: forall (t :: Type -> Meta -> (Type -> Type) -> Type -> Type) i (s :: Meta) (f :: Type -> Type) p a. (Datatype s) => PU [Node] a -> t i s f p -> PU [Node] a
optElemD x y = case formatElement (datatypeName y) of
  "" -> x
  n -> xpElemNodes (fromString n) x

-- | Pickle adapter for 'Constructor'
{-# INLINE optElemC #-}
optElemC :: forall (t :: Type -> Meta -> (Type -> Type) -> Type -> Type) i (s :: Meta) (f :: Type -> Type) p a. (Constructor s) => PU [Node] a -> t i s f p -> PU [Node] a
optElemC x y = case formatElement (conName y) of
  "" -> x
  n -> xpElemNodes (fromString n) x

-- | Pickle adapter for 'Selector'
{-# INLINE optElem #-}
optElem :: forall (t :: Type -> Meta -> (Type -> Type) -> Type -> Type) i (s :: Meta) (f :: Type -> Type) p a. (Selector s) => PU [Node] a -> t i s f p -> PU [Node] a
optElem x y = case formatElement (selName y) of
  "" -> x
  n -> xpElemNodes (fromString n) x

-- | Format field names nicely
{-# INLINE formatElement #-}
formatElement :: String -> String
formatElement = headToLower . stripLeadingAndTrailingUnderscore

{-# INLINE headToLower #-}
headToLower :: String -> String
headToLower l = case l of
  [] -> []
  (x : xs) -> toLower x : xs

{-# INLINE stripLeadingAndTrailingUnderscore #-}
stripLeadingAndTrailingUnderscore :: String -> String
stripLeadingAndTrailingUnderscore = stripLeadingUnderscore . stripTrailingUnderscore

{-# INLINE stripLeadingUnderscore #-}
stripLeadingUnderscore :: String -> String
stripLeadingUnderscore s = case s of
  ('_' : ls) -> ls
  ls -> ls

{-# INLINE stripTrailingUnderscore #-}
stripTrailingUnderscore :: String -> String
stripTrailingUnderscore s = case s of
  "" -> ""
  [x, '_'] -> [x]
  (x : xs) -> x : stripTrailingUnderscore xs
