{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE Trustworthy #-}
{-# OPTIONS_GHC -fno-prefer-byte-code #-}
-- |
-- Module      :  Games.ECS.Prototype
-- Description : Prototype definitions
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- Prototypes are exemplar individuals which form a template.
module Games.ECS.Prototype (module Games.ECS.Prototype, module Games.ECS.Prototype.PrototypeID) where


import Control.Lens

--import Data.Coerce

--import Data.Ix
--import Data.Vector.Unboxed.Deriving
import GHC.Generics
import Games.ECS.Serialisation
import Games.ECS.Prototype.PrototypeID
import Games.ECS.Component.TH
import Games.ECS.Component

-- | A component for denoting that an individual is a prototype, to be instantiated later.
data IsPrototype = IsPrototype
  { -- | The t'PrototypeID'.
    _rawIsPrototypeID :: !PrototypeID,
    -- | Indicates that this extends --- and overrides, in case of conflicting values --- another prototype.
    _extendsPrototype :: Maybe PrototypeID
  }
  deriving stock (Eq, Generic, Show)

makeLenses ''IsPrototype

instance HasPrototypeID IsPrototype where
  {-# INLINE prototypeID #-}
  prototypeID = rawIsPrototypeID

instance {-# OVERLAPS #-} XMLPickler [Node] IsPrototype where
  {-# INLINE xpickle #-}
  xpickle =
    xpWrap
      (\(pid, epid) -> IsPrototype pid epid)
      (\(IsPrototype pid epid) -> (pid, epid))
      ( xpElemAttrs
          "prototype"
          ( xpPair
              (pickleAsAttribute "prototypeName")
              (pickleAsAttribute "extends")
          )
      )

instance Component IsPrototype where
  type CanonicalName IsPrototype = "isPrototype"


makeHasComponentClass ''IsPrototype
