-- |
-- Module      :  Games.ECS.Component.TH
-- Description : Template Haskell derivation of Component classes
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- Implements helper classes and constraints on components.

module Games.ECS.Component.TH (module Games.ECS.Component.TH.Internal) where

import Games.ECS.Component.TH.Internal (makeHasComponentClass)
