-- |
-- Module      :  Games.ECS.Slot
-- Description : ECS Slots
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
-- 
-- Slots are /logical/ places in a `Games.ECS.Component.Store.ComponentStore`.

module Games.ECS.Slot where

import Control.Lens
-- | A helper class for a generic "has" `Control.Lens.Type.Lens'`.
class HasType a s where
  {-# MINIMAL typed | getTyped, setTyped #-}
  -- | A 'Lens'' for accessing a well-known contained type.
  typed :: Lens' s a
  typed = lens (getTyped @a) (flip (setTyped @a))
  -- | A getter for accesing a well-known contained type.
  {-# INLINE typed #-}
  getTyped :: s -> a
  getTyped s = s ^. typed @a
  {-# INLINE getTyped #-}
  -- | A setter for a well-known contained type.
  setTyped :: a -> s -> s
  setTyped a s = s & (typed @a) .~ a
  {-# INLINE setTyped #-}
