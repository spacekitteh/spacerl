-- |
-- Module      :  Games.ECS.Util.Misc
-- Description : Other junk.
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- Things which don't really fit anywhere else.

module Games.ECS.Util.Misc where

import Data.Hashable
import Data.Sequence (Seq)

-- | a 'Hashable' 'Seq'.
newtype HashableSeq a = HashableSeq (Seq a) deriving newtype Eq

instance Hashable a => Hashable (HashableSeq a) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (HashableSeq s) = foldl hashWithSalt salt s
