{-# LANGUAGE QuasiQuotes #-}
-- |
-- Module      :  Games.ECS.Prototype.Serialisation
-- Description :  Serialisation for entity prototypes.
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- Prototypical individuals can have a specialised representation when serialised, allowing them to omit 'Games.ECS.Entity' elements.
module Games.ECS.Prototype.Serialisation (AsPrototype(AsPrototype), includePrototypesFromRaws)where
import Games.ECS.Serialisation
import Control.Exception
import Text.XML.QQ
import Control.Monad.IO.Class
import Data.Foldable (foldlM)    
import Text.XML
import Text.XML.Lens
import Control.Lens    
import Data.Map
import Data.Text
import Games.ECS.World
import GHC.Generics (Generic)
import Data.String
import GHC.TypeLits
import Data.Proxy
import Data.Hashable

-- | A wrapper for individual prototype entities which should be serialised in the prototype format. The main
-- difference is that, instead of an 'Games.ECS.Entity' reference, there is a user-friendly --- and stable ---
-- prototype name.
newtype AsPrototype (prototypeClass :: Symbol) worldType = AsPrototype (worldType Individual)
    deriving stock (Generic)
deriving stock instance Eq (worldType Individual) => Eq (AsPrototype pc worldType)
deriving stock instance Ord (worldType Individual) => Ord (AsPrototype pc worldType)
deriving anyclass instance (Hashable (worldType Individual)) => Hashable (AsPrototype pc worldType)
deriving stock instance Show (worldType Individual) => Show (AsPrototype pc worldType)    

-- | The general idea of this instance is that first, when pickling, we run a normal pickler, then modify the
-- resulting XML node. Likewise, when unpickling, we first modify the parsed XML node, then run the normal
-- pickler.
instance (KnownSymbol prototypeClass, XMLSerialise (worldType Individual)) => XMLSerialise (AsPrototype prototypeClass worldType) where
    -- | Ignores the element name.
    deserialise _ e = case fromXMLElement e of
                        Right e' -> deserialisePrototype (symbolVal (Proxy :: Proxy prototypeClass)) e'
                        Left errs -> Left (ErrorMessage ( Data.Text.show errs))

    -- | Ignores the element name.
    serialise _ elem = toXMLElement $ serialisePrototype (symbolVal (Proxy  :: Proxy prototypeClass)) elem
    

mergePrototypeDefinitions :: [Element] -> [Element]
mergePrototypeDefinitions = id
-- mergePrototypeDefinitions originalList = add each base prototype to resolvedList
--                                          remove each base prototype from originalList
--                                          while there are unresolved prototypes (original list isn't empty)
--                                            prototypeToResolve <- prototype that hasn't been resolved, but all of its parents have
--                                            resolvedList <- resolvePrototypeDefinition resolvedList prototypeToResolve
--                                            remove from original list
-- resolvePrototypeDefinition :: [Element] -> Element -> Element
-- resolvePrototypeDefinition existingResolved current = for each incomplete hierarchy of prototypes, augment the current one with parent values, resolving conflicts with younger (or later ancestors in case of multiple inheritance) = higher precedence
includePrototypesFromRaws :: forall (prototypePluralClass :: Symbol) (prototypeSingularClass :: Symbol) {worldType} {m} . (MonadIO m, KnownSymbol prototypePluralClass, KnownSymbol prototypeSingularClass, World worldType, XMLSerialise (worldType Individual)) => worldType Storing -> FilePath -> m (Either [UnpickleError] (worldType Storing))
includePrototypesFromRaws world path = do
  definitions <- liftIO $ Text.XML.readFile def path
  let rawRelevantPrototypes = definitions ^..root ... named (fromString $ symbolVal (Proxy :: Proxy prototypePluralClass)) ... named (fromString $ symbolVal (Proxy :: Proxy prototypeSingularClass)) 
      relevantPrototypes = mergePrototypeDefinitions rawRelevantPrototypes
  (newWorld, errs) <- foldlM (\(w, errs) e -> do
            newRef <- liftIO newUniqueEntRef
            let res = deserialise @(AsPrototype prototypeSingularClass worldType) (fromString $ symbolVal (Proxy :: Proxy prototypeSingularClass)) (toXMLElement e)
            case res of
              Left err -> pure (w, err : errs)
              Right (AsPrototype proto) -> do
                                pure (storeEntity (proto & unsafeEntityReference .~ newRef) w, errs)) (world, []) relevantPrototypes 
  case errs of
    [] -> pure (Right newWorld)
    _ -> pure (Left errs)
    
  
                       
deserialisePrototype :: ( XMLSerialise (worldType Individual)) => String -> Element -> Either UnpickleError (AsPrototype pc worldType)
deserialisePrototype prototypeClass elem = do
  asEntNode <- prototypeNodeToEntityNode prototypeClass (elem^. re _Element)
  AsPrototype <$> deserialise "individual" (toXMLElement (asEntNode ^?! _Element))


serialisePrototype :: (XMLSerialise (worldType Individual)) => String -> (AsPrototype pc worldType) -> Element
serialisePrototype prototypeClass (AsPrototype ent) =
    case fromXMLElement (serialise "individual" ent) of
      Left errs -> throw $ XMLConversionException errs
      Right elem -> case entityNodeToPrototypeNode prototypeClass (NodeElement elem) of
                      (NodeElement transformed) -> transformed
                      _ -> throw $ UnpicklingException (ErrorMessage "entityNodeToPrototypeNode didn't return an XML element!")
    
prototypeNodeToEntityNode :: String -> Text.XML.Node -> Either UnpickleError Text.XML.Node
prototypeNodeToEntityNode prototypeClass (NodeElement protoElem@(Element nodeName (attribs :: Map Name Text) childNodes)) | nodeName == (fromString prototypeClass) =
    case protoElem^? Text.XML.Lens.attr "prototypeName" of
      Just prototypeName -> pure (NodeElement (Element "individual" (attribs & at "prototypeName" .~ Nothing & at "extends" .~ Nothing ) newChildNodes)) where
        prototypeExtends = protoElem ^? Text.XML.Lens.attr "extends"
        blankEntRefNode = [xml|<entity entRef="-1" />|] ^?! _Right . root . re _Element
        isPrototypeNode = [xml|<isPrototype><prototype
                               %{ case prototypeExtends }
                               %{ of Just parent }
                               extends="#{parent}"
                               %{ of Nothing }
                               %{ endcase }
                               prototypeName="#{prototypeName}" /></isPrototype>|] ^?! _Right .  root . re _Element
        newChildNodes = blankEntRefNode : isPrototypeNode : (cleanChildNodes childNodes)
      Nothing -> Left (ErrorMessage "No prototypeName attribute")
prototypeNodeToEntityNode protoClass _ = Left (ErrorMessage $ "Not at a " <> (pack protoClass) <> " element node")
                                 
entityNodeToPrototypeNode :: String -> Text.XML.Node -> Text.XML.Node
entityNodeToPrototypeNode prototypeClass (NodeElement (Element "individual" oldAttribs childNodes)) = NodeElement (Element (fromString prototypeClass) attribs newChildNodes) where
    prototypeAttrs = childNodes ^?! traversed . _Element . named "isPrototype" ... named "prototype" .  attrs
    attribs = oldAttribs <> prototypeAttrs
    newChildNodes = cleanChildNodes childNodes

                                 
cleanChildNodes :: [Text.XML.Node] -> [Text.XML.Node]
cleanChildNodes childNodes = childNodes ^.. traversed . confusing (filtered (hasn't (_Element . named "isPrototype")) . filtered (hasn't (_Element . named "entity")))
