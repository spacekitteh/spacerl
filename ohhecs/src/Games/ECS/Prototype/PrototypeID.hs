{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE Trustworthy #-}
-- |
-- Module      :  Games.ECS.Prototype.PrototypeID
-- Description : Prototype definitions
-- Copyright   :  (C) 2020 Sophie Taylor
-- License     :  AGPL-3.0-or-later
-- Maintainer  :  Sophie Taylor <sophie@spacekitteh.moe>
-- Stability   :  experimental
-- Portability: GHC
--
-- Prototype IDs are essentially just strings.

module Games.ECS.Prototype.PrototypeID
    (
     PrototypeID (PrototypeID),
     HasPrototypeID(..),
    )
    where

import Games.ECS.Serialisation
import Control.Lens
import Data.Interned    
import Data.Interned.Text
import Data.Hashable
import GHC.Generics    
import Data.String
import Data.Text
import Control.DeepSeq
import GHC.Read

-- | A prototype's ID is distinct from its entity reference in that it is stable, and in a unique namespace.
newtype PrototypeID = PrototypeID {_unPrototypeID :: InternedText}
  deriving newtype (Eq, Ord)
  deriving newtype (XMLPickleAsAttribute)
  deriving stock (Generic)

  deriving newtype (Hashable)  
makeClassy ''PrototypeID
instance NFData PrototypeID where rnf x = seq x ()
instance Show PrototypeID where
    show (PrototypeID s) = unpack (unintern s)
instance IsString PrototypeID where
    fromString = PrototypeID . intern . pack
instance Read PrototypeID where
    readPrec  = fmap (PrototypeID . intern) $ readPrec
    readListPrec = readListPrecDefault
