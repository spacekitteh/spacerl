{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE Trustworthy #-}
module Games.ECS.Entity.EntityMap where
import Control.Lens
import Data.Coerce
import Games.ECS.Serialisation
import Games.ECS.Entity    
import Games.ECS.Entity.EntitySet
import GHC.Exts (IsList(..))
import GHC.Generics    
import Data.IntMap.Strict qualified as IM
import Control.DeepSeq    
import GHC.Stack
newtype EntityMap a = EntityMap {_theEntityMap :: IM.IntMap a}
    deriving newtype (Eq, Show, Semigroup, Monoid, NFData)
    deriving stock (Functor, Foldable, Traversable, Generic, Generic1)

             
emptyEntityMap :: EntityMap a
emptyEntityMap = EntityMap (IM.empty)

{-# INLINE CONLIKE singletonEntityMap #-}                 
singletonEntityMap :: Entity -> a -> EntityMap a
singletonEntityMap (EntRef e) a = EntityMap $! IM.singleton e a

{-# INLINE CONLIKE null #-}                                  
null :: EntityMap a -> Bool
null (EntityMap m) = IM.null m
                                  
makeLensesWith (lensRules & generateSignatures .~ False) ''EntityMap
-- | Access the underlying 'IM.IntMap'.
theEntityMap :: Iso' (EntityMap a) (IM.IntMap a)


instance XMLPickler [Node] a => XMLPickler [Node] (EntityMap a) where
    xpickle = ("EntityMap","") <?+>
                (xpWrap
                 (maybe emptyEntityMap GHC.Exts.fromList) (\m -> if Games.ECS.Entity.EntityMap.null m then Nothing else Just (GHC.Exts.toList m)) $ -- Don't encode empty maps
                   xpOption $ xpAll $ xpElem "li" (pickleAsAttribute "key") xpickle)

                
instance FunctorWithIndex Entity EntityMap where
    {-# INLINE imap #-}
    imap f (EntityMap m) = EntityMap $ IM.mapWithKey (coerce f) m
instance FoldableWithIndex Entity EntityMap where
  {-# INLINE ifoldMap #-}
  {-# INLINE ifoldr #-}
  {-# INLINE ifoldl' #-}
  ifoldMap f (EntityMap s) = IM.foldMapWithKey (coerce f) s
  ifoldr f b (EntityMap s) = IM.foldrWithKey (coerce f) b s
  ifoldl' f b (EntityMap s) = IM.foldlWithKey' (flip (coerce f)) b s

instance TraversableWithIndex Entity EntityMap where
  {-# INLINE itraverse #-}
  itraverse f (EntityMap s) = fmap (\i -> EntityMap i) $ itraverse (f . EntRef) s                              

type instance Index (EntityMap a) = Entity
type instance IxValue (EntityMap a) = a

instance Ixed (EntityMap a) where
    {-# INLINE ix #-}
    ix (EntRef k) f (EntityMap m) = EntityMap <$> ix k f m
    
instance At (EntityMap a) where
    {-# INLINE at #-}
    at (EntRef k) f em@(EntityMap m) =
        f mv <&> \r -> case r of
                         Nothing -> maybe em (const (coerce $ IM.delete k m)) mv
                         Just v' -> coerce (IM.insert k v' m)
        where
          mv = IM.lookup k m

instance AsEmpty (EntityMap a) where
    {-# INLINE _Empty #-}
    _Empty = nearly (EntityMap IM.empty) (\(EntityMap m) -> IM.null m)

instance IsList (EntityMap a) where
    type Item (EntityMap a) = (Entity, a)
    {-# INLINE fromList #-}
    fromList l = EntityMap $ IM.fromList (fmap (\(EntRef e, a) -> (e, a)) l)
--    fromListN n l = EntityMap $ IM.fromListN n (fmap (\(EntRef e, a) -> (e, a)) l)
    {-# INLINE toList #-}
    toList  = (fmap (\(e, a) -> (EntRef e, a))) . IM.toList . _theEntityMap

{-# INLINE fromListWith #-}              
fromListWith :: (a -> a -> a) -> [(Entity, a)] -> EntityMap a
fromListWith comb l = EntityMap $! IM.fromListWith comb (fmap (\(EntRef e, a) -> (e, a)) l)

{-# INLINE unionWith #-}                      
unionWith :: (a -> a -> a) -> EntityMap a -> EntityMap a -> EntityMap a
unionWith f (EntityMap a) (EntityMap b) = EntityMap $! (IM.unionWith f a b)

{-# INLINE CONLIKE keysSet #-}                                          
keysSet :: EntityMap a -> EntitySet
keysSet (EntityMap a) = EntitySet $! (IM.keysSet a)

{-# INLINE (!) #-}                        
infixl 9 !                        
(!) :: HasCallStack => EntityMap a -> Entity -> a
(EntityMap m) ! (EntRef a) = m IM.! a

{-# INLINE CONLIKE insert #-}                             
insert :: Entity -> a -> EntityMap a -> EntityMap a
insert (EntRef !e) a (EntityMap !m) = EntityMap $! IM.insert e a m
    
