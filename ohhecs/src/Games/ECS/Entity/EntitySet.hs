{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE Trustworthy #-}
module Games.ECS.Entity.EntitySet where
import Data.Coerce
import Control.Monad    
import Games.ECS.Entity
import Data.IntSet qualified as IS
import Control.DeepSeq
import Control.Lens
import Data.Functor
import GHC.Generics    
import GHC.Exts (IsList(..))      
import Data.Hashable
import Games.ECS.Serialisation
import Data.IntSet.Lens    

-- | An efficient storage for a collection of entities.
newtype EntitySet = EntitySet {_theEntitySet :: IS.IntSet}
  deriving newtype (Eq, Show, Semigroup, Monoid, NFData, Hashable)
  deriving stock Generic

makeLensesWith (lensRules & generateSignatures .~ False) ''EntitySet
-- | Access the underlying 'IS.IntSet'.
theEntitySet :: Iso' EntitySet IS.IntSet


type instance Index EntitySet = Entity
type instance IxValue EntitySet = ()    
instance Ixed EntitySet where
    {-# INLINE ix #-}
    ix (EntRef k) f m'@(EntitySet m) = if IS.member k m then
                               f () $> m'
                              else pure m'
instance At EntitySet where
    {-# INLINE at #-}
    at (EntRef k) f s'@(EntitySet s) = fmap choose (f (guard member_)) where
                      member_ = IS.member k s
                      (inserted, deleted)
                        | member_ = (s', EntitySet $ IS.delete k s)
                        | otherwise = (EntitySet $ IS.insert k s, s')
                      choose (Just ~()) = inserted
                      choose Nothing = deleted
               
instance HasEntityReferences EntitySet where
  {-# INLINE getEntityReferences #-}
  getEntityReferences = coerced . members . Control.Lens.to EntRef               

instance IsList EntitySet where
    type Item EntitySet = Entity
    {-# INLINE toList #-}    
    toList (EntitySet e) = coerce $ IS.toList e
    {-# INLINE fromList #-}                       
    fromList = coerce .  IS.fromList . coerce

instance Contains EntitySet where
    {-# INLINE contains #-}
    contains (EntRef k) f (EntitySet s) = fmap coerce $ IS.alterF f k s
instance IsEntityStore EntitySet where
  {-# INLINE knownEntities #-}
  knownEntities = theEntitySet . knownEntities
  blankEntityStorage = EntitySet blankEntityStorage
               
deriving via AsList (EntitySet) instance  XMLPickler [Node] (EntitySet)             


{-# INLINE foldl' #-}
foldl' :: (b -> Entity -> b) -> b -> EntitySet -> b
foldl' f x (EntitySet s) = IS.foldl' (\b i -> f b (EntRef i)) x s
-- | Construct a new t'EntitySet' with a given 'Entity'.
singletonEntitySet :: Entity -> EntitySet
singletonEntitySet (EntRef !k) = force $ EntitySet (IS.singleton k)
{-# INLINE singletonEntitySet #-}
{-# RULES "entitySet/singletonMember" forall e. member e (singletonEntitySet e) = True #-}
{-# INLINE difference #-}
difference :: EntitySet -> EntitySet -> EntitySet
difference (EntitySet a) (EntitySet b) = EntitySet (a `IS.difference` b)

{-# INLINE fromDistinctAscList #-}                                         
fromDistinctAscList :: [Entity] -> EntitySet
fromDistinctAscList = coerce . IS.fromDistinctAscList . coerce

-- | Helper `Iso'` for selecting entities which satisfy predicates.
{-# INLINE asIntersection #-}
asIntersection :: Iso' IntersectionOfEntities EntitySet
asIntersection = iso (\case Intersect es -> EntitySet es) (Intersect . _theEntitySet)
                 
{-# INLINE setOf #-}
setOf :: Getting EntitySet s Entity -> s -> EntitySet
setOf l = views l singletonEntitySet

{-# INLINE CONLIKE member #-}          
member :: Entity -> EntitySet -> Bool
member (EntRef e) (EntitySet es) = IS.member e es

-- | A helper 'Monoid' for selecting entities which satisfy multiple predicates.
newtype IntersectionOfEntities = Intersect {_unIntersect :: IS.IntSet} deriving stock (Eq, Show)

instance Semigroup IntersectionOfEntities where
  {-# INLINE CONLIKE (<>) #-}
  (Intersect a) <> (Intersect b) = Intersect (IS.intersection a b)

instance Monoid IntersectionOfEntities where
  mempty = error "mempty IntersectionOfEntities"


instance AsEmpty EntitySet where
    {-# INLINE _Empty #-}
    _Empty = coerced @EntitySet @EntitySet @(IS.IntSet)  . _Empty


instance IsEntityStore IntersectionOfEntities where
  {-# INLINE knownEntities #-}
  knownEntities = asIntersection . knownEntities
  blankEntityStorage = Intersect blankEntityStorage


-- | For types which may have one or more t'EntitySet'.
class HasEntitySet a where
  entitySet :: Fold a EntitySet

instance HasEntitySet EntitySet where
  {-# INLINE entitySet #-}
  entitySet = simple

instance HasEntitySet IntersectionOfEntities where
  {-# INLINE entitySet #-}
  entitySet = asIntersection
{-# INLINE unions #-}           
unions :: (Functor f, Foldable f) => f EntitySet -> EntitySet
unions !fs = force $ coerce . IS.unions . fmap coerce $! fs
{-# INLINE union #-}
union :: EntitySet -> EntitySet -> EntitySet
union !a !b = force $ coerce $  IS.union  (coerce a) (coerce b)
