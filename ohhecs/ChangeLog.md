Changelog for [`ohhecs` package](https://hackage.haskell.org/package/ohhecs)

# 0.0
## 0.0.3
- Added specialised support for prototype serialisation, allowing stable, user-friendly IDs instead of entity references.
- Changed IsPrototype serialisation schema.
## 0.0.2
Switched prototype IDs to be text-based, allowing for a much nicer representation.
## 0.0.1
Initial release.
