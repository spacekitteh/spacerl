{ mkDerivation, base, byte-order, containers, generic-lens
, ghc-prim, hashable, intern, lens, lib, mtl, parallel, polysemy
, polysemy-plugin, tagged, template-haskell, th-abstraction
, unordered-containers, vector, vector-th-unbox
}:
mkDerivation {
  pname = "ohhecs";
  version = "0.1.0.0";
  src = ./.;
  libraryHaskellDepends = [
    base byte-order containers generic-lens ghc-prim hashable intern
    lens mtl parallel polysemy polysemy-plugin tagged template-haskell
    th-abstraction unordered-containers vector vector-th-unbox
  ];
  license = lib.licenses.gpl3Plus;
}
